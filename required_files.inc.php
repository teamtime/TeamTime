<?php
// required_files.inc.php

/*
	TeamTime is a software to manage people working in team on a cyclic shift.
	Copyright (C) 2012 Manioul - webmaster@teamtime.me

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

set_include_path(implode(PATH_SEPARATOR, array(realpath(dirname(__FILE__)), get_include_path())));

require_once 'firePHP.inc.php';

require_once 'autoload.inc.php';
if (isset($conf['page']['include']['constantes']))		require_once 'constantes.inc.php';
if (isset($conf['page']['include']['errors']))			require_once 'errors.inc.php';
if (isset($conf['page']['include']['globalConfig']))		require_once 'config.inc.php';
if (isset($conf['page']['include']['globals_db']))		require_once 'globals_db.inc.php';
if (isset($conf['page']['include']['init']))			require_once 'init.inc.php';
if (isset($conf['page']['include']['session']))			require_once 'session.inc.php';
if (isset($conf['page']['include']['bibliothequeMaintenance']))	require_once 'bibliotheque_maintenance.inc.php';
if (isset($conf['page']['include']['smarty'])) {		require_once 'smarty_page.inc.php';
} elseif (isset($conf['page']['include']['smarty_ajax'])) {	require_once 'smarty_ajax.inc.php'; }

?>
