<?php
// ajax.php
//
/**
 * Traitement des requêtes ajax.
 */

/*
	TeamTime is a software to manage people working in team on a cyclic shift.
	Copyright (C) 2012 Manioul - webmaster@teamtime.me

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


ob_start(); // Obligatoire pour firePHP

/*
 * Configuration de la page
 * Définition des include nécessaires
 */
	$conf['page']['include']['constantes'] = 1; // Ce script nécessite la définition des constantes
	$conf['page']['include']['errors'] = NULL; // le script gère les erreurs avec errors.inc.php
	$conf['page']['include']['class_debug'] = 1; // La classe debug est nécessaire à ce script
	$conf['page']['include']['globalConfig'] = 1; // Ce script nécessite config.inc.php
	$conf['page']['include']['init'] = 1; // la session est initialisée par init.inc.php
	$conf['page']['include']['globals_db'] = 1; // Le DSN de la connexion bdd est stockée dans globals_db.inc.php
	$conf['page']['include']['class_db'] = 1; // Le script utilise class_db.inc.php
	$conf['page']['include']['session'] = 1; // Le script utilise les sessions par session.imc
	$conf['page']['include']['classUtilisateur'] = NULL; // Le sript utilise uniquement la classe utilisateur (auquel cas, le fichier class_utilisateur.inc.php
	$conf['page']['include']['class_utilisateurGrille'] = 1; // Le sript utilise la classe utilisateurGrille
	$conf['page']['include']['class_cycle'] = 1; // La classe cycle est nécessaire à ce script (remplace grille.inc.php
	$conf['page']['include']['class_email'] = 1; // La classe Email est nécessaire à ce script (remplace grille.inc.php
	$conf['page']['include']['class_menu'] = NULL; // La classe menu est nécessaire à ce script
	$conf['page']['include']['smarty_ajax'] = 1; // Smarty sera utilisé sur cette page
	$conf['page']['compact'] = NULL; // Compactage des scripts javascript et css
	$conf['page']['include']['bibliothequeMaintenance'] = NULL; // La bibliothèque des fonctions de maintenance est nécessaire
/*
 * Fin de la définition des include
 */


/*
 * Configuration de la page
 */
// Définit la valeur de $DEBUG pour le script
// on peut activer le debug sur des parties de script et/ou sur certains scripts :
// $DEBUG peut être activer dans certains scripts de required et désactivé dans d'autres
	$DEBUG = false;

	/*
	 * Choix des éléments à afficher
	 */
	
	// Affichage du menu horizontal
	$conf['page']['elements']['menuHorizontal'] = false;
	// Affichage messages
	$conf['page']['elements']['messages'] = false;
	// Affichage du choix du thème
	$conf['page']['elements']['choixTheme'] = false;
	// Affichage du menu d'administration
	$conf['page']['elements']['menuAdmin'] = false;
	
	// éléments de debug
	
	// FirePHP
	$conf['page']['elements']['firePHP'] = true;
	// Affichage des timeInfos
	$conf['page']['elements']['timeInfo'] = $DEBUG;
	// Affichage de l'utilisation mémoire
	$conf['page']['elements']['memUsage'] = $DEBUG;
	// Affichage des WherewereU
	$conf['page']['elements']['whereWereU'] = $DEBUG;
	// Affichage du lastError
	$conf['page']['elements']['lastError'] = $DEBUG;
	// Affichage du lastErrorMessage
	$conf['page']['elements']['lastErrorMessage'] = $DEBUG;
	// Affichage des messages de debug
		$conf['page']['elements']['debugMessages'] = $DEBUG;



	// Utilisation de jquery
	$conf['page']['javascript']['jquery'] = false;
	// Utilisation de grille2.js.php
	$conf['page']['javascript']['grille2'] = false;
	// Utilisation de utilisateur.js
	$conf['page']['javascript']['utilisateur'] = false;

	// Feuilles de styles
	// Utilisation de la feuille de style general.css
	$conf['page']['stylesheet']['general'] = true;
	$conf['page']['stylesheet']['grille'] = false;
	$conf['page']['stylesheet']['grilleUnique'] = false;
	$conf['page']['stylesheet']['utilisateur'] = false;

	// Compactage des pages
	$conf['page']['compact'] = false;
	
/*
 * Fin de la configuration de la page
 */

require 'required_files.inc.php';

$array = array();

/*
 * $err est le message à afficher à la fin de la requête.
 * Si $err n'est pas touché, c'est 'requête non traitée' qui sera envoyé
 * Si $err === '' alors le message 'Mise à jour effectuée' sera envoyé
 * Si $err === NULL alors aucun message ne sera envoyé
 */
$err = "Requête non traitée";

	$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("Requête Ajax", "TRACE", "", "", "%s")'
		, $_SESSION['db']->db_real_escape_string(json_encode($_REQUEST)))
	);

if (!check_token($_REQUEST['tk'])) {
	$err = "Token invalide";
}
$array['request'] = json_encode($_REQUEST);
if (sizeof($_REQUEST) > 0 && check_token($_REQUEST['tk'])) {
	/*
	 *  Régénération du token
	 */
	//
	//!\\ Certaines requêtes doivent désactiver le token
	//    (cas des $.post dans des boucles each)
	//    Si vous utilisez une requête ajax dans un .each
	//    rétablissez l'ancienne valeur du token :
	//    __________________________________
	//      $array['tk'] = $oldToken;
	//      $_SESSION['token'] = $oldToken;
	//    __________________________________
	$oldToken = $_SESSION['token'];
	$array['tk'] = gen_token(sizeof($_SESSION['token']), TRUE);
	// Définition de la valeur de requête traitée
	// 0 si la requête n'a pas été traitée correctement
	// 1 si la requête a été traitée correctement
	$array['ok'] = 0;
	if (array_key_exists('q', $_REQUEST)) {
		switch($_REQUEST['q']) {
		/**
		 * Gestion de la configuration de la grille
		 */
		case 'CF':
			if (	$_SESSION['utilisateur']->hasRole('teamEdit')
				&& array_key_exists('conf', $_REQUEST)
				&& array_key_exists('id', $_REQUEST)) {
					if ($_REQUEST['conf'] != 'W' && $_REQUEST['conf'] != 'E') {
						$err = 'Conf inconnue...';
					} else {
						if (preg_match('/confa(\d{4})m(\d*)j(\d*)/', $_REQUEST['id'], $ar)) {
							firePhpLog($ar, 'arr');
							$date = new Date(sprintf("%04d-%02d-%02d", $ar[1], $ar[2], $ar[3]));
							$affectation = $_SESSION['utilisateur']->affectationOnDate($date);
							$sql = sprintf("
								UPDATE `TBL_GRILLE`
								SET `conf` = '%s'
								WHERE `readonly` = FALSE
								AND `date` BETWEEN '%s' AND '%s'
								AND `centre` = '%s'
								AND `team` = '%s'
								", $_REQUEST['conf']
								, $date->date()
								, $date->addJours(Cycle::getCycleLength($affectation['centre'], $affectation['team'])-1)->date()
								, $affectation['centre']
								, $affectation['team']
							);

							$_SESSION['db']->db_interroge($sql);
							if ($_SESSION['db']->db_affected_rows() < Cycle::getCycleLength($affectation['centre'], $affectation['team'])) { // Le verrouillage ne verrouille pas les jours de REPOS, d'où un nombre de données affectées même lorsque la grille n'est pas modifiable
								$err = "Modification impossible...";
								$_SESSION['db']->db_interroge(sprintf('
									CALL messageSystem("Modification de la configuration impossible.", "DEBUG", "updateConf.php", "mod failed", "affected_rows:%d;shouldBe:%d;POST:%s;SESSION:%s")'
									, $_SESSION['db']->db_affected_rows()
									, Cycle::getCycleLength($affectation['centre'], $affectation['team'])
									, $_SESSION['db']->db_real_escape_string(json_encode($_REQUEST))
									, $_SESSION['db']->db_real_escape_string(json_encode($_SESSION))
									)
								);
							} else {
								$err = "";
							}
							firePhpLog($sql, 'SQL');
						} else {
							$err = "Date inconnue";
						}
					}
			}
			break;
		/**
		 * Valide des congés
		 *
		 * change l'état des congés de saisis à validés
		 */
		case 'CG':
			// Conserve l'ancien token car cette requête est répétée 
			// plusieurs fois en peu de temps, rendant inadéquate
			// l'utilisation du token (à cause des requêtes asynchrones)
			$array['tk'] = $oldToken;
			$_SESSION['token'] = $oldToken;
			if (preg_match('/u(.+)d(\d{2,4}-\d{2}-\d{2,4})/', $_POST['id'], $d)) { // La date doit respecter les formats fr ou us
				$date = new Date($d[2]);
				if (array_key_exists('f', $_POST) && $_SESSION['utilisateur']->hasRole('teamEdit')) {
					$etat = ($_POST['f'] >= 2 ? 2 : 1);
					$sql = sprintf("UPDATE `TBL_VACANCES` SET `etat` = %d WHERE `sdid` = (SELECT `sdid` FROM `TBL_L_SHIFT_DISPO` WHERE `date` = '%s' AND `uid` = %d LIMIT 1)", $etat, $date->date(), $d[1]);
					$_SESSION['db']->db_interroge($sql);
					$err = '';
				}
			} else {
				$err = "Date inconnue";
			}
			break;
		/**
		 * Calcul une semaine d'heures pour l'équipe de l'utilisateur
		 *
		 */
		case 'CH':
			$sql = sprintf("SELECT MIN(`date`), DATEDIFF((SELECT MAX(`date`) FROM `TBL_HEURES_A_PARTAGER` WHERE `dispatched` IS FALSE AND `centre` = '%s' AND `team` = '%s'),
								(SELECT MAX(`date`) FROM `TBL_HEURES_A_PARTAGER` WHERE `dispatched` IS TRUE AND `centre` = '%s' AND `team` = '%s'))
				FROM `TBL_HEURES_A_PARTAGER`
				WHERE `dispatched` IS FALSE
				AND `centre` = '%s'
				AND `team` = '%s'
				", $_SESSION['utilisateur']->centre()
				, $_SESSION['utilisateur']->team()
				, $_SESSION['utilisateur']->centre()
				, $_SESSION['utilisateur']->team()
				, $_SESSION['utilisateur']->centre()
				, $_SESSION['utilisateur']->team());
			$row = $_SESSION['db']->db_fetch_array($_SESSION['db']->db_interroge($sql));
			if ($row[1] > 0) {
				$_SESSION['db']->db_interroge(sprintf("CALL dispatchHeuresBetween('%s', '%s', '%s', DATE_ADD('%s', INTERVAL 1 WEEK))"
					, $_SESSION['utilisateur']->centre()
					, $_SESSION['utilisateur']->team()
					, $row[0]
					, $row[0]
				));
				$err = "Des heures ont été calculées." . "(".$row[0]." - ".$row[1].")";
			} else {
				$err = NULL;
			}
			break;
		/**
		 * Vérifie si un login existe déjà
		 * 
		 * @return int 1 si le login existe déjà
		 *             2 si il existe pour le même nom, prénom (le compte existait déjà)
		 *             3 si il n'existe pas
		 */
		case 'CL':
			if (array_key_exists('k', $_REQUEST)) {
				$sql = sprintf("SELECT `nom`
					, `prenom`
					, `email`
					FROM `TBL_SIGNUP_ON_HOLD`
					WHERE `url` = '%s'
					", $_SESSION['db']->db_real_escape_string($_REQUEST['k'])
				);
				$result = $_SESSION['db']->db_interroge($sql);
				if (mysqli_num_rows($result) == 0) {
					return false;
				}
				if (utilisateurGrille::loginAlreadyExistsInDb($login, $nom, $prenom, $email) !== false) {
					return true;
				}
			}
			return false;
			break;
		/**
		 * Gestion des congés
		 * changer l'année d'un congé ou modifier le statut d'un congé
		 */
		case 'CO':
			if (array_key_exists('op', $_REQUEST) && $_REQUEST['op'] == 'yt') {
				if ($_SESSION['utilisateur']->hasRole('teamEdit') || $_SESSION['utilisateur']->uid() == $_REQUEST['id']) {
					$date = new Date($_REQUEST['val']);
					$_SESSION['db']->db_interroge(sprintf("
						CALL toggleAnneeConge(%d, '%s')
						", (int) $_REQUEST['id']
						, $date->date()));
					$err = '';
				}
			} elseif (preg_match('/u(.+)d(\d{2,4}-\d{2}-\d{2,4})/', $_REQUEST['id'], $array)) { // La date doit respecter les formats fr ou us
				$date = new Date($array[2]);
				// Changer l'année d'un congé
				if (array_key_exists('y', $_REQUEST) && $_REQUEST['y'] == 1) {
					if ($_SESSION['utilisateur']->hasRole('teamEdit') || $_SESSION['utilisateur']->uid() == $array[1]) {
						$_SESSION['db']->db_interroge(sprintf("
							CALL toggleAnneeConge(%d, '%s')
							", $array[1]
							, $date->date()));
					}
				// Changer le statut d'un congé
				} elseif (array_key_exists('f', $_REQUEST) && $_SESSION['utilisateur']->hasRole('teamEdit')) {
					$etat = ($_REQUEST['f'] >= 2 ? 2 : 1);
					$sql = sprintf("
						UPDATE `TBL_VACANCES`
						SET `etat` = %d
						WHERE `sdid` = (SELECT `sdid`
							FROM `TBL_L_SHIFT_DISPO`
							WHERE `date` = '%s'
							AND `uid` = %d
							LIMIT 1)
						"
						, $etat
						, $date->date()
						, $array[1]);
					$_SESSION['db']->db_interroge($sql);
				}
			} else {
				$err = "Date inconnue";
			}
			break;
		/**
		 * Validation des utilisateurs ayant créés un compte.
		 * Les editeurs peuvent valider ou invalider l'inscription
		 * d'un nouvel utilisateur.
		 */
		case 'CU':
			if ($_SESSION['utilisateur']->hasRole('editeurs')) {
				// Suppression des comptes créés par des inconnus (confirmUser.php)
				//
				// id contient l'identifiant de l'entrée dans la table TBL_SIGNUP_ON_HOLD
				if (array_key_exists('submit', $_REQUEST) && $_REQUEST['submit'] == "infirm") {
					if (array_key_exists('id', $_REQUEST)) {
						if ($_SESSION['utilisateur']->hasRole('admin')) {
							// Les admins peuvent supprimer tous les comptes en attente
							// indépendamment de l'affectation
							$_SESSION['db']->db_interroge(sprintf(
								"DELETE FROM `TBL_SIGNUP_ON_HOLD`
								WHERE `id` = %d
								", $_REQUEST['id']));
						} else {
							// On précise centre et équipe pour éviter
							// la suppression d'inscription dans
							// d'autres équipes que celle de l'utilisateur
							$_SESSION['db']->db_interroge(sprintf(
								"DELETE FROM `TBL_SIGNUP_ON_HOLD`
								WHERE `id` = %d
								AND `centre` = '%s'
								AND `team` = '%s'
								", $_REQUEST['id']
								, $_SESSION['utilisateur']->centre()
								, $_SESSION['utilisateur']->team()
							));
						}
						die(htmlspecialchars("Utilisateur Supprimé"));
					}
				} elseif (array_key_exists('submit', $_REQUEST) && $_REQUEST['submit'] == "confirm" && array_key_exists('dateD', $_REQUEST) && array_key_exists('dateF', $_REQUEST) && array_key_exists('grade', $_REQUEST)) {
					// Confirme l'existence d'un utilisateur et création du compte utilisateur
					if (TRUE === utilisateurGrille::acceptUser($_REQUEST['id'], $_REQUEST['dateD'], $_REQUEST['dateF'], $_REQUEST['grade'])) {
						print(htmlspecialchars("Utilisateur accepté."));
					} else {
						print(htmlspecialchars("Le mail n'a pas été envoyé à l'utilisateur."));
					}
					exit;
				}
			}
			break;
		/**
		 * Change la valeur d'une activité sur la grille
		 */
		case 'CV':
			if (!$_SESSION['TEAMEDIT'] && $_SESSION['utilisateur']->uid() != $_POST['uid']) {
				$err = "N'éditez que votre ligne, svp.";
			} else {
				$date = new Date();
				$date->annee($_POST['Year']);
				$date->mois($_POST['Month']);
				$date->jour($_POST['Day']);
				$dispo = array(
					'uid'		=> sprintf('%02d', $_POST['uid'])
					,'date'		=> $date->date()
					,'dispo'	=> $_SESSION['db']->db_real_escape_string($_POST['dispo'])
					,'oldDispo'	=> $_SESSION['db']->db_real_escape_string($_POST['oldDispo'])
				);
				$affectation = $_SESSION['utilisateur']->affectationOnDate($date);
				$err = jourTravail::addDispo($dispo, $affectation['centre'], $affectation['team']);
				$array['ok'] = 1;
			}
			break;
		/**
		 * Opérations sur la base de données :
		 * Suppression (brute) d'activités et mise à jour de l'état de péréq
		 */
		case 'DB':
			if ($_SESSION['utilisateur']->hasRole('teamEdit')) {
				if (array_key_exists('op', $_REQUEST)) {
					// Type de l'opération à effectuer
					switch ($_REQUEST['op']) { 
						// Suppresion d'une activité de TBL_L_SHIFT_DISPO à partir du sdid
						// La suppression gère également la suppression de l'activité dans TBL_VACANCES
					case 'del':
						$err = $_SESSION['db']->db_interroge("
							DELETE FROM `TBL_L_SHIFT_DISPO`
							WHERE `sdid` = " . (int) $_REQUEST['id']);
						$err .= $_SESSION['db']->db_interroge("
							DELETE FROM `TBL_VACANCES`
							WHERE `sdid` = " . (int) $_REQUEST['id']);
						break;
					case 'upd':
						if ($_REQUEST['t'] == 'l') {
							switch ($_REQUEST['field']) {
							case 'pereq':
								$val = empty($_REQUEST['val']) ? 'FALSE' : 'TRUE';
								// On met à jour le statut de péréquation à condition que la date ne soit pas nul
								// (lequel cas ne pourrait correspondre qu'à une péréq)
								$sql = sprintf("
									UPDATE `TBL_L_SHIFT_DISPO`
									SET `pereq` = %s
									WHERE `sdid` = %d
									AND `date` != '0000-00-00'
									", $val
									, $_REQUEST['id']
								);
								$err = $_SESSION['db']->db_interroge($sql);
								break;
							}
						}
						break;
					}
					// Si les deux requêtes se sont bien passées, elles renvoient chacune 1
					// d'où $err === "11" si tout s'est bien passé
					if ($err === "11") $err = '';
				}
			}
			break;
		/**
		 * Formulaire choix de l'affectation principale
		 *
		 * @return string html formulaire
		 *
		 */
		case 'FT':
			$smarty->assign('uid', (int) $_POST['uid']);
			$user = new utilisateurGrille((int) $_POST['uid']);
			$smarty->assign('affectation', $user->affectationPrincipale(date('Y-m-d')));
			$smarty->display('formTeam.tpl');
			return true;
			break;
		/**
		 * Remplit les données utilisateur
		 */
		case 'FU':
			if ($_SESSION['utilisateur']->hasRole('editeurs')) {
				$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("Remplissage des champs de l\'utilisateur", "TRACE", "ajax.php", "fillUser", "%s")'
					, $_SESSION['db']->db_real_escape_string(json_encode($_REQUEST)))
				);
				/*$sql = sprintf("
					SELECT `uid`, `nom`, `prenom`, `email`
					FROM `TBL_USERS`
					WHERE `uid` = %d
					", (int) $_REQUEST['uid']
				);
				die(json_encode($_SESSION['db']->db_fetch_assoc($_SESSION['db']->db_interroge($sql))));
				 */
				$user = new utilisateurGrille((int) $_REQUEST['uid']);
				$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("FU objet utilisateurGrille", "TRACE", "", "", "%s")'
					, $_SESSION['db']->db_real_escape_string(json_encode(array($user->asJSON(), $_REQUEST['uid'])))
				));
				$array['user'] = $user->userAsArray(true);
				$err = NULL;
			}
			break;
		/**
		 * Retourne les heures attribuées sur la date demandée.
		 */
		case 'GH':
			if (array_key_exists('d', $_REQUEST)
			    && array_key_exists('m', $_REQUEST)
			    && array_key_exists('y', $_REQUEST)) {
				$date = new Date(sprintf("%04d-%02d-%02d", $_REQUEST['y'], $_REQUEST['m'], $_REQUEST['d']));
				$affectation = $_SESSION['utilisateur']->affectationOnDate($date);
				$return = "";
				$sql = sprintf("
					SELECT `uid`, `normales`, `instruction`, `statut`, `secteurs`, `occurrence`
					FROM `TBL_HEURES`
					WHERE `date` = '%s'
					AND `uid` IN (SELECT `uid` FROM `TBL_AFFECTATION`
						WHERE '%s' BETWEEN `beginning` AND `end`
						AND `centre` = '%s'
						AND `team` = '%s')
					UNION
					SELECT 0, `heures` AS `normales`, 0, 0, `secteurs`, 0
					FROM `TBL_HEURES_A_PARTAGER`
					WHERE `date` = '%s'
					AND `centre` = '%s'
					AND `team` = '%s'
					UNION
					SELECT -1, SUM(`normales`) + SUM(instruction) AS `normales`, 0, 0, `secteurs`, 0
					FROM TBL_HEURES
					WHERE `date` = '%s'
					AND `uid` IN (SELECT `uid` FROM `TBL_AFFECTATION`
						WHERE '%s' BETWEEN `beginning` AND `end`
						AND `centre` = '%s'
						AND `team` = '%s'
						AND `grade` != 'c'
						AND `grade` != 'theo'
						AND `principale` IS FALSE)
					GROUP BY `secteurs`
					"
					, $date->date()
					, $date->date()
					, $affectation['centre']
					, $affectation['team']
					, $date->date()
					, $affectation['centre']
					, $affectation['team']
					, $date->date()
					, $date->date()
					, $affectation['centre']
					, $affectation['team']
				);
				$result = $_SESSION['db']->db_interroge($sql);
				while($row = $_SESSION['db']->db_fetch_assoc($result)) {
					 $array['heures'][$row['secteurs']][$row['uid']] = $row;
				}
				mysqli_free_result($result);				
				$array['heures']['date'] = $date->date();
			}
			$err = NULL;
			$_SESSION['token'] = $oldToken;
			$array['tk'] = $oldToken;
			break;
		/**
		 * Formulaire d'ajout d'information supplémentaires d'activité
		 */
		case 'IS':
			// FIXME Token invalide pour les infos supplémentaires...
			if (($_SESSION['utilisateur']->hasRole('my_edit') && $_SESSION['utilisateur']->checkUid($_REQUEST['uid']) || $_SESSION['utilisateur']->hasRole('teamEdit') )) {
				if (	array_key_exists('Year', $_REQUEST)
					&& array_key_exists('Month', $_REQUEST)
					&& array_key_exists('Day', $_REQUEST)
					&& array_key_exists('uid', $_REQUEST)
					&& $_REQUEST['uid'] == (int) $_REQUEST['uid']
					&& array_key_exists('info', $_REQUEST)) {
						$date = new Date();
						$date->annee($_REQUEST['Year']);
						$date->mois($_REQUEST['Month']);
						$date->jour($_REQUEST['Day']);
						$sql = sprintf("
							UPDATE `TBL_L_SHIFT_DISPO`
							SET `title` = '%s'
							WHERE `date` = '%s'
							AND `uid` = %d
							", $_SESSION['db']->db_real_escape_string($_REQUEST['info'])
							, $date->date()
							, (int) $_REQUEST['uid']
						);
						$_SESSION['db']->db_interroge($sql);
						$err = '';
				}
				// TODO Alimentation du compte récup
				if (array_key_exists('alimRcup', $_REQUEST) && $_REQUEST['alimRcup'] == 'on') {
				}
			}
			break;
		/**
		 * Liste les affectations possibles
		 *
		 * @return JSON
		 *
		 */
		case 'LA':
			if (calEvent::typeIsValid($_POST['t'])) {
				print(json_encode(Affectation::listeAllAffectations($_POST['t'])));
			} else {
				print(json_encode(Affectation::listeAllAffectations()));
			}
			return true;
			break;
		/**
		 * Liste les activités disponibles pour un utilisateur sur un jour
		 *
		 * @param
		 *
		 * @return JSON
		 */
		case 'LD':
			$date = new Date($_POST['date']);
			$utilisateur = new utilisateurGrille($_POST['uid']);
			$a = $utilisateur->listAvailableActivities($date);
			break;
		/**
		 * Passe le site en ligne ou hors-ligne
		 */
		case 'LK':
			if ($_SESSION['utilisateur']->hasRole('admin') && array_key_exists('r', $_POST)) {
				if ($_POST['r'] == 'on') {
					$status = "true";
				} else if ($_POST['r'] == "off") {
					$status = "false";
				} else {
					$err = "Empty query";
					exit; // FIXME le message ne parvient jamais à l'utilisateur
				}

				$sql = sprintf("UPDATE `TBL_CONSTANTS` SET `valeur` = '%s' WHERE `nom` = 'online'", $status);
				$_SESSION['db']->db_interroge($sql);
				$err = "";
			}
			break;
		/**
		 * Recherche une liste des utilisateurs de TeamTime à partir des premières lettres du nom
		 */
		case 'LU':
			if ($_SESSION['utilisateur']->hasRole('editeurs')) {
				$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("Listage des utilisateurs dont le nom commence par...", "TRACE", "ajax.php", "listeUser", "%s")'
					, $_SESSION['db']->db_real_escape_string(json_encode($_REQUEST)))
				);
				$users = array();
				$sql = sprintf("
					SELECT `nom`, `prenom`, `uid`
					FROM `TBL_USERS`
					WHERE `nom` LIKE '%%%s%%'
					", $_SESSION['db']->db_real_escape_string($_REQUEST['nom'])
				);
				$result = $_SESSION['db']->db_interroge($sql);
				while($row = $_SESSION['db']->db_fetch_assoc($result)) {
					 $users[] = $row;
				}
				mysqli_free_result($result);
				if (sizeof($users) > 0) {
					$array['users'] = $users;
					// On ne veut pas afficher de message
					$err = NULL;
				} else {
					die ('0');
				}
			}
			break;
		/**
		 * Ajout d'un remplaçant
		 */
		case 'RE':
			$date = new Date();
			$date->annee($_POST['Year']);
			$date->mois($_POST['Month']);
			$date->jour($_POST['Day']);
			$rempla = array(
				'uid'		=> $_POST['uid']
				,'date'		=> $date->date()
				,'nom'		=> $_POST['nom']
				,'phone'	=> $_POST['phone']
				,'email'	=> $_POST['email']
			);
			if (jourTravail::addRempla($rempla) == 1) {
				$err = NULL;
				$array['ok'] = 1;
			} else {
				$err = "Votre remplaçant n'a pas été enregistré...\nVérifiez que vous avez le droit de modifier cette entrée.";
			}
			break;
		/**
		 * Demande de réinitialisation du mot de passe
		 */
		case 'RP':
			// La chaîne 'email' est filtrée par la méthode resetPwd
			if ($_POST['email'] != '') {
				$err = utilisateurGrille::resetPwd($_POST['email']);
			}
			break;
		/**
		 * Enregistrement d'un nouvel utilisateur
		 */
		case 'RS':
			$array['q'] = 'RS';
			// Recherche la liste des affectations possibles
			$sql = "SELECT `nom`, `type`
				FROM `TBL_CONFIG_AFFECTATIONS`
				WHERE `type` = 'centre'
				OR `type` = 'team'";
			$result = $_SESSION['db']->db_interroge($sql);
			while($row = $_SESSION['db']->db_fetch_assoc($result)) {
				if ($row['type'] == 'centre') {
					$centres[] = $row['nom'];
				} else {
					$teams[] = $row['nom'];
				}
			}
			mysqli_free_result($result);
			// Vérifie l'existence des champs et les enregistre dans $row
			if (array_key_exists('iNom', $_POST)) {
				if (preg_match('/^[a-zA-Z]+$/', trim($_POST['iNom']))) {
					$row['nom'] = trim($_POST['iNom']);
					$array['ok'] = 1;
				} else {
					$array['ok'] = 0;
				}
			}
			if (array_key_exists('iPrenom', $_POST)) {
				if (preg_match('/^[a-zA-Z]+$/', trim($_POST['iNom']))) {
					$row['prenom'] = trim($_POST['iPrenom']);
					$array['ok'] = 1;
				} else {
					$array['ok'] = 0;
				}
			}
			if (array_key_exists('iEmail', $_POST)) {
				if ($row['email'] = filter_var(trim($_POST['iEmail']), FILTER_SANITIZE_EMAIL)) {
					$array['ok'] = 1;
				} else {
					$array['ok'] = 0;
				}
			}
			if (array_key_exists('centre', $_POST)) {
				if (in_array(trim($_POST['centre']), $centres)) {
					$centre = trim($_POST['centre']);
					$array['ok'] = 1;
				} else {
					$array['ok'] = 0;
				}
			}
			if (array_key_exists('team', $_POST)) {
				if (in_array(trim($_POST['team']), $teams)) {
					$team = trim($_POST['team']);
					$array['ok'] = 1;
				} else {
					$array['ok'] = 0;
				}
			}
			if (utilisateurGrille::emailAlreadyExistsInDb($row['email'])) {
				$err = "Un compte existe déjà avec cette adresse.";
				$array['ok'] = 0;
			} elseif ($array['ok'] == 1) {
				$_SESSION['db']->db_interroge(
					sprintf("INSERT INTO TBL_SIGNUP_ON_HOLD
					(`nom`, `prenom`, `email`, `centre`, `team`)
					VALUES
					('%s', '%s', '%s', '%s', '%s')
					", $_SESSION['db']->db_real_escape_string($row['nom'])
					, $_SESSION['db']->db_real_escape_string($row['prenom'])
					, $_SESSION['db']->db_real_escape_string($row['email'])
					, $_SESSION['db']->db_real_escape_string($centre)
					, $_SESSION['db']->db_real_escape_string($team)
					)
				);
				$err = "Votre compte a été créé. Vous recevrez un mail lorsque votre demande aura été traitée.\nVérifiez éventuellement dans vos spams.";
			} else {
				$err = "Votre compte n'a pas été créé. Vérifiez les données saisies.";
			}

			break;
		/**
		 * Recrée tous les utilisateurs dans la base de données
		 */
		case 'RU':
			if ($_SESSION['utilisateur']->hasRole('admin') ) {
				$sql = sprintf(
					"CALL __recreateAllUsersDb('%s', '%s')"
					, $GLOBALS['DSN']['user']['password']
					, $GLOBALS['DSN']['user']['hostname'] == 'localhost' ? 'localhost' : '%'
				);
				$_SESSION['db']->db_interroge($sql);
				$err = '';
			}
			break;
		/**
		 * Suppression de téléphone, adresse, affectation, activité...
		 */
		case 'SU':
			switch ($_REQUEST['r']) {
			case 'act':
				$_SESSION['db']->db_interroge(sprintf("DELETE FROM `TBL_DISPO` WHERE `did` = %d", $_REQUEST['did']));
				$err = "";
				$array['id'] = 'act' . $_REQUEST['did'];
				break;
			case 'phone' :
				$oPhone = new Phone( (int) $_REQUEST['id']);
				if (!empty($_SESSION['ADMIN']) || !empty($_SESSION['EDITEURS']) || !empty($_SESSION['TEAMEDIT']) || $_SESSION['utilisateur']->uid() == $oPhone->uid()) {
					$oPhone->delete();
					$err = "Mise à jour effectuée";
				} else {
					$err = "Désolé, vous ne pouvez pas faire cela...";
				}
				break;
			case 'adresse' :
				$oAdresse = new Adresse( (int) $_REQUEST['id']);
				if (!empty($_SESSION['ADMIN']) || !empty($_SESSION['EDITEURS']) || !empty($_SESSION['TEAMEDIT']) || $_SESSION['utilisateur']->uid() == $oAdresse->uid()) {
					$oAdresse->delete();
					$err = "Mise à jour effectuée";
				} else {
					$err = "Désolé, vous ne pouvez pas faire cela...";
				}
				break;
			case 'affectation' :
			case 'affectpale' :
				$oAffectation = new Affectation( (int) $_REQUEST['id']);
				if (!empty($_SESSION['ADMIN']) || !empty($_SESSION['EDITEURS']) || !empty($_SESSION['TEAMEDIT']) || $_SESSION['utilisateur']->uid() == $oAffectation->uid()) {
					$oAffectation->delete();
					$err = "Mise à jour effectuée";
				} else {
					$err = "Désolé, vous ne pouvez pas faire cela...";
				}
				break;
			case 'dispatchSchema' :
				$sql = sprintf("
					CALL suppressDispatchSchema(%d)
					", $_REQUEST['id']
				);
				$_SESSION['db']->db_interroge($sql);
				$err = "Mise à jour effectuée";
				break;
			case 'dispo' :
				if ($_SESSION['ADMIN']) {
					$sql = sprintf("DELETE FROM `TBL_DISPO`
						WHERE `did` = %d
						", $_GET['did']);
				} elseif ($_SESSION['EDITEURS']) {
					$sql = sprintf("DELETE FROM `TBL_DISPO`
						WHERE `did` = %d
						AND FIND_IN_SET('%s', `centres`)
						AND `team` = '%s'
						", $_GET['did']
						, $_SESSION['utilisateur']->centre()
						, $_SESSION['utilisateur']->team()
					);
				}
				if (isset($sql)) {
					$_SESSION['db']->db_interroge($sql);
					$err = "Mise à jour effectuée";
				} else {
					$err = "Désolé, vous ne pouvez pas faire cela...";
				}
				break;
			}
			break;
		/**
		 * Send Mailtest : envoie un mail de test
		 */
		case 'SM':
			if (Email::testemail()) {
				$err = "Message de test envoyé.";
			} else {
				$err = "Le message n'a pas été envoyé...";
			}
			break;
		/**
		 * Suppression de pereq
		 */
		case 'SP':
			if ($_SESSION['utilisateur']->hasRole('teamEdit')) {
				$sql = sprintf("DELETE FROM `TBL_L_SHIFT_DISPO`
					WHERE `sdid` = %d", $_POST['sdid']);
				$_SESSION['db']->db_interroge($sql);
				$err = '';
			} else {
				$err = "Vous n'avez pas les droits nécessaires...";
			}
			break;
		/**
		 * Year Toggle : change l'année d'un congé à partir de son sdid
		 */
		case 'YT':
			if ($_SESSION['utilisateur']->hasRole('teamEdit') || $_SESSION['utilisateur']->uid() == $_REQUEST['uid']) {
				$date = new Date($_REQUEST['d']);
				$_SESSION['db']->db_interroge(sprintf("
					CALL toggleAnneeConge(%d, '%s')
					", $_REQUEST['uid']
					, $date->date()));
				$err = '';
			}
			break;
		}
	}
}

if ($err !== "") {
	$array['msg'] = nl2br(htmlspecialchars($err));
} elseif (!is_null($err)) {
	$array['msg'] = htmlspecialchars("Mise à jour effectuée.");
	$array['ok'] = 1;
}

print (json_encode($array));

/*
 * Informations de debug
 */
include 'debug.inc.php';
firePhpLog($conf, '$conf');
firePhpLog(debug::getInstance()->format(), 'format debug messages');
firePhpLog($javascript, '$javascript');
firePhpLog($stylesheet, '$stylesheet');


ob_end_flush(); // Obligatoire pour firePHP

?>
