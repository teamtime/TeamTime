<?php
// update_bfid_vsid_pcid.php
//
// Effectue les opérations nécessaires à l'utilisation de calEvent

/*
	TeamTime is a software to manage people working in team on a cyclic shift.
	Copyright (C) 2012 Manioul - webmaster@teamtime.me

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

ini_set('max_excecution_time', 500); // 500 secondes pour exécuter le script

// Require authenticated user
// L'utilisateur doit être logué pour accéder à cette page
$requireAdmin = true;


/*
 * Configuration de la page
 * Définition des include nécessaires
 */
	$conf['page']['include']['constantes'] = 1; // Ce script nécessite la définition des constantes
	$conf['page']['include']['errors'] = 1; // le script gère les erreurs avec errors.inc.php
	$conf['page']['include']['class_debug'] = 1; // La classe debug est nécessaire à ce script
	$conf['page']['include']['globalConfig'] = 1; // Ce script nécessite config.inc.php
	$conf['page']['include']['init'] = 1; // la session est initialisée par init.inc.php
	$conf['page']['include']['globals_db'] = 1; // Le DSN de la connexion bdd est stockée dans globals_db.inc.php
	$conf['page']['include']['class_db'] = 1; // Le script utilise class_db.inc.php
	$conf['page']['include']['session'] = 1; // Le script utilise les sessions par session.imc
	$conf['page']['include']['classUtilisateur'] = NULL; // Le sript utilise uniquement la classe utilisateur (auquel cas, le fichier class_utilisateur.inc.php
	$conf['page']['include']['class_utilisateurGrille'] = 1; // Le sript utilise la classe utilisateurGrille
	$conf['page']['include']['class_cycle'] = 1; // La classe cycle est nécessaire à ce script (remplace grille.inc.php
	$conf['page']['include']['class_menu'] = 1; // La classe menu est nécessaire à ce script
	$conf['page']['include']['smarty'] = 1; // Smarty sera utilisé sur cette page
	$conf['page']['compact'] = false; // Compactage des scripts javascript et css
	$conf['page']['include']['bibliothequeMaintenance'] = false; // La bibliothèque des fonctions de maintenance est nécessaire
/*
 * Fin de la définition des include
 */


/*
 * Configuration de la page
 */
        $conf['page']['titre'] = sprintf("Mise à jour passage calEvent"); // Le titre de la page
// Définit la valeur de $DEBUG pour le script
// on peut activer le debug sur des parties de script et/ou sur certains scripts :
// $DEBUG peut être activer dans certains scripts de required et désactivé dans d'autres
	$DEBUG = false;

	/*
	 * Choix des éléments à afficher
	 */
	
	// Affichage du menu horizontal
	$conf['page']['elements']['menuHorizontal'] = true;
	// Affichage messages
	$conf['page']['elements']['messages'] = true;
	// Affichage du choix du thème
	$conf['page']['elements']['choixTheme'] = false;
	// Affichage du menu d'administration
	$conf['page']['elements']['menuAdmin'] = false;
	
	// éléments de debug
	
	// FirePHP
	$conf['page']['elements']['firePHP'] = false;
	// Affichage des timeInfos
	$conf['page']['elements']['timeInfo'] = $DEBUG;
	// Affichage de l'utilisation mémoire
	$conf['page']['elements']['memUsage'] = $DEBUG;
	// Affichage des WherewereU
	$conf['page']['elements']['whereWereU'] = $DEBUG;
	// Affichage du lastError
	$conf['page']['elements']['lastError'] = $DEBUG;
	// Affichage du lastErrorMessage
	$conf['page']['elements']['lastErrorMessage'] = $DEBUG;
	// Affichage des messages de debug
	$conf['page']['elements']['debugMessages'] = $DEBUG;



	// Utilisation de jquery
	$conf['page']['javascript']['jquery'] = false;
	// Utilisation de grille2.js.php
	$conf['page']['javascript']['grille2'] = false;
	// Utilisation de utilisateur.js
	$conf['page']['javascript']['utilisateur'] = false;

	// Feuilles de styles
	// Utilisation de la feuille de style general.css
	$conf['page']['stylesheet']['general'] = true;
	$conf['page']['stylesheet']['grille'] = false;
	$conf['page']['stylesheet']['grilleUnique'] = false;
	$conf['page']['stylesheet']['utilisateur'] = false;

	// Compactage des pages
	$conf['page']['compact'] = false;
	
/*
 * Fin de la configuration de la page
 */

require 'required_files.inc.php';

/**
 * Exécute une requête SQL et affiche la requête
 *
 * param $db object base de données
 * param $sql string la requête à effectuer
 */
function update($db, $sql) {
	print("$sql<br>");
	$db->db_interroge($sql);
}
$db = new database($GLOBALS['DSN']['admin']); // Les utilisateurs de la base autres que admin n'ont pas les droits requis.
?>
<h2>Pensez à vous <a href="logout.php">déconnecter</a> à la fin de ce script</h2>
<?php

// Un peu de ménage dans les tables qui vont être remplacées par TBL_CAL_EVENTS (suppression des date 0000-00-00)
$sql = "DELETE FROM `TBL_VACANCES_SCOLAIRES` WHERE dateD = 0 OR dateF = 0";
print("$sql<br>");
$db->db_interroge($sql);
$sql = "DELETE FROM `TBL_BRIEFING` WHERE dateD = 0 OR dateF = 0";
print("$sql<br>");
$db->db_interroge($sql);
$sql = "DELETE FROM `TBL_PERIODE_CHARGE` WHERE dateD = 0 OR dateF = 0";
print("$sql<br>");
$db->db_interroge($sql);
$sql = "DROP TABLE IF EXISTS `TBL_CAL_EVENTS`;";
$db->db_interroge($sql);
$sql = "CREATE TABLE `TBL_CAL_EVENTS` ("
	. " `id` int(11) NOT NULL AUTO_INCREMENT,"
	. " `uid` int(11) NULL,"
	. " `dateD` date NOT NULL,"
	. " `dateF` date NOT NULL,"
	. " `description` varchar(256) NOT NULL,"
	. " `type` set('bfid','vsid','pcid','msid') NOT NULL,"
	. " `centres` TEXT NULL,"
	. " `teams` TEXT NULL,"
	. " `uids` TEXT NULL,"
	. " PRIMARY KEY (`id`)"
	. ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
print("$sql<br>");
$db->db_interroge($sql);
$sql = "INSERT INTO TBL_CAL_EVENTS (SELECT NULL, 5, dateD, dateF, description, 'bfid', 'LFFFE,LFFFW', team, NULL FROM TBL_BRIEFING WHERE dateD != 0 AND dateF != 0);";
print("$sql<br>");
$db->db_interroge($sql);
$sql = "INSERT INTO TBL_CAL_EVENTS (SELECT NULL, 5, dateD, dateF, description, 'vsid', 'LFFFE,LFFFW,LFPO', team, NULL FROM TBL_VACANCES_SCOLAIRES);";
print("$sql<br>");
$db->db_interroge($sql);
$sql = "INSERT INTO TBL_CAL_EVENTS (SELECT NULL, 5, dateD, dateF, description, 'pcid', 'LFFFE,LFFFW', team, NULL FROM TBL_PERIODE_CHARGE);";
print("$sql<br>");
$db->db_interroge($sql);
// Ajoute les briefings présents dans la grille et absents de TBL_BRIEFING
$sql = "INSERT INTO `TBL_CAL_EVENTS`
	SELECT NULL, 5, MIN(`date`) AS `dateD`, MAX(`date`) AS `dateF`, `briefing` AS `description`, 'bfid', 'LFFFE,LFFFW', 'all', NULL
	FROM `TBL_GRILLE`
	WHERE (`date` BETWEEN '2015-05-29' AND '2015-09-27'
		OR `date` BETWEEN '2015-10-10' AND NOW())
	AND `briefing` != ''
	GROUP BY `briefing`
	";
print("$sql<br>");
$db->db_interroge($sql);

print("Sleeping...<br>");
sleep(15);

$sql = "DROP TABLE `TBL_BRIEFING`;";
print("$sql<br>");
$db->db_interroge($sql);
$sql = "DROP TABLE `TBL_VACANCES_SCOLAIRES`;";
print("$sql<br>");
$db->db_interroge($sql);
$sql = "DROP TABLE `TBL_PERIODE_CHARGE`;";
print("$sql<br>");
$db->db_interroge($sql);
$sql = "ALTER TABLE `TBL_GRILLE` DROP `briefing`";
print("$sql<br>");
$db->db_interroge($sql);
$sql = "ALTER TABLE `TBL_GRILLE` CHANGE `pcid` `pcid` INT NULL, CHANGE `vsid` `vsid` INT NULL, ADD `bfid` INT NULL AFTER `vsid`";
print("$sql<br>");
$db->db_interroge($sql);
$sql = "ALTER TABLE `TBL_ELEMS_MENUS` CHANGE `creation` `creation` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP";
print("$sql<br>");
$db->db_interroge($sql);
$sql = "ALTER TABLE `TBL_ELEMS_MENUS` CHANGE `description` `description` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT ''";
print("$sql<br>");
$db->db_interroge($sql);

print("Sleeping...<br>");
sleep(5);

$sql = "SHOW TABLES";
print("$sql<br>");
$result = $db->db_interroge($sql);
$array = array();
while($row = $db->db_fetch_array($result)) {
	if (preg_match('/TBL_/', $row[0])) {
		$array[] = $row[0];
	}
}
mysqli_free_result($result);
print('Libération des résultats de la requête<br>');
foreach ($array as $table) {
	$sql = "SHOW COLUMNS FROM `$table` WHERE `Field` LIKE '%centre%' OR `Field` LIKE '%team%'";
	print("$sql<br>");
	$result = $db->db_interroge($sql);
	$test = false;
	while ($row = $db->db_fetch_array($result)) {
		$test = true;
		if (preg_match('/centre/', $row[0])) {
			$centre = $row[0];
		} else {
			$team = $row[0];
		}
	}
	mysqli_free_result($result);
	print('Libération des résultats de la requête<br>');
	if ($test) {
		printf ("%s => %s, %s<br>", $table, $centre, $team);
		$sql = sprintf("UPDATE `%s`
			SET `%s` = 'LFFFE'
			WHERE
			`%s` LIKE '%%athis%%'
			AND `%s` LIKE '%%e%%'
			", $table
			, $centre
			, $centre
			, $team
		);
		print("$sql<br>");
		$db->db_interroge($sql);
		$sql = sprintf("UPDATE `%s`
			SET `%s` = 'LFFFW'
			WHERE
			`%s` LIKE '%%athis%%'
			AND `%s` LIKE '%%w%%'
			", $table
			, $centre
			, $centre
			, $team
		);
		print("$sql<br>");
		$db->db_interroge($sql);
		for ($i=1; $i<=12; $i++) {
			$sql = "UPDATE `$table` SET `$team` = '$i' WHERE `$team` = '${i}e' OR `$team` = '${i}w'";
			print("$sql<br>");
			$db->db_interroge($sql);
		}
		$sql = sprintf("UPDATE `%s`
			SET `%s` = 'LFFFE'
			WHERE
			`%s` LIKE '%%athis%%'
			AND `%s` = 'all'
			", $table
			, $centre
			, $centre
			, $team
		);
		print("$sql<br>");
		$db->db_interroge($sql);
	}
}
$sql = "UPDATE TBL_CYCLE SET centre = 'LFFFE' WHERE centre = 'athis'";
print("$sql<br>");
$db->db_interroge($sql);
$sql = "INSERT INTO TBL_CYCLE SELECT NULL, vacation, horaires, rang, 'LFFFW', team FROM TBL_CYCLE WHERE centre = 'LFFFE'";
print("$sql<br>");
$db->db_interroge($sql);
$sql = "UPDATE TBL_GRILLE SET cid = cid + 24 WHERE centre = 'LFFFW'";
print("$sql<br>");
$db->db_interroge($sql);
$sql = "DELETE FROM `TBL_CONFIG_AFFECTATIONS` WHERE `type` = 'team' AND `nom` LIKE '%w%'";
print("$sql<br>");
$db->db_interroge($sql);
$sql = "TRUNCATE `TBL_CONFIG_AFFECTATIONS`";
$db->db_interroge($sql);
$sql = "INSERT INTO `TBL_CONFIG_AFFECTATIONS`
	(`caid`, `type`, `nom`, `description`)
	VALUES
	(NULL, 'centre', 'LFFFE', 'CRNA-Nord - Sub Est'),
	(NULL, 'centre', 'LFFFW', 'CRNA-Nord - Sub Ouest'),
	(NULL, 'centre', 'LFEE', 'CRNA-Est'),
	(NULL, 'centre', 'LFMME', 'CRNA-Sud-Est - Sub Est'),
	(NULL, 'centre', 'LFMMW', 'CRNA-Sub-Est - Sub Ouest'),
	(NULL, 'centre', 'LFRR', 'CRNA-Ouest'),
	(NULL, 'centre', 'LFBB', 'CRNA-Sud-Ouest'),
	(NULL, 'centre', 'LFPO', 'TWR Orly'),
	(NULL, 'grade', 'c', 'Élève'),
	(NULL, 'grade', 'theo', 'Élève ayant obtenu son théorique'),
	(NULL, 'grade', 'pc', 'Premier contrôleur'),
	(NULL, 'grade', 'fmp', 'FMPiste'),
	(NULL, 'grade', 'dtch', 'Contrôleur détaché'),
	(NULL, 'grade', 'ce', 'Chef d''équipe'),
	(NULL, 'grade', 'cds', 'Chef de salle'),
	";
for ($i=1; $i<=12; $i++) {
	$sql .= "(NULL, 'team', '$i', 'Équipe $i'),";
}
$db->db_interroge(substr($sql, 0, -1));

// Modifie TBL_DISPO, le champ centre devient centres
$sql = "ALTER TABLE `TBL_DISPO` CHANGE `centre` `centres` VARCHAR(50) NOT NULL";
print("$sql<br>");
$db->db_interroge($sql);
// et étend les dispo LFFFE toutes équipes à LFFFW
$sql = "UPDATE `TBL_DISPO`
	SET `centres` = 'LFFFE,LFFFW'
	WHERE `team` = 'all'";
$db->db_interroge($sql);

$sql = "SELECT * FROM `TBL_CAL_EVENTS`";
print("$sql<br>");
$result = $db->db_interroge($sql);
while($row = $db->db_fetch_assoc($result)) {
	$calEvent = new calEvent($row);
	$calEvent->updateGrille();
}
print('Libération des résultats de la requête<br>');
mysqli_free_result($result);

// Passe Vex en toutes équipes pour gérer les titres de congés
//
// Pour LFFFE 10
update($db, "CALL moveDid(32, 111)");
// Pour LFFFW 9
update($db, "CALL moveDid(32, 274)");
// Pour LFFFE 8
update($db, "CALL moveDid(32, 187)");
// Fixe à 50 le nombre de congés exceptionnels autorisés jusqu'au 31 décembre
update($db, "UPDATE `TBL_DISPO`
	SET `quantity` = '50',
	`date limite depot` = '01-01',
	`centres` = 'LFFFE,LFFFW',
	`team` = 'all',
	`title` = 1
	WHERE `TBL_DISPO`.`did` = 32");


// Ajoute un menu d'aide
update($db, "INSERT INTO `ttm`.`TBL_MENUS`
	(`idx`, `titre`, `description`, `parent`, `creation`, `modification`, `allowed`, `actif`, `type`)
	VALUES
	(NULL, 'support', 'aide et support', '1', NOW(), CURRENT_TIMESTAMP, 'all', '1', NULL)");
update($db, "INSERT INTO `ttm`.`TBL_ELEMS_MENUS` (`idx`, `titre`, `description`, `lien`, `sousmenu`, `creation`, `modification`, `allowed`, `actif`) VALUES
	(NULL, 'support', 'de l''aide', '', (SELECT `idx` FROM `TBL_MENUS` WHERE `titre` = 'support'), NOW(), CURRENT_TIMESTAMP, 'all', TRUE)
	, (NULL, 'wiki', 'la page d''aide', 'https://dev.titoux.info/projects/teamtime-core/wiki', NULL, NOW(), CURRENT_TIMESTAMP, 'all', TRUE)
	, (NULL, 'report de bug', '', 'https://dev.titoux.info/projects/teamtime-core/issues/new', NULL, NOW(), CURRENT_TIMESTAMP, 'all', TRUE)
	");
update($db, "INSERT INTO `TBL_MENUS_ELEMS_MENUS` (`idxm`, `idxem`, `position`)
	VALUES (1, (SELECT `idx` FROM `TBL_ELEMS_MENUS` WHERE `titre` = 'support'), 6)");
update($db, "INSERT INTO `TBL_MENUS_ELEMS_MENUS` (`idxm`, `idxem`, `position`)
	VALUES ((SELECT `idx` FROM `TBL_MENUS` WHERE `titre` = 'support'), (SELECT `idx` FROM `TBL_ELEMS_MENUS` WHERE `titre` = 'wiki'), 10)");

update($db, "INSERT INTO `TBL_MENUS_ELEMS_MENUS` (`idxm`, `idxem`, `position`)
	VALUES ((SELECT `idx` FROM `TBL_MENUS` WHERE `titre` = 'support'), (SELECT `idx` FROM `TBL_ELEMS_MENUS` WHERE `titre` = 'report de bug'), 20)");

update($db, "ALTER TABLE `TBL_USERS` ADD `annuaire` BOOLEAN NOT NULL DEFAULT TRUE COMMENT 'L\'utilisateur veut-il paraitre dans l\'annuaire' AFTER `pref`;");

unset($db);


// Affichage du bas de page
$smarty->display('footer.tpl');

?>
