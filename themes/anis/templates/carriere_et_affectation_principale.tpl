{* Smarty *}
<form name="" id="" class="" method="POST" action="monCompte.php">
<fieldset id="affectpale"><legend>Affectations principales<div class="flechebas" onclick="$(this).toggleClass('flechehaut');$(this).toggleClass('flechebas');$('#captabpal').toggle();"></div></legend>
<table id="captabpal" class="altern-row" style="width:850px;display:none;">
<thead>
<tr>
<td>Centre</td><td>Équipe</td><td>Début</td><td>Fin</td><td></td>
</tr>
</thead>
<tbody>
{foreach $datas as $carriere}
{if $carriere->principale()}
<tr id="affectation{$carriere->aid()}" class="pale" title="Poids : {$carriere->poids()}">
<td>{$carriere->centreDisplay()}</td>
<td>{$carriere->teamDisplay()}</td>
<td>{$carriere->beginning()->formatDate()}</td>
<td>{$carriere->end()->formatDate()}</td>
<td><div class="imgwrapper12" style="left:5px;cursor:pointer;" onclick='supprInfo("affectpale", {$carriere->aid()}, {$utilisateur->uid()});' title="Supprimer l'entrée"><img class="cnl" alt="supprimer" src="{$image}" /></div></td>
</tr>
{/if}
{/foreach}
</tbody>
</table>
</fieldset>
</form>

