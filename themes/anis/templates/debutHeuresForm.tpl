{* Smarty *}
<form id="fIntervalHeures" class="ng w24" method="POST" action="">
Dernières heures calculées : <strong>{$lastHeures}</strong>
<ul>
<li>
<fieldset>
<ul>
<li>
<label for="dateDebut">Début des heures</label>
<input type="date" id="dateD" name="dateD" {if !empty($defaultD)}value="{$defaultD}" {/if}/>
</li>
<li>
<label for="dateFin">Fin des heures</label>
<input type="date" id="dateF" name="dateF" {if !empty($defaultF)}value="{$defaultF}" {/if}/>
</li>
</fieldset>
</li>
<li>
<input type="submit" value="Afficher les heures" onclick="$('#fIntervalHeures').attr('action', '#');" />
</li>
<li>
<input type="submit" value="Exporter vers un tableur" title="Exporte les heures au format csv." onclick="$('#fIntervalHeures').attr('action', 'exportcsvhours.php');" />
</li>
<li class="bouton" onclick="window.print();">Imprimer les heures</li>
</ul>
</form>
