{* Smarty *}
{* Affiche les dates pour lesquelles les heures sont à caculer et déjà calculées *}
{foreach $heuresCalc as $centre => $teams}
<table class="w24 altern-row" style="display:block;float:left;margin:10px;">
<thead><tr><th>Équipe</th><th title="Premières heures saisies non distribuées">Non distribuées</th><th>Heures calculées</th></tr></thead><tbody>
{foreach $teams as $team => $arr}
<tr><td>{$team}</td><td>{$arr.undone}</td><td>{$arr.done}</td></tr>
{/foreach}
</tbody>
<caption style="font-size:200%;">{$centre}</caption>
</table>
{/foreach}
