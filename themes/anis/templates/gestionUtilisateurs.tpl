{* Smarty *}
<div>
{if $affectations|@count > 0}
<form id="fFilter" name="fFilter" method="get" action="#">
</form>
{/if}
<form name="fLU" method="POST" action="">
<table class="altern-row genElem">
{foreach from=$usersInfos key=lineNb item=infos name=lineBoucle}
{if $lineNb == 0}<thead>{elseif $lineNb == 1}</thead>
<tbody>{/if}
<tr id="line{$lineNb}">
{foreach from=$infos key=Field item=value}
{if $lineNb == 0}<th>{else}<td id="{$Field}{$infos.uid}">{/if}{if $Field == 'uid' && $lineNb != 0}<input type="checkbox" name="uid[]" value="{$value}" />&nbsp;<a href="monCompte.php?uid={$value}">{$value}</a>&nbsp;<a href="" id="suppr{$infos.uid}">-</a>{else}{$value}{/if}{if $lineNb == 0}</th>{else}</td>{/if}
{/foreach}
</tr>
{/foreach}
</tbody>
</table>
<div class='ng w24'>
{include file="html.form.select.tpl" select=Affectation::listeAffectations('centre')}
{include file="html.form.select.tpl" select=Affectation::listeAffectations('team')}
<label for="pale">Affectation principale</label><input type="checkbox" name="pale" id="pale" onchange="$('#grade').fadeToggle(100)" />
{include file="html.form.select.tpl" select=Affectation::listeAffectations('grade')}
<br />
<input type="date" name="dateD" id="dateD" placeholder="Début" title="Date de début dans l'affectation" />
<input type="date" name="dateF" id="dateF" placeholder="Fin" title="Date de fin dans l'affectation" />
<input type="submit" value="Confirmer" />
</div>
</form>
</div>
