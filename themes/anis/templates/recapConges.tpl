{* Smarty *}
<div class="bloc" style="margin:0 20px;min-height:308px;width:430px;z-index:100;"><h3><a href="?uid={$smarty.get.uid}&year={$year-1}">&lt;</a> Récapitulatif {$year} <a href="?uid={$smarty.get.uid}&year={$year+1}">&gt;</a></h3>
<table style="margin:20px 0;" class="altern-row">
<thead>
<tr>
<th>Nom</th><th>Congé</th><th>Congé (long)</th><th>Année</th><th>Nombre posé</th><th>Reliquat</th><th>Quota</th>
</tr>
</thead>
<tbody>
{foreach $decompte as $vacances}

<tr{if $vacances['reliquat'] < 0} class="warning"{/if}>
<td><a href="?uid={$vacances['uid']}&year={$year}">{$vacances['nom']}</a></td><td>{$vacances['dispo']}</td><td>{$vacances['nom_long']}</td><td>{$vacances['year']}</td><td>{$vacances['déposé']}</td><td>{$vacances['reliquat']}</td><td>{$vacances['quantity']}</td>
</tr>
{/foreach}
</tbody>
</table>
</div>
