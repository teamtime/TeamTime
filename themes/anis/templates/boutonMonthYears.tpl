{* Smarty *}

{* Dessine des boutons permettant de choisir un mois et une année *}
<ul class='bmy noprint'>
{foreach $months as $mois => $dates}
	<a href="?d={$dates.debut}&amp;f={$dates.fin}&amp;p=1"><li class="bouton">{$mois}</li></a>
{/foreach}
</ul>
