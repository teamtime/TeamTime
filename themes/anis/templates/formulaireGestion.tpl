{* Smarty *}
{foreach $titres as $id => $titre}
<div class="dBriefing">
<h2 onclick="$('#dBriefing{$id}').slideToggle('slow');" id="button-dBriefing{$id}" class="bouton">Gestion de {$titre}</h2>
<div id="dBriefing{$id}" style="display:none;margin:15px 0;">
{if isset($datas[$id])}
{foreach $datas[$id] as $brief}
{if $brief@first}
<h3>Liste actuelle des {$titre}</h3>
<table class="altern-row">
<thead>
<tr>
<th>Description</th><th>Période</th><th>Annonceur</th><th>Centres</th><th>Équipes</th><th>uids</th><th>Suppression</th>
</tr>
</thead>
<tbody>
{/if}
<tr>
<td>{$brief->description()}</td>
<td> du <a href="affiche_grille.php?dateDebut={$brief->dateD()}">{$brief->dateD()}</a> au <a href="affiche_grille.php?dateDebut={$brief->dateF()}">{$brief->dateF()}</a></td>
<td>{$brief->uid()}</td>
<td>{$brief->centres_as_csv()}</td>
<td>{$brief->teams_as_csv()}</td>
<td>{$brief->uids_as_csv()}</td>
<td><a href="gestion.php?id={$brief->id()}"><div class="imgwrapper12"><img class="cnl" alt="supprimer" src="{$image}" /></div></a></td>
</tr>
{/foreach}
</tbody>
</table>
{/if}
<h3 class="bouton" onclick="addFormCalEvent('{$id}')" id="button-dBriefing{$id}-addForm">Ajouter des {$titre}</h3>
</div>
</div>
{/foreach}
