{* Smarty *}
<table class="altern-row panel">
{foreach $activites as $activite}
{if $activite@first}<thead>{elseif $activite@iteration == 2}</thead><tbody>{/if}
<tr id="act{$activite.did}">
{foreach $activite as $field}
{if $field@iteration > 1}
{if $field@last && $activite@iteration > 1}
{* L'entrée est-elle active ? *}
<td><a href="?did={$activite.did}&amp;a={!$field}">{$field}</a></td>
{else}
<td>{$field}</td>
{/if}
{/if}
{/foreach}
{if $activite@first}
<td></td>
<td></td>
<td></td>
<td></td>
{else}
<td><form class="supDid">
<input type="hidden" name="ajax" value="true" />
<input type="hidden" name="q" value="SU" />
<input type="hidden" name="r" value="act" />
<input type="hidden" name="did" value="{$activite.did}" />
<div class="imgwrapper12" title="Supprimer"><img class="cnl" alt="supprimer" src="{$image}" title="Supprimer" /></div>
</form></td>
<td class="pointer" onclick="edAct({$activite.did})">Éditer</td>
<td>{if $activite@iteration > 2}<a href="?did={$activite.did}&amp;u=1&amp;poids={$activite.poids}">Remonter</a>{/if}</td>
<td>{if !$activite@last}<a href="?did={$activite.did}&amp;d=1&amp;poids={$activite.poids}">Descendre</a>{/if}</td>
{/if}
</tr>
{/foreach}
</tbody>
</table>
