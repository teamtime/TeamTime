{* Smarty *}
<div class="centre"><span class="underline">Agent :</span> {$nom|upper}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="underline">Centre :</span> {$centre|upper}</div>
<table id="listeHeures" class="altern-row">
<caption>Listing des heures du {$dateDebut} au {$dateFin}</caption>
<thead>
<tr>
<th class="noprint">Nom</th>
<th>Date</th>
<th>Heures normales</th>
<th>Heures instruction</th>
<th>Heures simulateur</th>
<th>Heures double</th>
<th>Occurrences</th>
<th class="noprint">Hide</th>
<th class="noprint">Suppr</th>
</tr>
</thead>
<tbody>
{foreach $heures as $date => $aHeure}
<tr>
<td class="noprint">{$nom|upper}</td>
<td><a href="heuresJour.php?d={$aHeure.global.date}">{$aHeure.global.date}</a></td>
<td>{$aHeure.global.normales}</td>
<td>{$aHeure.global.instruction}</td>
<td>{$aHeure.global.simulateur}</td>
<td>{$aHeure.global.double}</td>
<td>{$aHeure.global.occurrence}</td>
<td class="noprint"></td>
<td class="noprint"></td>
</tr>
{/foreach}
<tr>
<td class="noprint">{$nom|upper}</td>
<td>TOTAL</td>
<td>{$totaux.normales}</td>
<td>{$totaux.instruction}</td>
<td>{$totaux.simulateur}</td>
<td>{$totaux.double}</td>
<td>{$totaux.occurrence}</td>
<td colspan="2" class="noprint"></td>
</tbody>
</table>
{if $smarty.get.p == 1}<script>
$(function() { window.print(); });
</script>{/if}
