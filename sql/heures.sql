SET names 'utf8';
-- Création des tables si elles n'existent pas

CREATE TABLE IF NOT EXISTS TBL_HEURES (
	uid INT(11) NOT NULL,
	did INT(11) NOT NULL,
	date DATE NOT NULL,
	normales DECIMAL(4,2) NOT NULL,
	instruction DECIMAL(4,2) NOT NULL,
	simulateur DECIMAL(4,2) NOT NULL,
	statut ENUM('fixed', 'shared', 'unattr') DEFAULT 'unattr',
	PRIMARY KEY (uid, date)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- La table temporaire pour distribuer les heures doit être recréée
DROP TABLE IF EXISTS tmpPresents;

CREATE TABLE IF NOT EXISTS `TBL_HEURES_A_PARTAGER` (
	  `centre` varchar(50) NOT NULL,
	  `team` varchar(10) NOT NULL,
	  `date` date NOT NULL,
	  `heures` decimal(4,2) NOT NULL,
	  `dispatched` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Positionné lorsque les heures ont été calculées',
	  `writable` tinyint(1) NOT NULL DEFAULT '1',
	  PRIMARY KEY (`centre`,`team`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Le nombre d''heures à partager par jour';

CREATE TABLE IF NOT EXISTS `TBL_DISPATCH_HEURES` (
	  `rid` int(11) NOT NULL AUTO_INCREMENT,
	  `cids` varchar(64) NOT NULL,
	  `centre` varchar(50) NOT NULL DEFAULT 'athis',
	  `team` varchar(10) NOT NULL DEFAULT '9e',
	  `grades` varchar(60) NOT NULL DEFAULT 'pc',
	  `dids` varchar(128) DEFAULT NULL,
	  `type` enum('norm','instru','simu') NOT NULL,
	  `statut` enum('shared','fixed') NOT NULL COMMENT 'Les heures sont partagées ou fixes',
	  `heures` decimal(4,2) NOT NULL COMMENT 'Nombre d''heures allouées',
	  `ordre` INT NOT NULL COMMENT 'définit la précédence des règles',
	  PRIMARY KEY (`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `TBL_DISPATCH_HEURES_USER` (
	  `rid` int(11) NOT NULL,
	  `cycles` varchar(64) NOT NULL,
	  `centre` varchar(50) NOT NULL DEFAULT 'athis',
	  `team` varchar(10) NOT NULL DEFAULT '9e',
	  `grades` varchar(60) NOT NULL DEFAULT 'pc',
	  `dispos` varchar(128) DEFAULT NULL,
	  `type` enum('norm','instru','simu') NOT NULL,
	  `statut` enum('shared','fixed') NOT NULL COMMENT 'Les heures sont partagées ou fixes',
	  `heures` decimal(4,2) NOT NULL COMMENT 'Nombre d''heures allouées (en décimal)',
	  PRIMARY KEY (`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP VIEW IF EXISTS VIEW_HEURES_PAR_JOUR;
CREATE VIEW VIEW_HEURES_PAR_JOUR AS
    SELECT uid, nom, prenom, date, SUM(normales) AS normales, SUM(instruction) AS instruction, SUM(simulateur) AS simulateur, SUM(`double`) AS `double`
    FROM TBL_HEURES AS h
    NATURAL JOIN TBL_USERS AS u
    GROUP BY date, uid;
-- ALTER TABLE `TBL_HEURES` ADD `secteurs` VARCHAR(10) NULL DEFAULT NULL COMMENT 'Secteur ou groupement de secteurs auquel sont attribuées les heures.' AFTER `statut`;
-- ALTER TABLE `TBL_DISPATCH_HEURES` DROP `statut`;

DELIMITER |
DROP PROCEDURE IF EXISTS dispatchAllHeures|
CREATE PROCEDURE dispatchAllHeures ( IN centre_ VARCHAR(50) , IN team_ VARCHAR(10) )
BEGIN
	DECLARE done BOOLEAN DEFAULT 0;
	DECLARE dateTD DATE;
	DECLARE curDatesToDispatch CURSOR FOR SELECT date
		FROM TBL_HEURES_A_PARTAGER
		WHERE centre = centre_
		AND team = team_
		AND dispatched IS FALSE
		AND writable IS TRUE;
	
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	OPEN curDatesToDispatch;

	REPEAT
	FETCH curDatesToDispatch INTO dateTD;
	IF NOT done THEN
		CALL dispatchOneDayHeures( centre_ , team_ , dateTD );
	END IF;
	UNTIL done END REPEAT;

	CLOSE curDatesToDispatch;
END|
DROP PROCEDURE IF EXISTS dispatchHeuresBetween|
CREATE PROCEDURE dispatchHeuresBetween( IN centre_ CHAR(50) , IN team_ CHAR(10) , IN debut DATE , IN fin DATE )
BEGIN
	DECLARE done BOOLEAN DEFAULT 0;
	DECLARE current DATE;
	DECLARE curDate CURSOR FOR SELECT date
		FROM TBL_HEURES_A_PARTAGER
		WHERE centre = centre_
		AND team = team_
		AND date BETWEEN debut AND fin;

	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	OPEN curDate;

	REPEAT
	FETCH curDate INTO current;
	IF NOT done THEN
		-- DEBUG
		-- SELECT CONCAT('Traitement de ', current) AS Info;
		-- DEBUG
		CALL dispatchOneDayHeures( centre_, team_, current);
	END IF;
	UNTIL done END REPEAT;

	CLOSE curDate;
END|
-- Distribue les heures pour une journée et pour un secteur à tous les utilisateurs de l'équipe présents
DROP PROCEDURE IF EXISTS dispatchOneDayHeuresSecteur|
CREATE PROCEDURE dispatchOneDayHeuresSecteur (IN centre_ CHAR(50) , IN team_ CHAR(10) , IN secteur VARCHAR(10) , IN nbEleves SMALLINT(6) , IN date_ DATE)
BEGIN
	DECLARE done BOOLEAN DEFAULT 0;
	DECLARE heuresFixes, heuresTotales, norm, instr, simu, doub DECIMAL(6, 2);
	DECLARE uid_, nbPc, nbInstruits, nbInstructeurs, heuresInstructionDistribuees, reliquat SMALLINT(6);
	DECLARE curPresents CURSOR FOR SELECT uid FROM tmpPresents;
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	-- Table temporaire listant tous les utilisateurs présents ayant droit à des heures (élèves compris)
	DROP TEMPORARY TABLE IF EXISTS tmpPresentsHeures;
	CREATE TEMPORARY TABLE tmpPresentsHeures (
		uid SMALLINT(6) NOT NULL,
		grade VARCHAR(64) NOT NULL,
		did SMALLINT(6) NULL,
		normales DECIMAL(4,2) NOT NULL DEFAULT 0,
		instruction DECIMAL(4,2) NOT NULL DEFAULT 0,
		simulateur DECIMAL(4,2) NOT NULL DEFAULT 0,
		`double` DECIMAL(4,2) NOT NULL DEFAULT 0,
		statut ENUM('fixed', 'shared', 'unattr') DEFAULT 'unattr',
		rid INT(11),
		normalesInTheLast DECIMAL(6,2) NOT NULL,
		instructionInTheLast DECIMAL(6,2) NOT NULL,
		simulateurInTheLast DECIMAL(6,2) NOT NULL,
		doubleInTheLast DECIMAL(6,2) NOT NULL,
		poids SMALLINT(6),
		occurrence TINYINT(4)
	);

	-- Remplit la table temporaire avec la liste des utilisateurs présents
	-- et leurs heures dans le mois précédent
	INSERT INTO tmpPresentsHeures
		SELECT uid, grade, did, 0, 0, 0, 0, 'unattr', 0, 0, 0, 0, 0, poids, 0
		FROM tmpPresents;
	
	-- Ajout des heures passées
	OPEN curPresents;
	REPEAT
	FETCH curPresents INTO uid_;
	IF NOT done THEN
		SELECT SUM(normales), SUM(instruction), SUM(simulateur), SUM(`double`)
			INTO norm, instr, simu, doub
			FROM TBL_HEURES
			WHERE date BETWEEN DATE_SUB(date_, INTERVAL 1 MONTH) AND date_
			AND uid = uid_;
		IF norm IS NULL THEN
			SET norm = 0;
		END IF;
		IF instr IS NULL THEN
			SET instr = 0;
		END IF;
		IF simu IS NULL THEN
			SET simu = 0;
		END IF;
		IF doub IS NULL THEN
			SET doub = 0;
		END IF;
		UPDATE tmpPresentsHeures
			SET normalesInTheLast = norm
			, instructionInTheLast = instr
			, simulateurInTheLast = simu
			, doubleInTheLast = doub
			WHERE uid = uid_;
	END IF;
	UNTIL done END REPEAT;
	CLOSE curPresents;

	-- Recherche le nombre d'heures à partager
		-- On applique un coefficient de 1.25 sur les heures saisies
		-- On a donc au départ 25% d'heures en plus que saisie
		-- pour couvrir les temps de relève, les arrondis...
	SELECT FLOOR(heures * 1.25)
		INTO heuresTotales
		FROM TBL_HEURES_A_PARTAGER
		WHERE centre = centre_
		AND team = team_
		AND secteurs = secteur
		AND date = date_;
				-- DEBUG
	-- SELECT CONCAT('Avant dispatchHeuresFixes sur ', secteur), heuresTotales AS 'Total des heures à distribuer';
				-- DEBUG
	-- ******************************
	-- * Distribue les heures fixes *
	-- ******************************
	CALL dispatchHeuresFixes( centre_, team_, date_, secteur , nbEleves);

	-- Calcule les quarts d'heures restants
	SELECT 4 * (heuresTotales - SUM(normales) - SUM(instruction)), 4 * SUM(instruction), COUNT(uid)
		INTO reliquat, heuresInstructionDistribuees, nbPc
		FROM tmpPresentsHeures
		WHERE grade != 'c'
		AND grade != 'theo';
				-- DEBUG
	-- SELECT reliquat / 4 AS 'Heures restantes après distribution des heures fixes';
				-- DEBUG

	-- *****************************************
	-- * Distribution des heures d'instruction *
	-- *****************************************
		-- Si des heures d'instruction n'ont pas déjà été distribuées
		-- lors du remplissage des heures fixes.
		-- Les heures d'instruction représentent 40% des heures totales par élève limité à 2 élèves
		-- Les heures sont divisées par deux car un élève ne peut pas
		-- avoir 100% des heures puisque la moitié des heures correspond à des heures orga
	IF heuresInstructionDistribuees = 0 AND nbEleves > 0 THEN
		-- On détermine le nombre d'heures d'instruction à distribuer
		-- Les heures sont divisées par deux car l'élève ne peut être radar et orga en même temps
		SET heuresInstructionDistribuees = FLOOR(nbEleves * .4 * reliquat / 2);
		-- ****************************************************
		-- * Distribution des heures d'instruction aux élèves *
		-- ****************************************************
		SELECT COUNT(uid)
			INTO nbInstruits
			FROM tmpPresentsHeures
			WHERE grade = 'c'
			OR grade = 'theo';
				-- DEBUG
		-- SELECT secteur, reliquat / 4, heuresInstructionDistribuees / 4, nbInstruits;
				-- DEBUG
		-- Lorsqu'il n'y a qu'un seul élève, on lui attribue toutes les heures
		IF nbInstruits = 1 THEN
			UPDATE tmpPresentsHeures
				SET normales = heuresInstructionDistribuees / 4
				WHERE grade = 'c'
				OR grade = 'theo';
		ELSEIF nbInstruits > 1 THEN
			-- L'élève le plus avancé reçoit jusque une heure par défaut (une heure si il y a plus d'une heure à distribuer, sinon la totalité)
			UPDATE tmpPresentsHeures
				SET normales = (SELECT MIN(nb)
					FROM (SELECT 1 AS nb
						UNION
						SELECT heuresInstructionDistribuees / 4 AS nb) AS t)
				WHERE grade = 'c'
				OR grade = 'theo'
				ORDER BY poids
				LIMIT 1;
			SELECT heuresInstructionDistribuees - SUM(normales) * 4
				INTO reliquat
				FROM tmpPresentsHeures
				WHERE grade = 'c'
				OR grade = 'theo';
				-- DEBUG
		-- SELECT secteur, reliquat / 4, heuresInstructionDistribuees / 4, nbInstruits, uid, SUM(normales) FROM tmpPresentsHeures WHERE normales != 0 GROUP BY uid;
				-- DEBUG
			-- Le reliquat est distribué équitablement à l'ensemble des élèves
			UPDATE tmpPresentsHeures
				SET normales = normales + FLOOR(reliquat / nbInstruits) / 4
				WHERE grade = 'c'
				OR grade = 'theo';
			SELECT heuresInstructionDistribuees - SUM(normales) * 4
				INTO reliquat
				FROM tmpPresentsHeures
				WHERE grade = 'c'
				OR grade = 'theo';
				-- DEBUG
		-- SELECT secteur, reliquat / 4, heuresInstructionDistribuees / 4, nbInstruits, uid, SUM(normales) FROM tmpPresentsHeures WHERE normales != 0 GROUP BY uid;
				-- DEBUG
			-- Distribue les quarts d'heures restants
			UPDATE tmpPresentsHeures
				SET normales = normales + .25
				WHERE grade = 'c'
				OR grade = 'theo'
				ORDER BY poids ASC
				LIMIT reliquat;
				-- DEBUG
			-- SELECT secteur, reliquat / 4, heuresInstructionDistribuees / 4, nbInstruits, uid, SUM(normales) FROM tmpPresentsHeures WHERE normales != 0 GROUP BY uid;
			-- SELECT secteur, heuresInstructionDistribuees - SUM(normales) * 4
			--	FROM tmpPresentsHeures
			--	WHERE grade = 'c'
			--	OR grade = 'theo';
				-- DEBUG
		END IF;


		-- **********************************************************
		-- * Distribution des heures d'instruction aux instructeurs *
		-- **********************************************************
			-- Chaque instructeur instruit normalement un nombre entier d'heure
			-- un instructeur supplémentaire utilisera le reliquat
			--
		-- Recherche du nombre d'instructeurs (inst, fmp, ce, cds)
		SELECT COUNT(uid)
			INTO nbInstructeurs
			FROM tmpPresentsHeures
			WHERE grade = 'inst'
			OR grade = 'fmp'
			OR grade = 'ce'
			OR grade = 'cds';
		-- On attribue les heures entières à tous les PC instructeurs
		UPDATE tmpPresentsHeures
			SET instruction = FLOOR(heuresInstructionDistribuees / (4 * nbInstructeurs))
			WHERE grade = 'inst'
			OR grade = 'fmp'
			OR grade = 'ce'
			OR grade = 'cds';
		SELECT heuresInstructionDistribuees - SUM(instruction) * 4
			INTO reliquat
			FROM tmpPresentsHeures
			WHERE grade = 'inst'
			OR grade = 'fmp'
			OR grade = 'ce'
			OR grade = 'cds';
		-- Calcule le reliquat d'heures entières
		SET nbInstructeurs = FLOOR(reliquat / 4);
		-- Et l'attribue à ceux qui ont le moins d'heures d'instruction dans le dernier mois
		-- Le nombre d'heures d'instruction dans le dernier mois est mis à jour
		UPDATE tmpPresentsHeures
			SET instruction = instruction + 1
			, instructionInTheLast = instructionInTheLast + 1
			WHERE grade = 'inst'
			OR grade = 'fmp'
			OR grade = 'ce'
			OR grade = 'cds'
			ORDER BY instructionInTheLast ASC
			LIMIT nbInstructeurs;
		-- Puis le reliquat de quarts d'heures
		SELECT heuresInstructionDistribuees - SUM(instruction) * 4
			INTO reliquat
			FROM tmpPresentsHeures
			WHERE grade = 'inst'
			OR grade = 'fmp'
			OR grade = 'ce'
			OR grade = 'cds';
		UPDATE tmpPresentsHeures
			SET instruction = instruction + .25
			WHERE grade = 'inst'
			OR grade = 'fmp'
			OR grade = 'ce'
			OR grade = 'cds'
			ORDER BY instructionInTheLast ASC
			LIMIT reliquat;
		
		-- DEBUG Vérifie que le nombre d'heures d'instruction attribuées aux instructeurs
		-- sont compatibles avec le nombre d'heures d'instruction attribuées aux élèves
		SELECT SUM(normales)
			INTO norm
			FROM tmpPresentsHeures
			WHERE grade = 'c' OR grade = 'theo';
		SELECT SUM(instruction)
			INTO instr
			FROM tmpPresentsHeures
			WHERE grade != 'c' AND grade != 'theo';
		IF instr - norm != 0 THEN
			SELECT CONCAT("Alerte ! Le nombre d'heures d'instruction ne correspond pas entre instructeurs et instruits (", instr - norm, ").");
		END IF;
		-- DEBUG

		-- * Fin de la distribution des heures d'instruction *
		-- Calcule les quarts d'heures restants
		SELECT 4 * (heuresTotales - SUM(normales) - SUM(instruction))
			INTO reliquat
			FROM tmpPresentsHeures
			WHERE grade != 'c'
			AND grade != 'theo';
	END IF;

		-- DEBUG
		-- IF reliquat > 0 THEN
		--	SELECT CONCAT('Il reste ', reliquat, ' quarts d''heures à distribuer parmi ', nbPc, '.') AS 'Info';
		-- END IF;
		-- DEBUG

	-- Distribue le nombre entier de quarts d'heure distribuables
	UPDATE tmpPresentsHeures
		SET normales = normales + FLOOR(reliquat / nbPc) / 4,
		statut = 'shared'
		WHERE grade != 'c'
		AND grade != 'theo'
	; --	AND statut != 'fixed';

	-- Recherche du nombre de quarts d'heure restants
	SELECT 4 * (heuresTotales - SUM(normales) - SUM(instruction))
		INTO reliquat
		FROM tmpPresentsHeures
		WHERE grade != 'c'
		AND grade != 'theo';

		-- DEBUG
		-- IF reliquat > 0 THEN
		--	SELECT CONCAT('Il reste ', reliquat, ' quarts d''heures à distribuer.') AS 'Info';
		-- END IF;
		-- DEBUG

	-- On sélectionne le nombre de PC (correspondant au nombre de quarts d'heures) ayant le moins d'heures sur le dernier mois
	UPDATE tmpPresentsHeures
		SET normales = normales + .25,
		statut = 'shared'
		WHERE grade != 'c'
		AND grade != 'theo'
		-- AND statut != 'fixed'
		ORDER BY normalesInTheLast ASC
		LIMIT reliquat;

		-- DEBUG
	-- Recherche du nombre de quarts d'heure restants
	SELECT 4 * (heuresTotales - SUM(normales) - SUM(instruction))
		INTO reliquat
		FROM tmpPresentsHeures
		WHERE grade != 'c'
		AND grade != 'theo';

		IF reliquat > 0 THEN
			SELECT CONCAT('Il reste ', reliquat, ' quarts d''heures à distribuer (', secteur, ' - ', date_, ').') AS 'Info';
		END IF;
		-- DEBUG
	-- Calcul des occurrences
	IF date_ >= '2016-07-01' THEN
		UPDATE tmpPresentsHeures
			SET occurrence = FLOOR(normales / 2) + 1
			WHERE normales > .25;
		UPDATE tmpPresentsHeures
			SET occurrence = occurrence + FLOOR(instruction / 2) + 1
			WHERE instruction > .25;
	END IF;

	-- On reporte les heures dans la table heures
	REPLACE INTO TBL_HEURES
		SELECT uid, did, date_, normales, instruction, simulateur, `double`, statut, secteur, occurrence
			FROM tmpPresentsHeures;
END|
-- Distribue les heures fixes au sein de la table temporaire tmpPresentsHeures
DROP PROCEDURE IF EXISTS dispatchHeuresFixes|
CREATE PROCEDURE  dispatchHeuresFixes ( IN centre_ VARCHAR(50) , IN team_ VARCHAR(10) , IN date_ DATE , IN secteur VARCHAR(10) , IN nbEleves SMALLINT(6) )
BEGIN
	DECLARE done BOOLEAN DEFAULT 0;
	DECLARE nbPcHeuresFixes SMALLINT(6);
	DECLARE ruleid, didFixed INT(11);
	DECLARE heuresFixes DECIMAL (4,2);
	DECLARE gradeFixed VARCHAR(64);
	DECLARE typeFixed VARCHAR(10);
	-- Recherche les heures fixes attribuables
		-- Au cas où plusieurs règles sont susceptibles de s'appliquer,
		-- celle dont ordre est le plus élevé est retenue (traitée en dernier)
	DECLARE curFixed CURSOR FOR SELECT rid, grades, type, heures, dids
		FROM TBL_DISPATCH_HEURES
		WHERE FIND_IN_SET((SELECT cid
				FROM TBL_GRILLE
				WHERE date = date_
				AND centre = centre_
				AND team = team_)
			, cids)
		AND centre = centre_
		AND (team = team_ OR team = 'all')
		AND secteurs = secteur
		ORDER BY ordre ASC;

	OPEN curFixed;

	-- DEBUG
	-- SELECT CONCAT("Distribution des heures fixes ", secteur) AS 'Info';
	-- DEBUG
	-- Ajout des heures fixes
	REPEAT
	FETCH curFixed INTO ruleid, gradeFixed, typeFixed, heuresFixes, didFixed;
	IF NOT done THEN
		-- CALL messageSystem("Ajout des heures fixes", "DEBUG", 'dispatchOneDayHeures', NULL, CONCAT("ruleid:",ruleid,";grade:",gradeFixed,";heures:",heuresFixes,";did:",didFixed,";"));
		IF typeFixed = 'norm' THEN
			UPDATE tmpPresentsHeures
			SET normales = heuresFixes
			, statut = 'fixed'
			, rid = ruleid
			WHERE (FIND_IN_SET(grade, gradeFixed) OR grade = gradeFixed) AND (FIND_IN_SET(did, didFixed) OR did = didFixed OR didFixed IS NULL);
			-- DEBUG
			-- SELECT CONCAT('Heures fixes : ', heuresFixes, ' heures pour les ', gradeFixed, ' et dispo : ', (SELECT dispo FROM TBL_DISPO WHERE did = didFixed)) AS 'Info';
			-- DEBUG
			-- SELECT * FROM tmpPresentsHeures WHERE statut = 'fixed';
			-- DEBUG
		ELSEIF typeFixed = 'instru' AND nbEleves > 0 THEN
			UPDATE tmpPresentsHeures
			SET instruction = heuresFixes
			, statut = 'fixed'
			, rid = ruleid
			WHERE (FIND_IN_SET(grade, gradeFixed) OR grade = gradeFixed) AND (FIND_IN_SET(did, didFixed) OR did = didFixed OR didFixed IS NULL);
		ELSEIF typeFixed = 'simu' THEN
			UPDATE tmpPresentsHeures
			SET simulateur = heuresFixes
			, statut = 'fixed'
			, rid = ruleid
			WHERE (FIND_IN_SET(grade, gradeFixed) OR grade = gradeFixed) AND (FIND_IN_SET(did, didFixed) OR did = didFixed OR didFixed IS NULL);
		ELSEIF typeFixed = 'double' THEN
			UPDATE tmpPresentsHeures
			SET `double` = heuresFixes
			, statut = 'fixed'
			, rid = ruleid
			WHERE (FIND_IN_SET(grade, gradeFixed) OR grade = gradeFixed) AND (FIND_IN_SET(did, didFixed) OR did = didFixed OR didFixed IS NULL);
		END IF;
	END IF;
	UNTIL done END REPEAT;

	CLOSE curFixed;
	SET done = 0;

	SELECT COUNT(uid)
		INTO nbPcHeuresFixes
		FROM tmpPresentsHeures
		WHERE statut = 'fixed'
		AND grade != 'c'
		AND grade != 'theo';

	-- Corrige si il y a plus d'heures fixes distribuées que disponibles
	IF nbPcHeuresFixes > 0 THEN
		-- Calcule le nombre d'heures en trop par personne ayant des heures fixes
		-- On multiplie par 5 au lieu de 4 (quarts d'heure) pour garder des heures pour les utilisateurs qui n'ont pas d'heures fixes
		SELECT FLOOR(5 * (p.heures - (SUM(normales) + SUM(instruction))) / nbPcHeuresFixes) / 4
			INTO heuresFixes
			FROM tmpPresentsHeures AS t,
			TBL_HEURES_A_PARTAGER AS p
			WHERE grade != 'c'
			AND grade != 'theo'
			AND date = date_
			AND centre = centre_
			AND team = team_
			AND secteurs = secteur;
		IF heuresFixes < 0 THEN
			-- Retranche la part d'heures en trop (heuresFixes est négatif => addition)
			UPDATE tmpPresentsHeures
				SET normales = normales + heuresFixes ,
				instruction = instruction + heuresFixes
				WHERE statut = 'fixed';
		END IF;
	END IF;
END|
-- Distribue les heures pour une journée à tous les utilisateurs de l'équipe
DROP PROCEDURE IF EXISTS dispatchOneDayHeures|
CREATE PROCEDURE dispatchOneDayHeures ( IN centre_ CHAR(50) , IN team_ CHAR(10) , IN date_ DATE )
BEGIN
	DECLARE done BOOLEAN DEFAULT 0;
	DECLARE nbEleves SMALLINT(6);
	DECLARE ruleid, didFixed INT;
	DECLARE secteursFixed, typeFixed VARCHAR(10);
	DECLARE secteur VARCHAR(16);
	DECLARE gradeFixed VARCHAR(60);

	-- Liste les occurrences possibles
	DECLARE curOccurrences CURSOR FOR SELECT secteurs
		FROM TBL_HEURES_A_PARTAGER
		WHERE centre = centre_
		AND team = team_
		AND date = date_
		AND dispatched IS FALSE;
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	-- Table temporaire listant tous les utilisateurs présents ayant droit à des heures (élèves compris)
	DROP TEMPORARY TABLE IF EXISTS tmpPresents;
	CREATE TEMPORARY TABLE tmpPresents (
		uid SMALLINT(6) NOT NULL,
		grade VARCHAR(64) NOT NULL,
		did SMALLINT(6) NULL,
		poids SMALLINT(6) NOT NULL
	);
	INSERT INTO tmpPresents
		SELECT a.uid, grade, l.did, a.poids
		FROM TBL_AFFECTATION AS a
		NATURAL LEFT JOIN TBL_L_SHIFT_DISPO AS l
		JOIN TBL_DISPO AS d
		ON l.did = d.did
		WHERE l.date = date_
		AND date_ BETWEEN beginning AND end
		AND a.centre = centre_
		AND a.team = team_
		AND principale IS FALSE
		AND heures IS TRUE
		UNION
		SELECT a.uid, grade, NULL, a.poids
		FROM TBL_AFFECTATION AS a
		JOIN TBL_USERS AS u
		ON a.uid = u.uid
		WHERE date_ BETWEEN beginning AND end
		AND centre = centre_
		AND team = team_
		AND principale IS FALSE
		AND actif IS TRUE
		AND a.uid NOT IN (SELECT uid FROM TBL_L_SHIFT_DISPO WHERE date = date_ AND pereq IS FALSE);
	-- Compte le nombre d'élèves et fixe le maximum d'élèves à 2
	SELECT MIN(nb) INTO nbEleves
       		FROM (SELECT 2 AS nb
			UNION
			SELECT COUNT(uid) AS nb
			FROM tmpPresents
			WHERE (grade = 'c' OR grade = 'theo')
		) AS p;
		
	OPEN curOccurrences;

	REPEAT
	FETCH curOccurrences INTO secteur;
	IF NOT done THEN
		-- DEBUG
		-- SELECT CONCAT('Traitement de ', date_, ' / ', secteur, ' (nbEleves : ', nbEleves, ')') AS 'Info';
		-- DEBUG
		CALL dispatchOneDayHeuresSecteur(centre_, team_, secteur, nbEleves, date_);
	END IF;
	UNTIL done END REPEAT;

	UPDATE TBL_HEURES_A_PARTAGER
	SET dispatched = TRUE
	WHERE centre = centre_
	AND team = team_
	AND date = date_;
END
|
DROP PROCEDURE IF EXISTS addDispatchSchema|
CREATE PROCEDURE addDispatchSchema( IN cycles VARCHAR(64) , IN centre_ VARCHAR(50) , IN team_ VARCHAR(10) , IN grade_ VARCHAR(64) , IN dispos VARCHAR(128) , IN typ VARCHAR(64) , IN nbHeures DECIMAL(4,2) )
BEGIN
	DECLARE done BOOLEAN DEFAULT 0;
	DECLARE dids VARCHAR(64);
	DECLARE cids VARCHAR(64);
	DECLARE pivot VARCHAR(64);
	DECLARE tmp VARCHAR(64);
	DECLARE curDispo CURSOR FOR
		SELECT did
		FROM TBL_DISPO
		WHERE FIND_IN_SET(dispo, dispos);

	DECLARE curCycle CURSOR FOR
		SELECT cid
		FROM TBL_CYCLE
		WHERE FIND_IN_SET(vacation, cycles);

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

	-- Création de la table utilisateur qui offre une version lisible des schémas
	CREATE TABLE IF NOT EXISTS TBL_DISPATCH_HEURES_USER (
		rid INT( 11 ) NOT NULL ,
		cycles VARCHAR( 64 ) NOT NULL ,
		centre VARCHAR( 50 ) NOT NULL DEFAULT 'athis',
		team VARCHAR( 10 ) NOT NULL DEFAULT '9e',
		grades VARCHAR( 64 ) NOT NULL DEFAULT 'pc',
		dispos VARCHAR( 128 ) DEFAULT NULL ,
		type ENUM( 'norm', 'instru', 'simu' ) NOT NULL ,
		heures DECIMAL( 4, 2 ) NOT NULL COMMENT 'Nombre d\'heures allouées (en décimal)',
		PRIMARY KEY ( rid )
	) ENGINE = InnoDB DEFAULT CHARSET = utf8;

	-- Création du SET dispo
	OPEN curDispo;
	REPEAT
	FETCH curDispo INTO tmp;
	IF NOT done THEN
		SET pivot = CONCAT_WS(',', dids, tmp);
		SET dids = pivot;
	END IF;
	UNTIL done END REPEAT;
	CLOSE curDispo;
	SET done = 0;

	-- Création du SET cycle
	OPEN curCycle;
	REPEAT
	FETCH curCycle INTO tmp;
	IF NOT done THEN
		SET pivot = CONCAT_WS(',', cids, tmp);
		SET cids = pivot;
	END IF;
	UNTIL done END REPEAT;
	CLOSE curCycle;
	SET done = 0;

	INSERT INTO TBL_DISPATCH_HEURES
	(rid, cids, centre, team, grades, dids, type, heures)
	VALUES
	(NULL, cids, centre_, team_, grade_, dids, typ, nbHeures);

	INSERT INTO TBL_DISPATCH_HEURES_USER
	(rid, cycles, centre, team, grades, dispos, type, heures)
	VALUES
	(LAST_INSERT_ID(), cycles, centre_, team_, UPPER(grade_), dispos, typ, nbHeures);

	-- SELECT * FROM TBL_DISPATCH_HEURES;
	-- SELECT * FROM TBL_DISPATCH_HEURES_USER;
END
|
DROP PROCEDURE IF EXISTS suppressDispatchSchema|
CREATE PROCEDURE suppressDispatchSchema( IN ruleid INT(11) )
BEGIN
	DELETE FROM TBL_DISPATCH_HEURES
	WHERE rid = ruleid;
END
|
DROP PROCEDURE IF EXISTS updateDispatchSchema|
CREATE PROCEDURE updateDispatchSchema( IN ruleid INT )
BEGIN
	DECLARE done BOOLEAN DEFAULT 0;
	DECLARE dispoids VARCHAR(64);
	DECLARE tmp VARCHAR(64);
	DECLARE pivot VARCHAR(64);
	DECLARE cycls VARCHAR(64);
	DECLARE cyclids VARCHAR(64);
	DECLARE centre_ VARCHAR(50);
	DECLARE team_ VARCHAR(10);
	DECLARE grade_ VARCHAR(64);
	DECLARE disp VARCHAR(64);
	DECLARE typ VARCHAR(64);
	DECLARE statu VARCHAR(64);
	DECLARE heur DECIMAL(4,2);

	DECLARE curCycle CURSOR FOR
		SELECT vacation
		FROM TBL_CYCLE
		WHERE FIND_IN_SET(rang, (SELECT cids
			FROM TBL_DISPATCH_HEURES
			WHERE rid = ruleid)
		);

	DECLARE curDispo CURSOR FOR
		SELECT dispo
		FROM TBL_DISPO
		WHERE FIND_IN_SET(did, (SELECT dids
			FROM TBL_DISPATCH_HEURES
			WHERE rid = ruleid)
		);

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

	SELECT centre, team, grades, type, statut, heures
		INTO centre_, team_, grade_, typ, statu, heur
		FROM TBL_DISPATCH_HEURES
		WHERE rid = ruleid;

	OPEN curCycle;
	REPEAT
	FETCH curCycle INTO tmp;
	IF NOT done THEN
		SET pivot = CONCAT_WS(',', cycls, tmp);
		SET cycls = pivot;
	END IF;
	UNTIL done END REPEAT;
	CLOSE curCycle;
	SET done = 0;

	OPEN curDispo;
	REPEAT
	FETCH curDispo INTO tmp;
	IF NOT done THEN
		SET pivot = CONCAT_WS(',', disp, tmp);
		SET disp = pivot;
	END IF;
	UNTIL done END REPEAT;
	CLOSE curDispo;

	REPLACE INTO TBL_DISPATCH_HEURES_USER
	(rid, cycles, centre, team, grades, dispos, type, statut, heures)
	VALUES
	(ruleid, cycls, centre_, team_, UPPER(grade_), disp, typ, statu, heur);
END
|
DROP PROCEDURE IF EXISTS addHeuresIndividuelles|
CREATE PROCEDURE addHeuresIndividuelles( IN uid_ SMALLINT(6), IN dateH DATE, IN normal FLOAT, IN instruc FLOAT, IN simul FLOAT , IN doubl FLOAT )
BEGIN
	REPLACE INTO TBL_HEURES
		(uid, date, normales, instruction, simulateur, `double`, statut)
		VALUES
		(uid_, dateH, normal, instruc, simul, doubl, 'unattr');
END
|

DROP TRIGGER IF EXISTS deleteHours|
CREATE TRIGGER deleteHours
	AFTER DELETE ON TBL_HEURES_A_PARTAGER
	FOR EACH ROW
	DELETE FROM TBL_HEURES
		WHERE date = OLD.date
			AND uid IN (SELECT uid
				FROM TBL_AFFECTATION
				WHERE centre = OLD.centre
				AND team = OLD.team
				AND OLD.date BETWEEN beginning AND end)|
DELIMITER ;

DROP VIEW IF EXISTS affectations;
CREATE VIEW affectations AS
	SELECT nom, centre, team, grade, beginning, end
	FROM TBL_AFFECTATION a
	, TBL_USERS u
	WHERE a.uid = u.uid
	ORDER BY actif DESC,nom ASC, beginning ASC;
