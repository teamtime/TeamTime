<?php
// mesHeures.php
//
// Affiche les heures de l'utilisateur depuis la date demandée

/*
	TeamTime is a software to manage people working in team on a cyclic shift.
	Copyright (C) 2012 Manioul - webmaster@teamtime.me

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Require authenticated user
// L'utilisateur doit être logué pour accéder à cette page
$requireAuthenticatedUser = true;

ob_start(); // Obligatoire pour firePHP

/*
 * Configuration de la page
 * Définition des include nécessaires
 */
	$conf['page']['include']['constantes'] = 1; // Ce script nécessite la définition des constantes
	$conf['page']['include']['errors'] = 1; // le script gère les erreurs avec errors.inc.php
	$conf['page']['include']['class_debug'] = 1; // La classe debug est nécessaire à ce script
	$conf['page']['include']['globalConfig'] = 1; // Ce script nécessite config.inc.php
	$conf['page']['include']['init'] = 1; // la session est initialisée par init.inc.php
	$conf['page']['include']['globals_db'] = 1; // Le DSN de la connexion bdd est stockée dans globals_db.inc.php
	$conf['page']['include']['class_db'] = 1; // Le script utilise class_db.inc.php
	$conf['page']['include']['session'] = 1; // Le script utilise les sessions par session.imc
	$conf['page']['include']['classUtilisateur'] = NULL; // Le sript utilise uniquement la classe utilisateur (auquel cas, le fichier class_utilisateur.inc.php
	$conf['page']['include']['class_utilisateurGrille'] = 1; // Le sript utilise la classe utilisateurGrille
	$conf['page']['include']['class_cycle'] = 1; // La classe cycle est nécessaire à ce script (remplace grille.inc.php
	$conf['page']['include']['class_menu'] = 1; // La classe menu est nécessaire à ce script
	$conf['page']['include']['class_article'] = 1; // Le script utilise class_article.inc.php'affichage de certaines pages (licence)
	$conf['page']['include']['smarty'] = 1; // Smarty sera utilisé sur cette page
	$conf['page']['compact'] = false; // Compactage des scripts javascript et css
	$conf['page']['include']['bibliothequeMaintenance'] = false; // La bibliothèque des fonctions de maintenance est nécessaire
/*
 * Fin de la définition des include
 */


/*
 * Configuration de la page
 */
        $conf['page']['titre'] = sprintf("Liste mes heures"); // Le titre de la page
// Définit la valeur de $DEBUG pour le script
// on peut activer le debug sur des parties de script et/ou sur certains scripts :
// $DEBUG peut être activer dans certains scripts de required et désactivé dans d'autres
	$DEBUG = false;

	/*
	 * Choix des éléments à afficher
	 */
	
	// Affichage du menu horizontal
	$conf['page']['elements']['menuHorizontal'] = true;
	// Affichage messages
	$conf['page']['elements']['messages'] = true;
	// Affichage du choix du thème
	$conf['page']['elements']['choixTheme'] = false;
	// Affichage du menu d'administration
	$conf['page']['elements']['menuAdmin'] = false;
	
	// éléments de debug
	
	// FirePHP
	$conf['page']['elements']['firePHP'] = true;
	// Affichage des timeInfos
	$conf['page']['elements']['timeInfo'] = $DEBUG;
	// Affichage de l'utilisation mémoire
	$conf['page']['elements']['memUsage'] = $DEBUG;
	// Affichage des WherewereU
	$conf['page']['elements']['whereWereU'] = $DEBUG;
	// Affichage du lastError
	$conf['page']['elements']['lastError'] = $DEBUG;
	// Affichage du lastErrorMessage
	$conf['page']['elements']['lastErrorMessage'] = $DEBUG;
	// Affichage des messages de debug
	$conf['page']['elements']['debugMessages'] = $DEBUG;


	// Gestion des briefings
	$conf['page']['elements']['intervalDate'] = true;

	// Utilisation de jquery
	$conf['page']['javascript']['jquery'] = false;
	// Utilisation de grille2.js.php
	$conf['page']['javascript']['grille2'] = false;
	// Utilisation de utilisateur.js
	$conf['page']['javascript']['utilisateur'] = false;

	// Feuilles de styles
	// Utilisation de la feuille de style general.css
	$conf['page']['stylesheet']['general'] = true;
	$conf['page']['stylesheet']['grille'] = false;
	$conf['page']['stylesheet']['grilleUnique'] = false;
	$conf['page']['stylesheet']['utilisateur'] = false;

	// Compactage des pages
	$conf['page']['compact'] = false;
	
/*
 * Fin de la configuration de la page
 */

require 'required_files.inc.php';

// Préparation de l'aide
$smarty->assign('help', article::article('help mesHeures'));
$smarty->display('help.tpl');

$aHeures = array();
$aTotaux = array();
$checked = array();
$uid = (empty($_GET['uid']) || empty($_SESSION['ADMIN']) ? $_SESSION['utilisateur']->uid() : (int) $_GET['uid']);
$nom = (empty($_GET['nom']) || empty($_SESSION['ADMIN']) ? $_SESSION['utilisateur']->nom() : $_GET['nom']);
$prenom = (empty($_GET['nom']) || empty($_SESSION['ADMIN']) ? $_SESSION['utilisateur']->prenom() : '');
if (!empty($_GET['d'])) $dateDebut = $_GET['d'];
if (!empty($_GET['f'])) $dateFin = $_GET['f'];
if (!empty($_POST['dateD'])) $dateDebut = $_POST['dateD'];
if (!empty($_POST['dateF'])) $dateFin = $_POST['dateF'];

$affectation = $_SESSION['utilisateur']->affectationOnDate($dateDebut);

// Recherche la date des dernières heures calculées
$sql = sprintf("SELECT MAX(`date`)
	FROM `TBL_HEURES_A_PARTAGER`
	WHERE `dispatched` IS TRUE
	AND `centre` = '%s'
	AND `team` = '%s'
	", $_SESSION['utilisateur']->centre()
	, $_SESSION['utilisateur']->team());
$row = $_SESSION['db']->db_fetch_array($_SESSION['db']->db_interroge($sql));

$date = new Date($row[0]);

$smarty->assign('lastHeures', $date->formatDate());

$smarty->assign('dateD', $dateDebut);
$smarty->assign('dateF', $dateFin);

$smarty->assign('centre', $affectation['centre']);

// Recherche les différentes heures possibles (dans TBL_DISPATCH_HEURES)
$aType = $_SESSION['db']->db_set_enum_to_array('TBL_DISPATCH_HEURES', 'type');
unset($aType['Type']);


$smarty->assign('aType', $aType);


/*
 * Traitement du formulaire d'affichage des heures
 */
if (!empty($dateDebut)) {
	$dateDebut = new Date($dateDebut);
	if (!empty($dateFin)) {
		$dateFin = new Date($dateFin);
	} else {
		$dateFin = clone $dateDebut;
		$dateFin->addJours(365);
	}
	/*
	 * Gestion des exclusions
	 */
	$exclude = "";
	if (sizeof($_POST['dispo']) > 0) {
		// Permettre d'avoir des heures attribuées par l'utilisateur (unattr) sur des jours filtrés
		$exclude = sprintf("
			AND ((NOT FIND_IN_SET(`did`, '%s') OR `did` IS NULL)
			OR (FIND_IN_SET(`did`, '%s') AND `statut` = 'unattr'))
			", $_SESSION['db']->db_real_escape_string(implode(',', array_keys($_POST['dispo'])))
			, $_SESSION['db']->db_real_escape_string(implode(',', array_keys($_POST['dispo'])))
		);
	}

	$sql = sprintf("
		SELECT `date`, `normales`, `instruction`, `simulateur`, `double`, `secteurs`, `occurrence`
		FROM `TBL_HEURES`
		WHERE `uid` = %d
		AND `date` BETWEEN '%s' AND '%s'
		AND (`did` != (SELECT `did` FROM `TBL_DISPO` WHERE `dispo` = 'Rempla') OR `did` IS NULL)
		%s
		ORDER BY `date` ASC
		", $uid
		, $dateDebut->date()
		, $dateFin->date()
		, $exclude
	);
	$result = $_SESSION['db']->db_interroge($sql);
	while ($row = $_SESSION['db']->db_fetch_assoc($result)) {
		$d = new Date($row['date']);
		$row['date'] = $d->formatDate('fr');
		$aHeures[$row['date']][$row['secteurs']] = $row;
		$aHeures[$row['date']]['global']['date'] = $row['date'];
		$aHeures[$row['date']]['global']['normales'] += $row['normales'];
		$aHeures[$row['date']]['global']['instruction'] += $row['instruction'];
		$aHeures[$row['date']]['global']['simulateur'] += $row['simulateur'];
		$aHeures[$row['date']]['global']['double'] += $row['double'];
		if ($row['occurrence'] > 0) {
			$aHeures[$row['date']]['global']['occurrence'] .= $row['occurrence'] . ' ' . $row['secteurs'] . ' ';
		}
	}
	mysqli_free_result($result);
	/*
	 * Calcul des totaux
	 */
	$sql = sprintf("
		SELECT SUM(`normales`) AS `normales`, SUM(`instruction`) AS `instruction`, SUM(`simulateur`) AS `simulateur`, SUM(`double`) AS `double`
		FROM `TBL_HEURES`
		WHERE `uid` = %d
		AND `date` BETWEEN '%s' AND '%s'
		AND (`did` != (SELECT `did` FROM `TBL_DISPO` WHERE `dispo` = 'Rempla') OR `did` IS NULL)
		%s
		", $uid
		, $dateDebut->date()
		, $dateFin->date()
		, $exclude
	);
	$aTotaux = $_SESSION['db']->db_fetch_assoc($_SESSION['db']->db_interroge($sql));

	// Calcul les totaux des occurrences
	$sql = sprintf("SELECT CONCAT(SUM(`occurrence`), ' ', `secteurs`)
		FROM `TBL_HEURES`
		WHERE `date` BETWEEN '%s' AND '%s'
		AND `uid` = %d
		AND (`did` != (SELECT `did` FROM `TBL_DISPO` WHERE `dispo` = 'Rempla') OR `did` IS NULL)
		%s
		GROUP BY `secteurs`
		", $dateDebut->date()
		, $dateFin->date()
		, $uid
		, $exclude
	);
	$result = $_SESSION['db']->db_interroge($sql);
	while($row = $_SESSION['db']->db_fetch_array($result)) {
		 $aTotaux['occurrence'] .= $row[0] . ' ';
	}
	mysqli_free_result($result);	

	$smarty->assign('dateDebut', $dateDebut->formatDate('fr'));
	$smarty->assign('dateFin', $dateFin->formatDate('fr'));
	$smarty->assign('nom', $prenom . " " . $nom);
	$smarty->assign('heures', $aHeures);
	$smarty->assign('totaux', $aTotaux);
}
/*
 * Traitement du formulaire d'ajout d'heures
 */
if (!empty($_POST['date'])) {
	$date = new Date($_POST['date']);
	$norm = !empty($_POST['norm']) ? 0+$_POST['norm'] : 0;
	$instru = !empty($_POST['instru']) ? 0+$_POST['instru'] : 0;
	$simu = !empty($_POST['simu']) ? 0+$_POST['simu'] : 0;
	$double = !empty($_POST['double']) ? 0+$_POST['double'] : 0;
	$sql = sprintf("
		CALL addHeuresIndividuelles(%d, '%s', '%s', '%s', '%s', '%s')
		", $uid
		, $date->date()
		, $norm
		, $instru
		, $simu
		, $double
	);
	$_SESSION['db']->db_interroge($sql);
}
$smarty->display('debutHeuresForm.tpl');

$year = date('Y');
$previousMonth = ($date->jour() == $date->nbJoursMois()) ? $date->mois() : $date->previousMonth();
if ($previousMonth > date('m')) {
	$year--;
}
$months = array();
for ($i = 0; $i <= 5; $i++) {
	$m = ($i + $previousMonth + 6) % 12 + 1;
	$y = ($m <= $previousMonth ? $year : $year - 1);
	$mois = Date::genCalendrier($y);
	$months[$mois[$m]['asHTML'] . " " . $y] = array('debut' => sprintf("01-%02d-%04d", $m, $y), 'fin' => sprintf("%02d-%02d-%04d", $mois[$m]['nbJours'], $m, $y));
}

$smarty->assign('months', $months);
$smarty->display('boutonMonthYears.tpl');
// $smarty->display('saisieHeuresIndividuelles.tpl');
if (!empty($dateDebut)) {
	$smarty->display('mesHeures.tpl');
}

/*
 * Informations de debug
 */
include 'debug.inc.php';
firePhpLog($conf, '$conf');
firePhpLog(debug::getInstance()->format(), 'format debug messages');
firePhpLog($javascript, '$javascript');
firePhpLog($stylesheet, '$stylesheet');

// Affichage du bas de page
$smarty->display('footer.tpl');

ob_end_flush(); // Obligatoire pour firePHP

?>
