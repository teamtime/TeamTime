<?php
/**
 * TeamTime SPL autoloader.
 * PHP Version 5
 * @package TeamTime
 * @link https://gitlab.com/manioul/TeamTime
 * @author Manioul
 * @copyright 2012 - 2016 teamtime.me
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * @note This program is distributed in the hope that it will be useful - WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 */
/**
 * TeamTime SPL autoloader.
 * @param string $classname The name of the class to load
 */
function TeamTimeAutoload($classname)
{
    //Can't use __DIR__ as it's only in PHP 5.3+
    $filename = dirname(__FILE__).DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'class_'.strtolower($classname).'.inc.php';
    if (is_readable($filename)) {
        require $filename;
    }
}
if (version_compare(PHP_VERSION, '5.1.2', '>=')) {
    //SPL autoloading was introduced in PHP 5.1.2
    if (version_compare(PHP_VERSION, '5.3.0', '>=')) {
        spl_autoload_register('TeamTimeAutoload', true, true);
    } else {
        spl_autoload_register('TeamTimeAutoload');
    }
} else {
    /**
     * Fall back to traditional autoload for old PHP versions
     * @param string $classname The name of the class to load
     */
    function __autoload($classname)
    {
        TeamTimeAutoload($classname);
    }
}
