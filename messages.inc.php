<?php
// messages.inc.php
//
// Affichage de messages d'ordre général

/*
	TeamTime is a software to manage people working in team on a cyclic shift.
	Copyright (C) 2012 Manioul - webmaster@teamtime.me

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$messages = array();
$index = 0;

if (!array_key_exists('iAmVirtual', $_SESSION)) {
	if ($_SESSION['utilisateur']->hasRole('admin')) {
		$messages[$index]['message'] = "Connecté en tant que " . $_SESSION['utilisateur']->login();
		$messages[$index]['lien'] = "";
		$messages[$index]['classe'] = "warn";
		$index++;
	}
} else {
	$messages[$index]['message'] = "Connecté en tant que " . $_SESSION['utilisateur']->login() . " (" . $_SESSION['iAmVirtual'] . ")";
	$messages[$index]['lien'] = "";
	$messages[$index]['classe'] = "warn";
	$index++;
}
if ($_SESSION['utilisateur']->hasRole('admin') && !get_sql_globals_constant('online')) {
	$messages[$index]['message'] = "Le site est actuellement hors-ligne.";
	$messages[$index]['lien'] = "administration.php";
	$messages[$index]['classe'] = "warn";
	$index++;
}
if (array_key_exists('iAmVirtual', $_SESSION)) {// && !array_key_exists('ADMIN', $_SESSION))) {
	$messages[$index]['message'] = sprintf("Vous vous faites passer pour %s %s. Cliquez ici pour retrouver votre vraie personnalité...", $_SESSION['utilisateur']->prenom(), $_SESSION['utilisateur']->nom());
	$messages[$index]['lien'] = "impersonate.php?iWantMyselfBack=1";
	$messages[$index]['classe'] = "warn";
	$index++;
}
foreach ($_SESSION['utilisateur']->retrMessages() as $message) {
	$messages[$index]['message'] = $message->message();
	$message->setRead();
	$index++;
}
$_SESSION['utilisateur']->flushMessages();
// Recherche les annulations de congés
if ($_SESSION['utilisateur']->hasRole('teamEdit')) {
	$sql = sprintf("SELECT *
		FROM `TBL_VACANCES_A_ANNULER`
		WHERE `edited` IS FAlSE
		AND `uid` IN (SELECT `uid`
			FROM `TBL_ANCIENNETE_EQUIPE`
			WHERE `centre` = '%s'
			AND `team` = '%s'
			AND NOW() BETWEEN `beginning` AND `end`)
			", $_SESSION['utilisateur']->centre()
			, $_SESSION['utilisateur']->team()
		);
	if (mysqli_num_rows($_SESSION['db']->db_interroge($sql)) > 0) {
		$messages[$index]['message'] = "Vous avez des annulations de congés en attente d'impression : cliquez-moi pour les imprimer...";
		$messages[$index]['lien'] = 'conges.php';
		$index++;
	}
}
// Recherche les comptes créés en attente de validation
if ($_SESSION['utilisateur']->hasRole('editeurs')) {
	$sql = sprintf("SELECT * FROM `TBL_SIGNUP_ON_HOLD`
		WHERE `centre` = '%s'
		AND `team` = '%s'
		AND `url` IS NULL
		", $_SESSION['utilisateur']->centre()
		, $_SESSION['utilisateur']->team()
	);
	// Les admins accèdent à l'ensemble des créations de compte
	if ($_SESSION['utilisateur']->hasRole('admin')) {
		$sql = "SELECT * FROM `TBL_SIGNUP_ON_HOLD`
			WHERE `url` IS NULL";
	}
	$result = $_SESSION['db']->db_interroge($sql);
	if (mysqli_num_rows($result) > 0) {
		$messages[$index]['message'] = "Des utilisateurs ont fait une demande d'inscription et attendent votre acceptation.";
		$messages[$index]['lien'] = 'confirmUser.php';
		$index++;
	}
	mysqli_free_result($result);	
}

$smarty->assign('messages', $messages);
$smarty->display('messages.tpl');
?>
