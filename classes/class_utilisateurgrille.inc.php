<?php
// class_utilisateurgrille.inc.php
//
// étend la classe utilisateur aux utilisateurs de la grille
//

/*
	TeamTime is a software to manage people working in team on a cyclic shift.
	Copyright (C) 2012 Manioul - webmaster@teamtime.me

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * La classe utilisateurGrille étend la classe utilisateur aux utilisateurs de la grille.
 *
 */
class utilisateurGrille extends utilisateur {
	private $uid;
	private $nom;
	private $gid;
	private $prenom;
	private $classe = array(); // array('c', 'pc', 'ce', 'cds', 'dtch')
	private $roles = array(); // Les autorisations de l'utilisateur
	private $vismed; // Date de la prochaine visite médicale
	private $phone = array(); // Numéro de téléphone
	private $adresse; // adresse
	private $affectationPrincipale = array(); // tableau de l'affectation principale de l'utilisateur à la date courante
	private $affectations = array(); // tableau des affectations
	private $orderedAffectations = array(); // tableau des affectations rangées en ordre croissant
	private $cacheAffectation = array(); // Une variable pour garder en cache les affectations
	// précédemment recherchées afin d'éviter d'interroger la base de données à chaque fois
	// '2000-01-01'	=> array('centre'	=>
	// 			'team'		=>
	// 			'beginning'	=>
	// 			'end'		=>
	// 			)
	private $cacheNextAffectation = array(); // Une variable pour garder en cache les affectations
	// correspondant à la prochaine affectation dont la date est une clé du tableau
	private $centre = NULL; // centre actuel
	private $team = NULL; // team actuelle
	private $grade = NULL; // grade actuel
	private $poids; // La position d'affichage dans la grille (du plus faible au plus gros)
	private $annuaire; // Boolean vrai si l'utilisateur veut paraître dans l'annuaire
	/**
	 * Préférences utilisateur.
	 *
	 * Un tableau regroupant les préférences de l'utilsiateur
	 */
	private $pref = array();
	private $showtipoftheday; // L'utilisateur veut-il voir les tips of the day
	private $indexPage; // L'index de la page favorite (ouverte après la connexion) dans le tableaux $availablePages
	private $dispos; /* un tableau contenant un tableau des dispos indexées par les dates:
			* $dispos[date] = array('dispo1', 'dispo2',... 'dispoN'); */
	private $messages = array();
	private static $label = array();
	private static $availablePages = array(
		1	=> array ('titre'	=> 'Cycle unique'
				  , 'uri'	=> 'affiche_grille.php'
				  , 'gid'	=> 255)
		, 2	=> array('titre'	=> 'Trois cycles'
				  , 'uri'	=> 'affiche_grille.php?nbCycle=3'
				  , 'gid'	=> 255)
		, 3  	=> array('titre'	=> "mon compte"
				  , 'uri'	=> 'monCompte.php'
				  , 'gid'	=> 255)
		, 4  	=> array('titre'	=> "Gestion des utilisateurs"
				  , 'uri'	=> 'utilisateur.php'
				  , 'gid'	=> 0)
	);
// Méthodes statiques
	protected static function _label($index) {
		if (isset(self::$label[$index])) {
			return self::$label[$index];
		} else {
			return false;
		}
	}
	protected static function _localFieldsDefinition($regen = NULL) {
		foreach ($_SESSION['db']->db_getColumnsTable("TBL_AFFECTATION") as $row) {
			$fieldsDefinition[$row['Field']]['Field'] = isset($label[$row['Field']]) ? $label[$row['Field']] : $row['Field'];
			if ($row['Extra'] == 'auto_increment' || $row['Field'] == 'nblogin' || $row['Field'] == 'lastlogin') {
				// Ce champ ne sera pas saisi par l'utilisateur
			} else {
				$fieldsDefinition[$row['Field']]['width'] = -1;
				if (preg_match('/\((\d*)\)/', $row['Type'], $match) == 1) {
					if ($match[1] > 1) {
						$fieldsDefinition[$row['Field']]['width'] = ($match[1] < 10) ? $match[1] : 10;
						$fieldsDefinition[$row['Field']]['maxlength'] = $match[1];
					}
				}
				if (preg_match('/int\((\d*)\)/', $row['Type'], $match)) {
					if ($match[1] == 1) {
						$fieldsDefinition[$row['Field']]['type'] = "checkbox";
						$fieldsDefinition[$row['Field']]['value'] = 1;
					} else {
						$fieldsDefinition[$row['Field']]['type'] = "text";
					}
				} elseif ($row['Field'] == 'email') {
					$fieldsDefinition[$row['Field']]['type'] = 'email';
				} elseif ($row['Field'] == 'password') {
					$fieldsDefinition[$row['Field']]['type'] = 'password';
				} elseif ($row['Type'] == 'date') {
					$fieldsDefinition[$row['Field']]['type'] = 'date';
					$fieldsDefinition[$row['Field']]['maxlength'] = 10;
					$fieldsDefinition[$row['Field']]['width'] = 6;
				} else {
					$fieldsDefinition[$row['Field']]['type'] = 'text';
				}
			}
		}
	}
	public static function _fieldsDefinition($regen = NULL) {
		$correspondances = array(
			'sha1'		=> htmlspecialchars("Mot de passe", ENT_COMPAT)
			, 'arrivee'	=> htmlspecialchars("Date d'arrivée", ENT_COMPAT)
			, 'theorique'	=> htmlspecialchars("Date du théorique", ENT_COMPAT)
			, 'pc'		=> htmlspecialchars("Date du pc", ENT_COMPAT)
			, 'ce'		=> htmlspecialchars("Date ce", ENT_COMPAT)
			, 'cds'		=> htmlspecialchars("Date cds", ENT_COMPAT)
			, 'vismed'	=> htmlspecialchars("Date visite médicale", ENT_COMPAT)
			, 'lastlogin'	=> htmlspecialchars("Date de dernière connexion", ENT_COMPAT)
		);
		parent::_fieldsDefinition($correspondances, $regen);
	}
	/**
	 * Méthode permettant l'authentification de l'utilisateur
	 * dont le login et le mot de passe sont passés en paramètre.
	 *
	 * @param string $login est le login de l'utilisateur
	 * @param string $pwd est le mot de passe de l'utilisateur
	 *
	 * @return void
	 */
	public static function logon($login, $pwd) {
		$db = new database($GLOBALS['DSN']['notlogged']);

		$sql = sprintf("
			SELECT `uid`, `nblogin` FROM `TBL_USERS`
			WHERE `login` = '%s'
			AND `sha1` = SHA1('%s')
			", $db->db_real_escape_string($login)
			, $db->db_real_escape_string($login . $pwd)
		);
		$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("Tentative de connexion", "TRACE", "%s", "connection attempt", "%s")'
			, __METHOD__
			, $_SESSION['db']->db_real_escape_string(json_encode(array(
				'client'	=> $_SERVER['REMOTE_ADDR']
				, 'login'	=> $login
			))))
		);
		$result = $db->db_interroge($sql);
		if (mysqli_num_rows($result) > 0) {
			session_regenerate_id(); // Éviter les attaques par fixation de session
			$row = $db->db_fetch_assoc($result);
			mysqli_free_result($result);
			$DSN = $GLOBALS['DSN']['user'];
			$DSN['username'] = 'ttm.' . $row['uid'];
			if (FALSE === ($_SESSION['db'] = new database($DSN))) {
				// Interdit l'accès aux utilisateurs qui n'ont pas d'identifiant sur la base de données
				$db->db_interroge(sprintf('CALL messageSystem("L\'utilisateur n\'a pas de compte sur la base de données", "TRACE", "%s", "connection failed", "%s")'
					, __METHOD__
					, $db->db_real_escape_string(json_encode($_SESSION)))
				);
				unset($_SESSION);
				header('Location:index.php');
			}
			$_SESSION['utilisateur'] = new utilisateurGrille((int) $row['uid']);
			$_SESSION['AUTHENTICATED'] = true;
			$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("Connexion validée", "TRACE", "%s", "connection accepted", "%s")'
				, __METHOD__
				, $_SESSION['db']->db_real_escape_string(json_encode(array(
					'client'	=> $_SERVER['REMOTE_ADDR']
					, 'login'	=> $login
				))))
			);
			// Mise à jour des informations de connexion
			$upd = sprintf("
				UPDATE `TBL_USERS`
				SET `lastlogin` = NOW()
				, `nblogin` = %d
				WHERE `uid` = %d"
				, $row['nblogin'] + 1
				, $row['uid']);
			$_SESSION['db']->db_interroge($upd);
			$sql = sprintf("
				SELECT `role`
				FROM `TBL_ROLES`
				WHERE `uid` = %d
				AND beginning <= NOW()
				AND end >= NOW()"
				, $row['uid']);
			$result2 = $_SESSION['db']->db_interroge($sql);
			while ($row = $_SESSION['db']->db_fetch_array($result2)) {
				$_SESSION[strtoupper($row[0])] = true;
			}
			mysqli_free_result($result2);
		} else {
			$db->db_interroge(sprintf("
				CALL messageSystem('Tentative de connexion échouée [%s]', 'DEBUG', 'logon.php', NULL, 'login:%s;password:%s;')
				", $_SERVER['REMOTE_ADDR']
				, $db->db_real_escape_string($login)
				, $db->db_real_escape_string($pwd))
			);
			mysqli_free_result($result);
		}
	}
	/**
	 * Méthode créant une clé de réinitialisation de mot de passe
	 *
	 * Lorsqu'un compte est créé pour un utilisateur ou qu'un utilisateur
	 * veut réinitialiser son mot de passe, on doit créer une clé aléatoire
	 * qui est sauvegardée dans TBL_SIGNUP_ON_HOLD et qui sera demandée lors
	 * de la création d'un nouveau mot de passe.
	 *
	 * @param string $email L'email de l'utilisateur pour lequel on doit créer une clé de réinitialisation
	 *
	 * @return string La clé de réinitialisation
	 */
	public static function createReinitialisationKey($email) {
		$key = sha1(microtime() . rand());
		$email = filter_var(trim($email), FILTER_SANITIZE_EMAIL);
		// ajout de la clé dans la bdd
		$sql = sprintf("
			REPLACE INTO `TBL_SIGNUP_ON_HOLD`
			(`email`, `url`)
			VALUES
			('%s', '%s')
			", $_SESSION['db']->db_real_escape_string($email)
			, $key
		);
		$_SESSION['db']->db_interroge($sql);
		return $key;
	}
	/**
	 * Méthode permettant d'accepter un nouvel utilisateur dans une équipe.
	 *
	 * Cette méthode permet à un editeur d'accepter un nouvel utilisateur
	 * dans son équipe pour une période qu'il définit.
	 * L'utilisateur reçoit un message pour lui signifier qu'il doit compléter
	 * son compte pour être définitivement enregistré sur TeamTime.
	 * Ce n'est que lorsque l'utilisateur aura complété son compte qu'il sera
	 * définitivement créé sur TeamTime.
	 *
	 * @param int $id est l'index dans la table TBL_SIGNUP_ON_HOLD
	 * @param string $dateD la date d'arrivée dans l'équipe acceptant l'utilisateur (formats acceptés par la classe date)
	 * @param string $dateF la date de fin dans l'équipe acceptant l'utilisateur (formats acceptés par la classe date)
	 * @param string $grade est le grade de l'utilisateur
	 *
	 * @return boolean TRUE si le mail a été correctement envoyé, FALSE sinon
	 */
	public static function acceptUser($id, $dateD, $dateF, $grade) {
		$dateD = new Date($dateD);
		$dateF = new Date($dateF);
		$_SESSION['db']->db_interroge(sprintf("
			CALL acceptUser(%d, '%s', '%s', '%s')
			", $id
			, $dateD->date()
			, $dateF->date()
			, $_SESSION['db']->db_real_escape_string($grade)));
		//
		// Préparation du mail
		//
		$row = array(
			'description'	=> 'account accepted'
			, 'id'		=> (int) $id
		);
		//
		// Envoi du mail à l'utilisateur
		//
		if (TRUE === Email::QuickMailFromArticle($row)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	/**
	 * Vérifie si le login est déjà utilisé.
	 * 
	 * @return int 0 si le login n'existe pas.
	 *             1 si le login existe pour un nom différent
	 *             2 si le login pour un email différent
	 */
	public static function loginAlreadyExistsInDb($login, $nom, $prenom, $email) {
		$result = $_SESSION['db']->db_interroge(sprintf("
			SELECT `nom`
			FROM `TBL_USERS`
			WHERE `login` = '%s'
			AND `nom` != '%s'
			AND `prenom` != '%s'
			AND `email` != '%s'
			", $_SESSION['db']->db_real_escape_string($login)
			, $_SESSION['db']->db_real_escape_string($nom)
			, $_SESSION['db']->db_real_escape_string($prenom)
			, $_SESSION['db']->db_real_escape_string($email)
		));
		$return = false;
		if (mysqli_num_rows($result) > 0) $return = $_SESSION['db']->db_fetch_assoc($result);
		mysqli_free_result($result);
		return $return;
	}
	/**
	 * Vérifie si l'adresse mail est déjà utilisée
	 *
	 * @param string l'adresse mail à tester
	 *
	 * @return mixed false si le mail n'est pas connu
	 * 		 array[nom, prenom, login, uid] si le mail existe
	 */
	public static function emailAlreadyExistsInDb($email) {
		$result = $_SESSION['db']->db_interroge(
			sprintf("
			SELECT `uid`
			, `nom`
			, `prenom`
			, `login`
			FROM `TBL_USERS`
			WHERE `email` = '%s'
			", $_SESSION['db']->db_real_escape_string($email))
		);
		$return = false;
		if (mysqli_num_rows($result) > 0) $return = $_SESSION['db']->db_fetch_assoc($result);
		mysqli_free_result($result);
		return $return;
	}
	/**
	 * Vérifie si le compte à créer peut utiliser ce login.
	 *
	 * @param string $login le login proposé
	 * @param string $q le champ url de la table TBL_SIGNUP_ON_HOLD correspondant au compte à créer
	 *
	 * @return
	 */
	public static function canWeCreateThisLogin($login, $q) {
		$sql = sprintf("
			SELECT `nom`, `prenom`, `email`
			FROM `TBL_SIGNUP_ON_HOLD`
			WHERE `url` = '%s'
			", $_SESSION['db']->db_real_escape_string($q)
		);
		$row = $_SESSION['db']->db_fetch_assoc($_SESSION['db']->db_interroge($sql));
		if (0 == utilisateurGrille::loginAlreadyExistsInDb($login, $row['nom'], $row['prenom'], $row['email'])) {
		}
	}
	/**
	 * Méthode créant un utilisateur de TeamTime.
	 *
	 * Cette méthode fait appel à la fonction SQL createUser
	 * qui crée l'utilisateur dans la base (TBL_USERS et TBL_AFFECTATION ainsi que l'utilisateur MySQL)
	 * et lui attribue les rôles minimum (my_edit).
	 *
	 * @param array $row Le paramètre $row comprend :
	 * - nom *
	 * - prénom (prenom) *
	 * - login *
	 * - password (valeur aléatoire si vide)
	 * - locked (False par défaut)
	 * - poids
	 * - actif (False par défaut)
	 * - showtipoftheday (False par défaut)
	 * - page (affiche_grille.php par défaut définie dans la bdd)
	 * - centre (centre de l'utilisateur logué par défaut)
	 * - team (équipe de l'utilisateur logué par défaut)
	 * - grade (pc par défaut)
	 * - date d'arrivée dans l'équipe (dateD) (01/01 de l'année précédente par défaut)
	 * - date de départ de l'équipe (dateF) (31/12 de l'année en cours + 30)
	 * - sendmail = on si un mail doit être envoyé à l'utilisateur créé (False par défaut)
	 *
	 * @return int uid du nouvel utilisateur
	 */
	public static function createUser($row) {
		// Vérification des paramètres obligatoires
		if (!array_key_exists('nom', $row) ||
			!array_key_exists('prenom', $row) ||
			!array_key_exists ('email', $row)) {
			return false;
		}
		$email = filter_var(trim($row['email']), FILTER_SANITIZE_EMAIL);
		if (false === $email) return false;

		// Définition de valeurs par défaut
		if (!array_key_exists('dateD', $row)) $row['dateD'] = '01-01-' . (string)(date('Y') - 1);
		if (!array_key_exists('dateF', $row)) $row['dateF'] = '31-12-' . (string)(date('Y') + 30);
		if (!array_key_exists('centre', $row)) $row['centre'] = $_SESSION['utilisateur']->centre();
		if (!array_key_exists('team', $row)) $row['team'] = $_SESSION['utilisateur']->team();
		if (!array_key_exists('grade', $row)) $row['grade'] = 'pc';
		if (!array_key_exists('password', $row)) $row['password'] = sha1(microtime() . rand());
		$dateD = new Date($row['dateD']);
		$dateF = new Date($row['dateF']);
		$db = new database($GLOBALS['DSN']['createAccount']);
		$sql = sprintf("CALL createUser('%s', '%s', '%s', '%s', '%s', %s, %d, %s, %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')
			", $db->db_real_escape_string($row['nom'])
			, $db->db_real_escape_string($row['prenom'])
			, $db->db_real_escape_string($row['login'])
			, $db->db_real_escape_string($email)
			, $db->db_real_escape_string($row['password'])
			, array_key_exists('locked', $row) && $row['locked'] == 'on' ? 'TRUE' : 'FALSE'
			, (int) $row['poids']
			, array_key_exists('actif', $row) && $row['actif'] == 'on' ? 'TRUE' : 'FALSE'
			, array_key_exists('showtipoftheday', $row) && $row['showtipoftheday'] == 'on' ? 'TRUE' : 'FALSE'
			, $db->db_real_escape_string($row['page'])
			, $db->db_real_escape_string($GLOBALS['DSN']['user']['password'])
			, $db->db_real_escape_string($row['centre'])
			, $db->db_real_escape_string($row['team'])
			, $db->db_real_escape_string($row['grade'])
			, $dateD->date()
			, $dateF->date()
			, $GLOBALS['DSN']['user']['hostname'] == 'localhost' ? 'localhost' : '%'
		);
		$db->db_interroge($sql);
		// Envoi d'un mail pour avertir l'utilisateur de la création de son compte
		if (array_key_exists('sendmail', $row) && $row['sendmail'] == "on") {
			$sql = sprintf("
				SELECT `uid`
				FROM `TBL_USERS`
				WHERE `email` = '%s'
				", $_SESSION['db']->db_real_escape_string($email));
			$r = $_SESSION['db']->db_fetch_assoc($_SESSION['db']->db_interroge($sql));
			//
			// Préparation du mail
			//
			$arr = array(
				'description'	=> 'account created'
				, 'email'	=> $email
			);
			//
			// Envoi du mail à l'utilisateur
			//
			if (true !== Email::QuickMailFromArticle($arr)) {
				$err = "Échec : le mail n'a pas été envoyé...";
			}
		}
	}
	/**
	 * Méthode permettant de récupérer un mot de passe à partir de l'adresse mail.
	 *
	 * Cette méthode envoie un mail à un utilisateur contenant un lien qui lui permettra
	 * de mettre à jour son mot de passe.
	 *
	 * La méthode se charge de filtrer l'adresse mail passée en paramètre.
	 *
	 * @param string $email est l'adresse mail de l'utilisateur du compte à récupérer.
	 *
	 * @return string chaîne informant l'utilisateur de vérifier son mail si tout s'est bien passé ou indiquant l'erreur sinon.
	 */
	public static function resetPwd($email) {
		if (!filter_var(trim($email), FILTER_VALIDATE_EMAIL)) {
			return "L'adresse mail saisie n'est pas valide...";
		}
		$row['description'] = 'reset password';
		$row['email'] = filter_var(trim($email), FILTER_SANITIZE_EMAIL);
		$row['k'] = utilisateurGrille::createReinitialisationKey($row['email']);
		//
		// Envoi du mail à l'utilisateur
		//
		if (TRUE === Email::QuickMailFromArticle($row)) {
			return "Vous devriez recevoir un mail sous peu. Vérifiez éventuellement dans les spams si vous ne recevez rien.";
		} else {
			$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("Le mail n\'a pas été envoyé.", "DEBUG", "%s", "short", "%s")'
				, __METHOD__
				, $_SESSION['db']->db_real_escape_string(json_encode($row)))
			);
			return "Le mail n'a pas été envoyé. Contactez l'administrateur...";
		}
	}
	/**
	 * Méthode modifiant le mot de passe suite à un resetPwd.
	 *
	 * @param string $k la clé sauvegardée dans TBL_SIGNUP_ON_HOLD.url
	 * @param string $password le nouveau mot de passe
	 *
	 * @return boolean TRUE si tout s'est bien passé, FALSE sinon
	 */
	public static function resetDaPwd($k, $password) {
		$sql = sprintf("
			SELECT `email`
			FROM `TBL_SIGNUP_ON_HOLD`
			WHERE `url` = '%s'
			", $_SESSION['db']->db_real_escape_string($k)
		);
		$result = $_SESSION['db']->db_interroge($sql);
		// Aucune entrée ne correspond à la clé proposée ou plusieurs entrées lui correspondent.
		if (mysqli_num_rows($result) != 1) {
			$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("Plusieurs noms ou aucun pour l\'uid ?!", "DEBUG", "%s", "uid multiples", "k:%s")'
				, __METHOD__
				, $_SESSION['db']->db_real_escape_string($k))
			);
			return FALSE;
		}
		$row = $_SESSION['db']->db_fetch_assoc($result);
		mysqli_free_result($result);
		if (FALSE === utilisateurGrille::updatePwd($row['email'], $password, TRUE)) {
			$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("La mise à jour du mot de passe n\'a pas été effectuée.", "DEBUG", "%s", "short", "k:%s;email:%s")'
				, __METHOD__
				, $k
				, $row['email'])
			);
			return FALSE;
		}
		$sql = sprintf("
			DELETE FROM `TBL_SIGNUP_ON_HOLD`
			WHERE `url` = '%s'
			", $_SESSION['db']->db_real_escape_string($k)
		);
		$_SESSION['db']->db_interroge($sql);
		return TRUE;
	}
	/**
	 * Méthode permettant de modifier le mot de passe d'un utilisateur.
	 *
	 * Cette méthode met à jour le mot de passe de l'utilisateur à partir de son adresse mail.
	 * Si plusieurs utilisateurs avaient une adresse mail identique, ce qui ne devrait pas se produire,
	 * tous les comptes ayant cette adresse mail seraient modifiés.
	 *
	 * @param string $email est le mail de l'utilisateur. La méthode prend soin de filtrer la valeur de $email.
	 * @param string $password est le nouveau mot de passe.
	 * @param int optional si $sendmail est vrai (valeur par défaut), l'utilisateur recevra un mail confirmant la modification de mot de passe et lui rappelant son login.
	 *
	 * @return boolean TRUE si tout s'est bien passé, FALSE sinon
	 */
	public static function updatePwd($email, $password, $sendmail = TRUE) {
		$to = filter_var(trim($email), FILTER_SANITIZE_EMAIL);
		$sql = sprintf("
			UPDATE `TBL_USERS`
			SET `sha1` = SHA1(CONCAT(`login`, '%s'))
			WHERE email = '%s'
			", $_SESSION['db']->db_real_escape_string($password)
			, $_SESSION['db']->db_real_escape_string($to)
		);
		$_SESSION['db']->db_interroge($sql);
		//
		// Préparation du mail
		//
		$row = array(
			'description'	=> 'password updated'
			, 'to'		=> $to
		);
		//
		// Envoi du mail à l'utilisateur
		//
		if (TRUE === Email::QuickMailFromArticle($row)) {
			return TRUE;
		} else {
			$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("Le mail n\'a pas été envoyé.", "DEBUG", "%s", "short", "uid:%s")'
				, __METHOD__
				, $uid)
			);
			return FALSE;
		}
	}
	/**
	 * Méthode listant les pages accessibles par tous les utilisateurs à partir des entrées de menus.
	 *
	 * @return array un tableau utilisable par un html.form.select.tpl
	 */
	public static function listAvailablePages() {
		$select = array('label' => 'Première page', 'name' => 'page');
		$sql = "SELECT `titre` AS `content`, `lien` AS `value`
			FROM `TBL_ELEMS_MENUS`
			WHERE `allowed` = 'all'
			AND `titre` != 'logout'";
		$result = $_SESSION['db']->db_interroge($sql);
		while($row = $_SESSION['db']->db_fetch_assoc($result)) {
			$select['options'][] = $row;
		}
		mysqli_free_result($result);
		return $select;
	}
	/**
	 * Méthode générant une vCard à partir d'un uid
	 * @param int $uid
	 *  
	 * @return file vcard.vcf
	 */
	public static function generateVCard($uid)
	{
		if ($uid != (int) $uid || $uid <= 0) {
			return false;
		}
		$sql = sprintf("SELECT `nom`, `prenom`, `email`
			FROM `TBL_USERS`
			WHERE `uid` = %d
			AND `actif` IS TRUE
			AND `annuaire` IS TRUE", $uid);
		$row = $_SESSION['db']->db_fetch_assoc( $_SESSION['db']->db_interroge($sql) );
		if (! is_array($row)) {
			return false;
		}
		$vcard = new vCard;
		$vcard->setName($row['prenom'], $row['nom']);
		$vcard->setMail($row['email']);
		$sql = sprintf("SELECT `adresse`, `cp`, `ville`
			FROM `TBL_ADRESSES`
			WHERE `uid` = %d
			LIMIT 1", $uid);
		$row = $_SESSION['db']->db_fetch_assoc( $_SESSION['db']->db_interroge($sql) );
		if (is_array($row)) {
			$vcard->setAddress(array(
				"street_address"	=> $row['adresse'],
				"city"			=> $row['ville'],
				"postal_code"		=> $row['cp'],
				"state"			=> "",
				"country_name"		=> ""
			));
		}
		$sql = sprintf("SELECT `phone`, `description`
			FROM `TBL_PHONE`
			WHERE `uid` = %d", $uid);
		$result = $_SESSION['db']->db_interroge($sql);
		while($row = $_SESSION['db']->db_fetch_assoc($result)) {
			$vcard->setPhone(array(
				'phone'	=> $row['phone'],
				'type'	=> $row['description']
			));
		}
		mysqli_free_result($result);
		return $vcard;
	}
	/**
	 * Quelle est la vacation d'une équipe à la date $date
	 *
	 * @param string $centre le centre de l'équipe
	 * @param string $team l'équipe recherchée
	 * @param Date $date la date pour laquelle on recherche la vacation
	 *
	 * @return string la vcacation recherchée
	 */
	public static function whatIsTeamSVacation($centre, $team, $date) {
		if (!is_a($date, 'Date')) {
			return false;
		}
		$sql = sprintf("
			SELECT `vacation`
			FROM `TBL_CYCLE`
			WHERE `cid` = (
				SELECT `cid`
				FROM `TBL_GRILLE`
				WHERE `date` = '%s'
				AND `centre` = '%s'
				AND `team` = '%s'
			)
			", $date->date()
			, $centre
			, $team
		);
		$row = $_SESSION['db']->db_fetch_assoc($_SESSION['db']->db_interroge($sql));
		return $row['vacation'];
	}
// Constructeur
	public function __construct ($param = NULL) {
		//firePhpLog($param, "Création d'un nouvel objet utilisateurGrille");
		if (NULL !== $param) {
			if (is_array($param) && sizeof($param) == 1 && array_key_exists('uid', $param)) {
				$param = (int) $param['uid'];
			}
			if (is_array($param)) {
				parent::page('affiche_grille.php'); // La page par défaut des utilisateurs
				parent::__construct($param);
				$this->gid = 255; // Par défaut, on fixe le gid à la valeur la plus élevée
				return @ $this->setFromRow($param); // Retourne true si l'affectation s'est bien passée, false sinon
			} elseif (is_int($param)) {
				$this->setFromDb($param);
			} else {
				$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("Le paramètre du constructeur n\'est pas d\'un type attendu", "ERROR", "%s", "wrong param", "%s")'
					, __METHOD__
					, $_SESSION['db']->db_real_escape_string(json_encode($param)))
				);
			}
		}
		return true;
	}
	public function __destruct() {
		parent::__destruct();
		unset($this);
	}
	// Accesseurs
	/**
	 * Retourne les infos utilisateurs sous forme de tableau
	 *
	 * @param $secure boolean true si les données doivent être sécurisées
	 *        (ne pas apparaître) ie le hash du mot de passe sera supprimé
	 *
	 * @return array
	 */
	public function userAsArray($secure = false) {
		$aff = array();
		foreach($this->orderedAffectations() as $i => $affectation) {
			$aff[$i++] = $affectation->asArray();
		}
		$array = array_merge(
			parent::asArray()
		       	, array(
			'uid'			=> $this->uid
			, 'nom'			=> $this->nom
			, 'prenom'		=> $this->prenom
			, 'vismed'		=> $this->vismed
			, 'poids'		=> $this->poids
			, 'showtipoftheday'	=> $this->showtipoftheday
			, 'pref'		=> $this->prefAsJSON()
			, 'centre'		=> $this->centre()
			, 'team'		=> $this->team()
			, 'grade'		=> $this->grade()
			, 'affectations'	=> $aff
		));
		unset($array['sha1']);
		return $array;
	}
	public function asJSON() {
		return json_encode($this->userAsArray(true));
	}
	public function prefAsArray() {
		return $this->pref;
	}
	public function prefAsJSON() {
		return json_encode($this->pref);
	}
	public function setFromRow($row) {
		$valid = true;
		foreach ($row as $key => $value) {
			if (method_exists($this, $key)) {
				$this->$key($value);
			} else {
				/*$this->$key = $value;
				firePhpError($this->$key . " => " . $value, 'Valeur inconnue');
				debug::getInstance()->triggerError('Valeur inconnue' . $this->$key . " => " . $value);
				debug::getInstance()->lastError(ERR_BAD_PARAM);*/
				$valid = false;
			}
		}
		return $valid;
	}
	public function uid($uid = NULL) {
		if (!is_null($uid)) {
			$this->uid = (int) $uid;
		}
		if (isset($this->uid)) {
			return $this->uid;
		} else {
			return NULL;
		}
	}
	public function gid($gid = NULL) {
		if (!is_null($gid)) {
			$this->gid = (int) $gid;
		}
		if (isset($this->gid)) {
			return $this->gid;
		} else {
			return false;
		}
	}
	public function nom($nom = NULL) {
		if (!is_null($nom)) {
			$this->nom = (string) $nom;
		}
		if (isset($this->nom)) {
			return $this->nom;
		} else {
			return false;
		}
	}
	public function prenom($prenom = NULL) {
		if (!is_null($prenom)) {
			$this->prenom = (string) $prenom;
		}
		if (isset($this->prenom)) {
			return $this->prenom;
		} else {
			return false;
		}
	}
	public function annuaire($annuaire = NULL) {
		if (!is_null($annuaire)) {
			$this->annuaire = $annuaire;
		}
		if (isset($this->annuaire)) {
			return $this->annuaire;
		} else {
			return false;
		}
	}
	/**
	 * Vérifie si le paramètre passé est l'uid de l'utilisateur
	 *
	 * @param $uid int l'id à tester
	 *
	 * @return boolean true si l'uid est celui de l'utilisateur
	 *                 false sinon
	 */
	public function checkUid($uid) {
		return $_SESSION['utilisateur']->uid() == $uid;
	}
	/**
	 * Chargement des préférences utilisateur à partir d'une chaîne JSON.
	 *
	 * @param $param string chaîne JSON
	 */
	public function pref($param = NULL) {
		global $conf;
		if (is_string($param)) {
			$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("msg", "TRACE", "%s", "short", "%s")'
				, __METHOD__
				, $_SESSION['db']->db_real_escape_string(json_encode($param)))
			);
			$this->pref = json_decode($param, false);
			// Ajoute les compteurs en fin de grille sur tous les affichages
			if (is_array($this->pref) && array_key_exists('cpt', $this->pref)) {
				setcookie('cpt', (int) $this->pref->cpt, 0, $conf['session_cookie']['path'], NULL, $conf['session_cookie']['secure']);
			}
		} elseif (!is_array($this->pref)) {
			$sql = "SELECT `pref`
				FROM `TBL_USERS`
				WHERE `uid` = $this->uid";
			$row = $_SESSION['db']->db_fetch_assoc($_SESSION['db']->db_interroge($sql));
			$this->pref = json_decode($row['pref'], true);
		}
		return $this->pref;
	}
	/**
	 * Ajout d'une préférence utilisateur.
	 *
	 * @param $key string la clé de la préférence
	 * 	$value string la valeur de la préférence
	 *
	 * @return void
	 */
	public function addPref($key, $value) {
		$this->pref->$key = $value;
	}
	/**
	 * Retrouve la valeur d'une préférence.
	 *
	 * @param $param string clé indexant la préférence à rechercher
	 *
	 * @return string la valeur correspondant à la clé
	 */
	public function getPref($param) {
		if (isset($this->pref->$param)) {
			return $this->pref->$param;
		} else {
			return null;
		}
	}
	/**
	 * Retourne le centre actuel de l'utilisateur.
	 *
	 * @return string centre actuel de l'utilisateur
	 */
	public function centre() {
		$affectation = $this->affectationOnDate(date('Y-m-d'));
		return $affectation['centre'];
	}
	/**
	 * Retourne le team actuel de l'utilisateur.
	 *
	 * @return string équipe actuelle de l'utilisateur
	 */
	public function team() {
		$affectation = $this->affectationOnDate(date('Y-m-d'));
		return $affectation['team'];
	}
	/**
	 * Retourne le grade actuel de l'utilisateur.
	 *
	 * @return string grade actuel de l'utilisateur
	 */
	public function grade() {
		$affectation = $this->affectationOnDate(date('Y-m-d'));
		return $affectation['grade'];
	}
	/**
	 * Ajoute un téléphone unique.
	 *
	 * @param array $row
	 *
	 * @return int $phoneid
	 */
	public function addPhone($row) {
		$row['uid'] = $this->uid();
		$phone = new Phone($row);
		$phoneid = $phone->insert();
		firePhpLog($phoneid, 'phoneid');
		$this->phone[$phoneid] = $phone;
		return $phoneid;
	}
	/**
	 * Ajoute les téléphones venant d'un tableau.
	 *
	 * @param array $array
	 * 	 = ( 0 => ('numéro' => '1010101010'
	 * 		'description'	=> 'maison'
	 * 		'principal'	=> 'on'
	 * 		)
	 * 	      1 => (...)
	 * 	      )
	 *
	 * @return int nombre de numéros de téléphone de l'utilisateur
	 */
	public function addPhoneTableau($array) {
		$valid = true;
		firePhpLog($array, 'addPhoneTableau(array)');
		foreach ($array as $arr) {
			if (is_array($arr)) {
				$this->addPhone($arr);
			} else {
				$valid = false;
				break;
			}
		}
		firePhpLog($valid, 'valid');
		if (!$valid) $this->addPhone($array);
		return sizeof($this->phone);
	}
	public function deletePhone($phoneid) {
		$this->phone[$phoneid]->delete();
		unset($this->phone[$phoneid]);
	}
	/**
	 * Retourne la table des objets Phone.
	 *
	 * @param int $param optional retourne l'objet Phone indexé par $param
	 *
	 * @return object phone object
	 */
	public function phone($param = NULL) {
		if (sizeof($this->phone) == 0) { // Si on n'a pas encore récupéré les téléphones dans la bdd
			$this->_retrievePhone($param);
		}
		if (is_int($param)) { // Si $param est l'index d'un téléphone, on retourne l'objet correspondant
			firePhpLog($param, 'is_int');
			return $this->phone[$param];
		}
		if (is_array($param)) { // Si $param est un tableau, il contient les infos d'un(e) nouveau(x) téléphone(s)
			firePhpLog($param, 'is_array');
			$this->addPhoneTableau($param);
		}
		return $this->phone;
	}
	/**
	 * Ajoute une adresse postale unique.
	 *
	 * @param array $row les données d'adresse
	 *
	 * @return int adresseid index de la nouvelle adresse
	 */
	public function addAdresse($row) {
		$row['uid'] = $this->uid();
		$adresse = new Adresse($row);
		$adresseid = $adresse->insert();
		$this->adresse[$adresseid] = $adresse;
		return $adresseid;
	}
	/**
	 * Ajoute les adresses venant d'un tableau.
	 *
	 * @param array $array =
	 *            ( 0 => ('adresse' => '10 rue des alouettes'
	 * 		'cp'	=> '70000'
	 * 		'ville'	=> 'ville de lumière'
	 * 		)
	 * 	      1 => (...)
	 * 	      )
	 *
	 * @return int nombre d'adresses de l'utilisateur
	 */
	public function addAdresseTableau($array) {
		$valid = true;
		foreach ($array as $arr) {
			if (is_array($arr)) {
				$this->addAdresse($arr);
			} else {
				$valid = false;
				break;
			}
		}
		if (!$valid) $this->addAdresse($array);
		return sizeof($this->adresse);
	}
	public function deleteAdresse($adresseid) {
		$this->adresse[$adresseid]->delete();
		unset($this->adresse[$adresseid]);
	}
	public function adresse($param = NULL) {
		if (sizeof($this->adresse) == 0) $this->_retrieveAdresse($param);
		if (is_int($param)) return $this->adresse[$param]; // Si $param est l'index d'une adresse, on retourne l'objet correspondant
		if (is_array($param)) { // Si $param est un tableau, il contient les infos d'une(e) nouvelle(s) adresse(s)
			$this->addAdresseTableau($param);
		}
		return $this->adresse;
	}
	/**
	 * Retourne les rôles (sous forme de tableau).
	 *
	 * @return array Tableau contenant les différents rôles de l'utilisateur.
	 */
	public function roles() {
		if (!empty($TRACE) && true === $TRACE) {
			$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("", "DEBUG", "roles", NULL, "uid:%d;sizeof(roles):%d")'
				, $this->uid
				, sizeof($this->roles))
			);
		}
		if (sizeof($this->roles) < 1) {
			$this->dbRetrRoles();
			if (!empty($TRACE) && true === $TRACE) {
				$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("roles array is empty.", "DEBUG", "roles", NULL, "uid:%d;sizeof(roles):%d")'
					, $this->uid
					, sizeof($this->roles))
				);
			}
		}
		return $this->roles;
	}
	/**
	 * Teste si un utilisateur a un rôle en particulier.
	 *
	 * @param $role le nom du rôle à tester.
	 *
	 * @return boolean vrai si l'utilisateur a le rôle $role, false sinon.
	 */
	public function hasRole($role) {
		return in_array($role, $this->roles());
	}
	/**
	 * Teste si l'utilisateur est admin.
	 *
	 * @return boolean true si l'utilisateur est admin, false sinon.
	 */
	public function isAdmin() {
		return $this->hasRole('admin');
	}
	// Attribue les rôles en fonction de la base de données
	public function dbRetrRoles() {
		$sql = sprintf("
			SELECT role
			FROM `TBL_ROLES`
			WHERE uid = %d
			AND '%s' BETWEEN `beginning` AND `end`
			", $this->uid
			, date('Y-m-d')
		);
		if (!empty($TRACE) && true === $TRACE) {
			$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("%s", "DEBUG", "dbRetrRoles", "requête roles", "uid:%d")'
				, $sql
				, $this->uid)
			);
		}
		$result = $_SESSION['db']->db_interroge($sql);
		while($row = $_SESSION['db']->db_fetch_assoc($result)) {
			$this->roles[] = $row['role'];
		}
	}
	/**
	 * Ajoute un rôle à l'utilisateur.
	 *
	 * L'utilisateur qui ajoute un rôle à un autre utilisateur doit au moins disposer dudit rôle.
	 *
	 * @param array $param est un tableau :
	 * ('role' => role
	 * , 'beginning' => beginning
	 * , 'end' => end
	 * , 'centre' => centre
	 * , 'team' => team
	 * , 'comment' => comment )
	 * Si beginning et end ne sont pas définis, beginning prend la valeur de la date courante et end est fixé à 2050-12-31
	 * si centre et team ne sont pas définis, on utilise l'affectation courante de l'utilisateur
	 */
	public function addRole($param) {
		if (!is_array($param) || !isset($param['role'])) {
			$msg = sprintf("\$param devrait être un array (%s) et \$param['role'] doit être défini", $param);
			$short = "wrong param";
			$context = $param;
			$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("%s", "DEBUG", "addRole", "%s", "%s")'
				, $msg
				, $short
				, $context)
			); 
			return false;
		}
		if ($this->hasRole($param['role'])) {
			return true;
		}
		$affectation = $_SESSION['utilisateur']->affectationOnDate(date('Y-m-d'));
		if ( $_SESSION['utilisateur']->hasRole($param['role']) || $_SESSION['utilisateur']->isAdmin() ) {
			$_SESSION['db']->db_interroge(sprintf("
				CALL addRole(%d, '%s', '%s', '%s', '%s', '%s', '%s', TRUE)
				", $this->uid
				, $param['role']
				, isset($param['centre']) ? $param['centre'] : $affectation['centre']
				, isset($param['team']) ? $param['team'] : $affectation['team']
				, isset($param['beginning']) ? $param['beginning'] : date('Y-m-d')
				, isset($param['end']) ? $param['end'] : $affectation['end']
				, isset($param['comment']) ? $param['comment'] : ''
			));
			// TODO réévaluer les privilèges de l'utilisateur sur la base de données
			// Un utilisateur lambda ne doit pas avoir accès en écriture à certaines tables
			$this->roles = array();
			$this->dbRetrRoles();
			$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("Ajout de rôle", "DEBUG", "addRole", "Ajout de rôle", "uid:%d;role:%s;appelant:%d")'
				, $this->uid()
				, $param['role']
				, $_SESSION['utilisateur']->uid()
				)
			); 
		} else {
			$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("Tentative d\'ajout de rôle refusée", "DEBUG", "addRole", "operation rejected", "uid:%d;role:%s;appelant:%d")'
				, $this->uid()
				, $param['role']
				, $_SESSION['utilisateur']->uid()
				)
			); 
		}
	}
	/**
	 * Retire un rôle à l'utilisateur.
	 *
	 * La méthode vérifie si l'utilisateur possède déjà ce rôle.
	 *
	 * @param string $role le rôle à attribuer.
	 *
	 * @return void
	 */
	public function dropRole($role) {
		if ( $_SESSION['utilisateur']->hasRole($role) ) {
			$sql = sprintf("
				DELETE FROM `TBL_ROLES`
				WHERE `uid` = %d
				AND `role` = '%s'
				", $this->uid
				, $role
			);
			$_SESSION['db']->db_interroge($sql);
			// TODO réévaluer les privilèges de l'utilisateur sur la base de données
			$this->roles = array();
			$this->dbRetrRoles();
		}
	}
	public function vismed($vismed = NULL) {
		if (!is_null($vismed)) {
			$this->vismed = new Date($vismed);
		}
		return $this->vismed;
	}
	/**
	 * Ajoute une affectation à l'utilisateur.
	 *
	 * la bdd est mise à jour.
	 *
	 * @param array $row
	 * - 'centre' => 
	 * - 'team' => 
	 * - 'beginning' => 
	 * - 'end' => 
	 * - 'grade' => 
	 * 
	 */
	public function addAffectation($row) {
		if (!is_array($row)) return false;
		$row['uid'] = $this->uid();
		$affectation = new Affectation($row);
		$aid = $affectation->insert();
		unset($this->affectationOnDate);
		return true;
	}
	/**
	 * Ajoute les affectations venant d'un tableau.
	 *
	 * @param array $array = ( 0 => ('aid' => 5
	 * 		'centre'	=> 'LFFFE'
	 * 		'team'	=> '9'
	 * 		'beginning'	=> '1990-01-01'
	 * 		'end'	=> '2000-01-01'
	 * 		)
	 * 	      1 => (...)
	 * 	      )
	 */
	public function addAffectationsTableau($array) {
		$valid = true;
		foreach ($array as $arr) {
			if (is_array($arr)) {
				$this->addAffectation($arr);
			} else {
				$valid = false;
				break;
			}
		}
		if (!$valid) $this->addAffectation($array);
		return sizeof($this->affectations);
	}
	public function deleteAffectation($aid) {
		$this->affectations[$aid]->delete();
		unset($this->affectations[$aid]);
	}
	public function affectationOnDate($date) {
		if (!is_a($date, 'Date')) {
			$date = new Date($date);
		}
		if (isset($this->cacheAffectation[$date->date()]) && is_array($this->cacheAffectation[$date->date()])) {
			return $this->cacheAffectation[$date->date()];
		}
		$sql = sprintf("
			SELECT `centre`, `team`, `grade`, `beginning`, `end`
			FROM `TBL_AFFECTATION`
			WHERE `uid` = %d
			AND '%s' BETWEEN `beginning` AND `end`
			AND `principale` IS FALSE
			", $this->uid()
			, $date->date()
		);
		$result = $_SESSION['db']->db_interroge($sql);
		if (mysqli_num_rows($result) <= 0) {
			return false;
		}
		$this->cacheAffectation[$date->date()] = $_SESSION['db']->db_fetch_assoc($result);
		return $this->cacheAffectation[$date->date()];
	}
	/**
	 * Retourne l'affectation débutant après la date passée.
	 *
	 * Cette méthode permet de retrouver l'affectation débutant après la
	 * date passée. Cela permet d'obtenir un résultat pour les utilisateurs qui
	 * n'ont pas encore d'affectation à la date interrogée.
	 *
	 * @param Date un objet date représentant la date interrogée.
	 *
	 * @return array tableau de l'affectation.
	 *
	 */
	public function nextAffectation($date)
	{
		$sql = sprintf("
			SELECT `centre`, `team`, `grade`, `beginning`, `end`
			FROM `TBL_AFFECTATION`
			WHERE `uid` = %d
			AND '%s' <= `beginning`
			ORDER BY `beginning` ASC
			LIMIT 1
			", $this->uid()
			, $date->date()
		);
		$result = $_SESSION['db']->db_interroge($sql);
		if (mysqli_num_rows($result) <= 0) {
			return false;
		}
		$this->cacheNextAffectation[$date->date()] = $_SESSION['db']->db_fetch_assoc($result);
		return $this->cacheNextAffectation[$date->date()];
	}
	/**
	 * Retourne un tableau des affectations en ordre croissant.
	 *
	 * @param void
	 *
	 * @return array tableau des affectations
	 */
	public function orderedAffectations() {
		$result = $_SESSION['db']->db_interroge(sprintf("
			SELECT *
		       	FROM `TBL_AFFECTATION`
			WHERE `uid` = %d
			ORDER BY `principale`, `end` ASC
			", $this->uid()
		));
		$orderedAffectations = array();
		$i = 0;
		while ($row = $_SESSION['db']->db_fetch_assoc($result)) {
			$orderedAffectations[$i++] = new Affectation($row);
		}
		mysqli_free_result($result);
		return $orderedAffectations;
	}
	/**
	 * Méthode retournant l'affectation principale à la date passée
	 *
	 * @param string $date date de l'affectation
	 *
	 * @return object Affectation l'objet affectation contenant l'affectation principale
	 */
	public function affectationPrincipale($date = NULL) {
		if (is_string($date)) {
			$date = new Date($date);
		}
		if (is_null($date) || !is_a($date, 'Date')) {
			$date = new Date(date('Y-m-d'));
		}
		// Si une entrée existe déjà pour cette date, elle est retournée
		if (array_key_exists($date->date(), $this->affectationPrincipale)) {
			return $this->affectationPrincipale[$date->date()];
		}
		$sql = sprintf("SELECT *
			FROM `TBL_AFFECTATION`
			WHERE `uid` = %d
			AND '%s' BETWEEN `beginning` AND `end`
			AND principale IS TRUE
			"
			, $this->uid()
			, $date->date()
		);
		return $this->affectationPrincipale[$date->date()] = new Affectation($_SESSION['db']->db_fetch_assoc($_SESSION['db']->db_interroge($sql)));
	}
	public function poids($poids = NULL) {
		if (!is_null($poids)) {
			$this->poids = (int) $poids;
		}
		if (isset($this->poids)) {
			return $this->poids;
		} else {
			return -1;
		}
	}
	public function showtipoftheday($showtipoftheday = NULL) {
		if (!is_null($showtipoftheday)) {
			$this->showtipoftheday = ($showtipoftheday == 1 ? 1 : 0);
		}
		if (isset($this->showtipoftheday)) {
			return $this->showtipoftheday;
		} else {
			return false;
		}
	}
	public function dispos($dispos = NULL) {
		if (is_array($dispos)) {
			$this->dispos = $dispos;
		}
		if (isset($this->dispos)) {
			return $this->dispos;
		} else {
			return false;
		}
	}
	/**
	* Retourne les pages disponibles après la connexion pour l'utilisateur.
	*
	* @param string $titre
	* @param int $index
	*
	* @return array les pages accessibles à l'utilisateur
	* @return boolean NULL si les paramètres ne sont pas corrects
	 */
	public function availablePages($type = 'titre', $index = NULL) {
		firePhpLog("In availablePages");
		if ($type != 'titre' && $type != 'uri' && $type != 'index') {
			firePhpLog("Mauvais type pour les AvailablePages", $type);
			return NULL;
		}
		if (!is_null($index)) {
			$index = (int) $index;
			if (empty(self::$availablePages[$index][$type]) || self::$availablePages[$index]['gid'] < $this->gid()) $index = 1;
			return self::$availablePages[$index][$type];
		}
		$pages = array();
		foreach (self::$availablePages as $index => $array) {
			firePhpLog('gid', $this->gid());
			if ($array['gid'] >= $this->gid()) {
				if ($type == 'titre') {
					$pages[$index] = $array['titre'];
				} elseif ($type == 'uri') {
					$pages[$index] = $array['uri'];
				} else {
					firePhpWarn("Mauvais type pour les availablesPages", $type);
					return NULL;
				}
			}
		}
		return $pages;
	}
	public function indexPage($index = NULL) {
		if (!is_null($index)) {
			$this->index = (int) $index;

		}
		if (empty($this->indexPage)) {
			foreach (self::$availablePages as $index => $array) {
				if ($this->page() == $array['uri']) {
					$this->indexPage = $index;
					break;
				}
			}
		}
		return $this->indexPage;
	}
	/**
	 * Quelle est la vacation de l'utilisateur à la date $date
	 *
	 * @param Date $date la date pour laquelle on veut la vacation
	 *
	 * @return string la vacation recherchée
	 */
	public function whatIsMyVacation($date) {
		if (!is_a($date, 'Date')) {
			return false;
		}
		$affectation = $this->affectationOnDate($date);
		return utilisateurGrille::whatIsTeamSVacation($affectation['centre'], $affectation['team'], $date);
	}
	/**
	 * Liste les activités que peut poser l'utilisateur pour un utilisateur à une date définie.
	 *
	 * @param Date $date la date pour laquelle on recherche les activités
	 * @param int $uid default=NULL l'uid de l'utilisateur pour lequel on recherche l'activité.
	 *            Si $uid n'est pas fourni, c'est l'uid de l'utilisateur qui utilise la méthode
	 *
	 *
	 * @return array liste des activités possibles
	 */
	public function listAvailableActivities($date, $uid = NULL) {
		if (!is_a($date, Date)) {
			return false;
		}
		if (is_null($uid)) {
			$utilisateur = $_SESSION['utilisateur'];
		} else {
			$utilisateur = new utilisateurGrille($uid);
		}
		$arr = array();
		$affectation = $utilisateur->affectationOnDate($date);
		$find_in_set_user = '';
		$find_in_set_classes = '';
		$find_in_set_vacation = '';

		// La condition sur la vacation
		foreach (array_flip(array_flip(array_merge(array('all', $this->whatIsTeamSVacation($affectation['centre'], $affectation['team'], $date))))) as $set) {
			$find_in_set_vacation .= sprintf("FIND_IN_SET('%s', `Jours possibles`) OR ", $_SESSION['db']->db_real_escape_string($set));
		}
		$find_in_set_vacation = substr($find_in_set_vacation, 0, -4);

		// La condition sur qui peut recevoir l'activité
		foreach (array_flip(array_flip(array_merge(array('all', $utilisateur->login(), $affectation['grade']), $utilisateur->roles()))) as $set) {
			$find_in_set_classes .= sprintf("FIND_IN_SET('%s', `classes possibles`) OR ", $_SESSION['db']->db_real_escape_string($set));
		}
		$find_in_set_classes = substr($find_in_set_classes, 0, -4);

		// La condition sur qui peut poser
		foreach (array_flip(array_flip(array_merge(array('all', $_SESSION['utilisateur']->login(), $affectation['grade']), $_SESSION['utilisateur']->roles()))) as $set) {
			$find_in_set_user .= sprintf("FIND_IN_SET('%s', `peut poser`) OR ", $_SESSION['db']->db_real_escape_string($set));
		}
		$find_in_set_user = substr($find_in_set_user, 0, -4);
		$sql = sprintf("
			SELECT `dispo`,
			`did`,
			`nom_long`
			FROM `TBL_DISPO`
			WHERE (FIND_IN_SET('%s', `centres`) OR `centres` = 'all')
			AND (`team` = '%s' OR `team` = 'all')
			AND (%s)
			AND (%s)
			AND (%s)
			ORDER BY `poids`"
			, $affectation['centre']
			, $affectation['team']
			, $find_in_set_user
			, $find_in_set_classes
			, $find_in_set_vacation
		);
		$result = $_SESSION['db']->db_interroge($sql);
		while($row = $_SESSION['db']->db_fetch_assoc($result)) {
			$arr[] = $row;
		}
		mysqli_free_result($result);
		return $arr;
	}
	public function messages() {
		return $this->messages;
	}
	public function retrMessages() {
		$result = $_SESSION['db']->db_interroge("SELECT *
			FROM `TBL_MESSAGES_SYSTEME`
			WHERE `catégorie` = 'USER'
			AND lu IS FALSE
			AND `utilisateur` = 'ttm." . $this->uid . "@localhost'");
		while ($row = $_SESSION['db']->db_fetch_assoc($result)) {
			$this->messages[] = new message($row);
		}
		return $this->messages;
	}
	public function flushMessages() {
		$this->messages = array();
	}
// Méthodes relatives à la base de données
	public function setFromDb($uid) {
		$sql = sprintf("
			SELECT *
			FROM `TBL_USERS`
			WHERE `uid` = %d
			", $uid
		);
		$result = $_SESSION['db']->db_interroge($sql);
		$row = $_SESSION['db']->db_fetch_assoc($result);
		parent::__construct($row);
		$this->setFromRow($row);
		$this->dbRetrRoles();
		$this->_retrieveContact();
	}
	/**
	 * Vérifie si l'utilisateur existe déjà dans la base de données.
	 *
	 * Pour cela, on vérifie si l'email est déjà présent dans la bdd.
	 */
	public function emailExistsInDb() {
		return utilisateurGrille::emailAlreadyExistsInDb($this->email);
	}
	/************************
	 * Gestion des contacts *
	 ************************/
	protected function _retrieveAdresse($index = NULL) {
		firePhpLog($index, '_retrieveAdresse');
		$sql = sprintf("
			SELECT *
			FROM `TBL_ADRESSES`
			WHERE `uid` = %d"
			, $this->uid
		);
		$result = $_SESSION['db']->db_interroge($sql);
		while ($row = $_SESSION['db']->db_fetch_assoc($result)) {
			$this->adresse[$row['adresseid']] = new Adresse($row);
		}
		mysqli_free_result($result);
		if (is_null($index)) return $this->adresse;
		if (isset($this->adresse[$index])) return $this->adresse[$index];
		return NULL;
	}
	protected function _retrievePhone($index = NULL) {
		firePhpLog($index, '_retrievePhone');
		$sql = sprintf("
			SELECT *
			FROM `TBL_PHONE`
			WHERE `uid` = %d"
			, $this->uid
		);
		$result = $_SESSION['db']->db_interroge($sql);
		while ($row = $_SESSION['db']->db_fetch_assoc($result)) {
			$this->phone[$row['phoneid']] = new Phone($row);
		}
		mysqli_free_result($result);
		if (is_null($index)) return $this->phone;
		if (isset($this->phone[$index])) return $this->phone[$index];
		return NULL;
	}
	protected function _retrieveContact() {
		$this->_retrieveAdresse();
		$this->_retrievePhone();
	}
	/*
	 * Mise à jour des informations
	 */
	protected function _updateUser() {
		$array = $this->userAsArray();
		unset($array['vismed']);
		unset($array['lastlogin']);
		return $_SESSION['db']->db_update('TBL_USERS', $array);
	}
	protected function _updatePhone() {
		foreach ($this->phone() as $phone) {
			$phone->update();
		}
	}
	protected function _updateAdresse() {
		foreach ($this->adresse() as $adresse) {
			$adresse->update();
		}
	}
	public function updateContact() {
		$this->_updatePhone();
		$this->_updateAdresse();
	}
	public function fullUpdateDB() {
		$this->_updateUser();
		$this->updateContact();
		//$this->_updateClasse();
	}
// Méthodes utiles pour l'affichage
	public function userCell($dateDebut) {
		return array('nom'	=> htmlentities($this->nom())
			,'classe'	=> 'nom '
			,'id'		=> "u". $this->uid()
			,'uid'		=> $this->uid()
		);
	}
	// Prépare l'affichage des informations de l'utilisateur
	// Retourne un tableau dont la première ligne contient les noms des champs
	// et la seconde un tableau avec le contenu des champs accompagnés
	// d'informations nécesasires pour l'affichage html (id...)
	// $champs est un tableau contenant les champs à retourner
	public function displayUserInfos($champs) {
		$table = array();
		$index = 0;
		foreach ($champs as $champ) {
			$table[$champ] = array('Field'	=> $champ
				, 'content'		=> method_exists($champs, 'utilisateurGrille') ? $this->$champ() : 'unknown'
				, 'id'			=> $champ . $this->uid()
				, 'label'		=> _label($champ)
			);
		}
		return $table;
	}
}

?>
