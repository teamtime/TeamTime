<?php
// class_mail.inc.php
//
/**
 * Permet d'envoyer des emails aux utilisateurs.
 *
 * Cette classe s'appuie sur PHPMailer (https://github.com/PHPMailer/PHPMailer)
 */

/*
	TeamTime is a software to manage people working in team on a cyclic shift.
	Copyright (C) 2012 Manioul - webmaster@teamtime.me

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

set_include_path(implode(PATH_SEPARATOR, array(realpath('.'), get_include_path())));


class Email {
	private $err = NULL;
	private $crlf = "\n";
	/**
	 * Méthode statique pour envoyer rapidement un email.
	 *
	 * @param string $to adresse du destinataire du mail
	 * @param string $subject sujet du mail
	 * @param string $content contenu du mail
	 *
	 * @return boolean TRUE si l'envoi s'est bien passé, FALSE sinon
	 */
	public static function QuickMail($to, $subject, $content) {
		$mail = new PHPMailer;
		$mail->setFrom($GLOBALS['email']['sender'], 'No Reply');
		$mail->Subject = utf8_decode($subject);
		//$mail->Charset = 'utf-8';
		$mail->Body = utf8_decode($content);
		$mail->addAddress($to);
		if (array_key_exists('BCC', $GLOBALS['email'])) $mail->addBCC($GLOBALS['email']['BCC']);
		//$mail->AltBody = $content;
		if ('smtp' == $GLOBALS['email']['emailconf']['backend']) {
			$mail->isSMTP();
			$mail->Host = $GLOBALS['email']['emailconf']['host'];
			if (array_key_exists('port', $GLOBALS['email']['emailconf'])) {
				$mail->port = $GLOBALS['email']['emailconf']['port'];
			}
			if ($GLOBALS['email']['emailconf']['auth']) {
				$mail->SMTPAuth = true;
				$mail->Username = $GLOBALS['email']['emailconf']['username'];
				$mail->Password = $GLOBALS['email']['emailconf']['password'];
			}
			// Encryption utilisée (tls, ssl ou null)
			if (!is_null($GLOBALS['email']['emailconf']['encryption'])) {
				$mail->SMTPSecure = $GLOBALS['email']['emailconf']['encryption'];
			}
		}

		if ($mail->send()) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	/** Méthode statique pour envoyer un mail de test à $GLOBALS['email']['testmail']
	 *
	 * @return boolean TRUE si l'envoi s'est bien passé, false sinon
	 */
	public static function testemail()
	{
		return Email::QuickMail($GLOBALS['email']['testmail'], 'Mail de test', 'Ceci est un mail de test de TeamTime (origine : ' . $_SERVER['REMOTE_ADDR'] . ').');
	}
	/**
	 * Méthode statique pour envoyer rapidement
	 * un email à partir d'un contenu provenant de la bdd.
	 *
	 * @param array string[] $array un tableau contenant les éléments nécessaires
	 * 			à la construction du contenu du message.
	 * - $array['description'] la description permettant de
	 *   retrouver le contenu du mail
	 * - $array['to'] si il existe est le destinataire du mail
	 * - ...
	 * Les autres paramètres inclus dans le tableau dépendent du type
	 * de mail à envoyer.
	 * Cf les méthodes privées correspondantes...
	 *
	 * @return boolean TRUE si l'envoi s'est bien passé, FALSE sinon
	 */
	public static function QuickMailFromArticle($array) {
		$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("paramètre de la méthode.", "TRACE", "%s", "param", "%s")'
			, __METHOD__
			, $_SESSION['db']->db_real_escape_string(json_encode($array)))
		);
		if (!array_key_exists('description', $array)) {
			$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("description manquante pour le mail.", "DEBUG", "%s", "param description expected", "%s")'
				, __METHOD__
				, $_SESSION['db']->db_real_escape_string(json_encode($array)))
			);
			return FALSE;
		}
		if ($array['description'] == 'account accepted') {
			$row = Email::accountAccepted($array);
		} elseif ($array['description'] == 'reset password') {
			$row = Email::resetPassword($array);
		} elseif ($array['description'] == 'password updated') {
			$row = Email::passwordUpdated($array);
		} elseif ($array['description'] == 'account updated') {
			$row = Email::accountUpdated($array);
		} elseif ($array['description'] == 'account created') {
			$row = Email::accountCreated($array);
		} else {
			$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("Type de message inconnu", "DEBUG", "%s", "unknown message", "%s")'
				, __METHOD__
				, $_SESSION['db']->db_real_escape_string(json_encode($array)))
			);
			return FALSE;
		}
		$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("Tentative d\'envoi d\'un mail.", "DEBUG", "%s", "sending mail", "%s")'
			, __METHOD__
			, $_SESSION['db']->db_real_escape_string(json_encode($row)))
		);
		if (array_key_exists('to', $row) && filter_var($row['to'], FILTER_VALIDATE_EMAIL) && array_key_exists('subject', $row) && array_key_exists('content', $row)) {
			return Email::QuickMail($row['to'], $row['subject'], $row['content']);
		} else {
			$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("Le message n\'a pas été envoyé car il manquait des valeurs dans le tableau array", "DEBUG", "%s", "short", "%s")'
				, __METHOD__
				, $_SESSION['db']->db_real_escape_string(json_encode($row)))
			);
			return FALSE;
		}	
	}
	/**
	 * account accepted : la demande de création de compte par un utilisateur a été acceptée.
	 *
	 * Ce message est envoyé à un utilisateur qui a demandé à créer un compte sur TeamTime
	 * via l'interface (bouton s'enregistrer), lorsque sa demande a été acceptée par un editeurs.
	 * Ce message demande à l'utilisateur de créer son login et son mot de passe de connexion
	 * pour TeamTime en lui fournissant un lien.
	 *
	 * @param array $array Le paramètre $array comprend :
	 * - id			=> l'identifiant (doit être int) référent l'utilisateur dans TBL_SIGNUP_ON_HOLD
	 *
	 * @return array 
	 * - to			=> destinataire du mail
	 * - subject		=> sujet du mail
	 * - content		=> texte du mail
	 *
	 */
	private static function accountAccepted($array) {
		if (!array_key_exists('id', $array) && is_int($array['id'])) {
			ob_start();
			var_dump($array);
			$array['dump'] = ob_get_contents();
			ob_end_clean();
			$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("Le message n\'a pas été envoyé car il manquait des valeurs dans le tableau array", "DEBUG", "%s", "short", "%s")'
				, __METHOD__
				, $_SESSION['db']->db_real_escape_string(json_encode($array)))
			);
		}
		$id = $array['id'];
		$sql = "SELECT `email`, `url` AS `k`, `nom`, `prenom`, `titre` AS `subject`, `texte`
			FROM `TBL_SIGNUP_ON_HOLD`,
			`TBL_ARTICLES`
			WHERE `id` = $id
			AND `description` = 'account accepted'";
		$row = $_SESSION['db']->db_fetch_assoc($_SESSION['db']->db_interroge($sql));
		return array(
			'to'		=> $row['email']
			, 'subject'	=> $row['subject']
			, 'content'	=> sprintf($row['texte']
			, ucfirst($row['prenom'])
			, $GLOBALS['site']['URL']
			, $row['k']
			)
		);
	}
	/**
	 * reset password : un utilisateur a demandé à saisir un nouveau mot de passe.
	 *
	 * Ce message indique un lien à l'utilisateur pour qu'il puisse saisir un nouveau
	 * mot de passe de connexion à TeamTime.
	 *
	 * @param array $array Le paramètre $array comprend :
	 * - k			=> la clé passée en paramètre de l'url
	 * - email		=> le mail du destinataire
	 *
	 * @return array 
	 * - to			=> destinataire du mail
	 * - subject		=> sujet du mail
	 * - content		=> texte du mail
	 *
	 */
	private static function resetPassword($array) {
		if (!array_key_exists('k', $array) || !array_key_exists('email', $array)) {
			$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("Le message n\'a pas été envoyé car il manquait des valeurs dans le tableau array", "DEBUG", "%s", "short", "%s")'
				, __METHOD__
				, $_SESSION['db']->db_real_escape_string(json_encode($array)))
			);
		}
		$sql = sprintf("SELECT `email`, `prenom`, `titre` AS `subject`, `texte`
			FROM `TBL_USERS`,
			`TBL_ARTICLES`
			WHERE `email` = '%s'
			AND `description` = 'reset password'
			", $_SESSION['db']->db_real_escape_string(filter_var(trim($array['email']), FILTER_SANITIZE_EMAIL))
		);
		$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("Requête de construction du mail", "DEBUG", "%s", "short", "%s")'
			, __METHOD__
			, $_SESSION['db']->db_real_escape_string($sql))
		);
		$row = $_SESSION['db']->db_fetch_assoc($_SESSION['db']->db_interroge($sql));
		return array(
			'to'		=> $row['email']
			, 'subject'	=> $row['subject']
			, 'content'	=> sprintf($row['texte']
			, ucfirst($row['prenom'])
			, $GLOBALS['site']['URL']
			, $array['k']
			, $GLOBALS['site']['URL']
			, $array['k']
			)
		);
	}
	/**
	 * password updated : Ce message confirme que l'utilisateur a mis à jour son mot de passe.
	 *
	 * @param array $array Le paramètre $array comprend :
	 * - to			=> l'adresse mail de l'utilisateur dans TBL_USERS
	 * l'adresse mail doit avoir été filtrée, car la présente méthode ne la filtre pas
	 *
	 * @return array 
	 * - to			=> destinataire du mail
	 * - subject		=> sujet du mail
	 * - content		=> texte du mail
	 *
	 */
	private static function passwordUpdated($array) {
		$sql = sprintf("
			SELECT `prenom`,
			`login`,
			`titre` AS `subject`,
			`texte`
			FROM `TBL_USERS`,
			`TBL_ARTICLES`
			WHERE `email` = '%s'
			AND `description` = 'password updated'
			", $array['to']
		);
		$row = $_SESSION['db']->db_fetch_assoc($_SESSION['db']->db_interroge($sql));
		return array(
			'to'		=> $array['to']
			, 'subject'	=> $row['subject']
			, 'content'	=> sprintf($row['texte']
					, ucfirst($row['prenom'])
					, $row['login']
			)
		);
	}
	/**
	 * account updated
	 *
	 * Ce message devrait être évité dans la mesure où
	 * il contient le login et le mot de passe.
	 *
	 * @param array $array Le paramètre $array comprend :
	 * - uid		=> l'identifiant (doit être int) référent l'utilisateur dans TBL_USERS
	 *
	 * @return array 
	 * - to			=> destinataire du mail
	 * - subject		=> sujet du mail
	 * - content		=> texte du mail
	 *
	 */
	private static function accountUpdated($array) {
		if (!array_key_exists('uid', $array) && is_int($array['uid'])) {
			$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("Le message n\'a pas été envoyé car il manquait des valeurs dans le tableau array", "DEBUG", "%s", "short", "%s")'
				, __METHOD__
				, $_SESSION['db']->db_real_escape_string(json_encode($array)))
			);
		}
		$uid = $array['uid'];
		$sql = "SELECT `email`, `prenom`, `login`, `titre` AS `subject`, `texte`
			FROM `TBL_USERS`,
			`TBL_ARTICLES`
			WHERE `uid` = $uid
			AND `description` = 'account updated'";
		$row = $_SESSION['db']->db_fetch_assoc($_SESSION['db']->db_interroge($sql));
		return array(
			'to'		=> $row['email']
			, 'subject'	=> $row['subject']
			, 'content'	=> sprintf($row['texte']
					, ucfirst($row['prenom'])
					, $GLOBALS['site']['URL']
					, $array['password']
					)
		);
	}
	/**
	 * account created : Ce message est envoyé à un utilisateur lorsqu'un editeur lui a créé un compte.
	 *
	 *
	 * @param array $array Le paramètre $array comprend :
	 * - email		=> l'email de l'utilisateur (présent dans TBL_SIGNUP_ON_HOLD)
	 *
	 * @return array 
	 * - to			=> destinataire du mail
	 * - subject		=> sujet du mail
	 * - content		=> texte du mail
	 *
	 */
	private static function accountCreated($array) {
		if (!array_key_exists('email', $array)) {
			$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("Le message n\'a pas été envoyé car email absent dans le tableau array", "DEBUG", "%s", "short", "%s")'
				, __METHOD__
				, $_SESSION['db']->db_real_escape_string(json_encode($array)))
			);
		}
		$sql = sprintf("SELECT `prenom`, `url`, `titre` AS `subject`, `texte`
			FROM `TBL_SIGNUP_ON_HOLD`,
			`TBL_ARTICLES`
			WHERE `email` = '%s'
			AND `description` = 'account created'
			", $_SESSION['db']->db_real_escape_string($array['email'])
		);
		$row = $_SESSION['db']->db_fetch_assoc($_SESSION['db']->db_interroge($sql));
		return array(
			'to'		=> $array['email']
			, 'subject'	=> $row['subject']
			, 'content'	=> sprintf($row['texte']
					, ucfirst($row['prenom'])
					, $GLOBALS['site']['URL']
					, $row['url']
					, $GLOBALS['site']['URL']
					)
		);
	}
// Constructeur
	public function __construct() {
	}
	public function __destruct() {
		return true;
	}
}
?>
