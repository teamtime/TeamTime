<?php
// class_groupe.inc.php

class groupe {
// Déclaration des variables
	private $groupe;
	private $description;
	private $canwrite = 0;
	private $actif = 0;

	public function __construct() {}
	public function __destruct() {}
// Attribution/lecture des valeurs de l'objet
	public function groupe($param = NULL) {
		if ($param == NULL) {
			$this->groupe = $param;
		}
		return $this->groupe;
	}
	public function description($param = NULL) {
		if ($param == NULL) {
			$this->description = $param;
		}
		return $this->description;
	}
	public function setCanwrite() {
		$this->canwrite = 1;
	}
	public function unsetCanwrite() {
		$this->canwrite = 0;
	}
	public function setactif() {
		$this->actif = 1;
	}
	public function unsetactif() {
		$this->actif = 0;
	}
// Fonctions de bdd
	private function _db_insertGroupe() {
		// Ajoute un groupe à la base
		$insert = sprintf ("INSERT INTO `TBL_GROUPS` (groupe, description, canwrite, actif) VALUES ('%s', '%s', '%s', '%s')", $this->groupe, $this->description, $this->canwrite, $this->actif);
		interroge_db($insert);
	}
	private function _db_updateGroupe() {
		// Met à jour la description pour le groupe dans la base
		$update = sprintf ("UPDATE `TBL_GROUPS` SET description = '%s', canwrite = '%s', actif = '%s' WHERE groupe = '%s'", $this->description, $this->canwrite, $this->actif, $this->groupe);
		interroge_db($insert);
	}
	public function _db_groupeExists() {
		// Retourne TRUE si $groupe existe dans la base
		$select = sprintf ("SELECT * FROM `TBL_GROUPS` WHERE groupe = '%s'", $this->groupe);
		if (mysqli_num_rows(interroge_db($select)) > 0) { return TRUE; }
		else { return FALSE; }
	}
	public function _db_s_insertGroupe() {
		// Ajoute un groupe à la base ou le met à jour
		if ($this->db_groupeExists()) {
			$this->db_updateGroupe();
		} else {
			$this->db_insertGroupe();
		}
	}
}
?>
