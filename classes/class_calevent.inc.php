<?php
// class_calEvent.inc.php
//
// Une classe pour gérer les événements calendrier
//
// Celle-ci concerne notamment les vacances scolaires, les briefings, les périodes de charge
//
class calEvent {
// Propriétés
	public static $titres = array(
		'vsid'		=> 'vacances scolaires'
		, 'pcid'	=> 'période de charge'
		, 'bfid'	=> 'briefing'
		, 'msid'	=> 'message'
	);
	/**
	 * L'id de l'événement dans la base de données.
	 */
	private $id;
	/**
	 * L'uid de l'annonceur de l'événement.
	 */
	private $uid;
	/**
	 * La date de début de l'événement.
	 */
	private $dateD;
	/**
	 * La date de fin de l'événement.
	 */
	private $dateF;
	/**
	 * La description de l'événement.
	 */
	private $description;
	/**
	 * Le type d'évènement qui détermine le nom de 
	 * l'index.
	 * 
	 * - bfid => briefings
	 * - vsid => vacances scolaires
	 * - pcid => période de charge
	 */
	private $type;
	/**
	 * Les centres concernés par l'événement.
	 */
	private $centres = array();
	/**
	 * Les équipes concernées par l'événement.
	 */
	private $teams = array();
	/**
	 * Les uids concernés par l'évènement.
	 */
	private $uids = array();
// Méthodes statiques
	/**
	 * Méthode listant les différents types de calEvent possibles
	 *
	 * @param
	 *
	 * @return array les différents types de calEvent possibles
	 */
	private static function liste_types() {
		$fields = $_SESSION['db']->db_getColumnsTable('TBL_CAL_EVENTS');
		$string = $fields['type']['Type'];
		$string = preg_replace('/set\((.*)\)/', "$1", $string);
		$string = preg_replace('/\'/', "", $string);
		return explode(',', $string);
	}
	/**
	 * Méthode statique pour obtenir la description d'un évènement à partir de son id
	 *
	 * @param int id de l'évènement
	 *
	 * @return string description de l'évènement
	 */
	public static function desc($id = NULL) {
		if (is_null($id)) return FALSE;
		$sql = sprintf("
			SELECT `description` FROM `TBL_CAL_EVENTS`
			WHERE `id` = %d
			"
			, $id
		);
		$r = $_SESSION['db']->db_fetch_row($_SESSION['db']->db_interroge($sql));
		return $r[0];
	}
	/**
	 * Méthode supprimant un calEvent à partir de son id
	 *
	 * @param int id
	 *
	 * @return boolean False on error
	 */
	public static function deleteEvent($id = NULL) {
		if (is_int($id) || ctype_digit($id)) {
			$calEvent = new calEvent($id);
			$calEvent->__deleteEvent();
		} else {
			return FALSE;
		}
	}
	/**
	 * Méthode pour obtenir la liste des calEvent postérieurs à la date courante
	 *
	 * @param $array array un tableau contenant
	 *                     - la date à partir de laquelle les évènements sont retournés (array['date']) /!\ Doit être un objet Date
	 *                     - le centre recherché (array['centre'])
	 *                     - l'équipe recherchée (array['team'])
	 *                     - le type recherché (array['type'])
	 *
	 * @return array
	 */
	public static function list_post_calEvent($array = NULL) {
		if (!array_key_exists('date', $array) || !is_a($array['date'], 'Date')) {
			$date = '`dateF` >= NOW()';
		} else {
			$date = sprintf("
				`dateF` >= '%s'
				"
				, $array['date']->date()
			);
		}
		// Recherche du centre
		$centre = '';
		if (isset($array['centre']) && $array['centre'] != 'all') {
			$centre = sprintf("
				AND (`centres` LIKE '%%%s%%' OR `centres` = 'all')
				"
				, $_SESSION['db']->db_real_escape_string($array['centre'])
			);
		}
		// Recherche de l'équipe
		$team = '';
		if (isset($array['team']) && $array['team'] != 'all') {
			$team = sprintf("
				AND (`teams` LIKE '%%%s%%' OR `teams` = 'all')
				"
				, $_SESSION['db']->db_real_escape_string($array['team'])
			);
		}
		// Recherche par type
		$type = '';
		if (isset($array['type'])) {
			$type = sprintf("
				AND `type` = '%s'
				"
				, $_SESSION['db']->db_real_escape_string($array['type'])
			);
		}
		$sql = "SELECT *
			FROM `TBL_CAL_EVENTS`
			WHERE $date
			$centre
			$team
			$type
			ORDER BY `type`, `dateF`";
		$result = $_SESSION['db']->db_interroge($sql);
		while($row = $_SESSION['db']->db_fetch_assoc($result)) {
			 $datas[$row['type']][] = new calEvent($row);
		}
		mysqli_free_result($result);
		return $datas;
	}
	/**
	 * Valide le type de calEvent
	 *
	 * @param string le type à tester
	 *
	 * @return boolean true si $type est valide
	 *                 false sinon
	 */
	public static function typeIsValid($type = NULL) {
		return (!is_null($type) && in_array($type, self::liste_types()));
	}
// Méthodes privées
	/**
	 * Attribution ou lecture de l'id.
	 *
	 * @param $id int
	 *
	 * @return int l'id
	 */
	private function __id($id = NULL) {
		if (is_int($id) || ctype_digit($id)) {
			$this->id = (int) $id;
		}
		return $this->id;
	}
	/**
	 * Attribution ou lecture de l'uid du rédacteur.
	 *
	 * @param $uid int
	 *
	 * @return int l'uid
	 */
	private function __uid($uid = NULL) {
		if (is_int($uid) || ctype_digit($uid)) {
			$this->uid = (int) $uid;
		}
		return $this->uid;
	}
	/**
	 * attribution ou lecture de la date de début de l'événement.
	 *
	 * @param $dateD string La date de début au format chaîne de caractères ou objet Date
	 *
	 * @return object Date
	 */
	private function __dateD($dateD = NULL) {
		if (!is_null($dateD)) {
			if (is_string($dateD)) {
				$dateD = new Date($dateD);
			}
			if (is_a($dateD, 'Date')) {
				$this->dateD = $dateD;
			}
		}
		return $this->dateD;
	}
	/**
	 * attribution ou lecture de la date de fin de l'événement.
	 *
	 * @param $dateD string La date de fin au format chaîne de caractères ou objet Date
	 *
	 * @return object Date
	 */
	private function __dateF($dateF = NULL) {
		if (!is_null($dateF)) {
			if (is_string($dateF)) {
				$dateF = new Date($dateF);
			}
			if (is_a($dateF, 'Date')) {
				$this->dateF = $dateF;
			}
		}
		return $this->dateF;
	}
	/**
	 * Attribution ou lecture de la description de l'événement.
	 *
	 * @param $description string la description de l'événement.
	 *
	 * @return string la description
	 */
	private function __description($description = NULL) {
		if (!is_null($description)) {
			$this->description = $description;
		}
		return $this->description;
	}
	/**
	 * Attribution ou lecture du type de l'évènement
	 *
	 * @param string type d'évènement
	 *
	 * @return string type d'évènement
	 */
	private function __type($type = NULL) {
		if (self::typeIsValid($type)) {
			$this->type = $type;
		}
		return $this->type;
	}
	/**
	 * Attribution ou lecture des centres.
	 *
	 * @param $centre string Le(s) centre(s) à ajouter sous forme csv
	 *
	 * @return array Les centres
	 */
	private function __centres($centre = NULL) {
		if (!is_null($centre)) {
			if (!$_SESSION['utilisateur']->isAdmin()) {
				$centre = $_SESSION['utilisateur']->centre();
			}
			$this->centres = array_fill_keys(explode(',', $centre), 1);
		}
		return array_keys($this->centres);
	}
	/**
	 * Supprime un centre de l'évènement
	 *
	 * @param string le centre à supprimer
	 *               si vide, tous les centres sont supprimés
	 *
	 * @return boolean
	 */
	private function __remove_centre($centre = NULL) {
		if (is_null($centre)) {
			$this->centres = array();
			// Supprime les entrées dans TBL_GRILLE
			$sql = sprintf("
				UPDATE `TBL_GRILLE`
				SET 
				");
		} else {
			$array = explode(',', $centre);
			foreach ($array as $c) {
				if (array_key_exists($c, $this->centres)) {
					unset($this->centres[$c]);
				}
			}
		}
	}
	/**
	 * Attribution ou lecture des équipes.
	 *
	 * @param string $team les équipes à ajouter au format csv.
	 *
	 * @return array les équipes.
	 */
	private function __teams($team = NULL) {
		if (!is_null($team)) {
			if (!$_SESSION['utilisateur']->hasRole('teamEdit')) {
				$team = $_SESSION['utilisateur']->team();
			}
			$this->teams = array_fill_keys(explode(',', $team), 1);
		}
		return array_keys($this->teams);
	}
	/**
	 * Supprime une équipe de l'évènement
	 *
	 * @param string l'équipe à supprimer
	 *               si vide, toutes les équipes sont supprimées
	 *
	 * @return boolean
	 */
	private function __remove_team($team = NULL) {
		if (is_null($team)) {
			$this->teams = array();
		} else {
			$array = explode(',', $team);
			foreach ($array as $c) {
				if (array_key_exists($c, $this->teams)) {
					unset($this->teams[$c]);
				}
			}
		}
	}
	/**
	 * Attribution ou lecture des uids.
	 *
	 * @param string $team les uids à ajouter au format csv.
	 *
	 * @return array les équipes.
	 */
	private function __uids($uid = NULL) {
		if (!is_null($uid)) {
			$this->uids = array_fill_keys(explode(',', $uid), 1);
		}
		return array_keys($this->uids);
	}
	/**
	 * Supprime un uid de l'évènement
	 *
	 * @param string le uid à supprimer
	 *               si vide, tous les uids sont supprimées
	 *
	 * @return boolean
	 */
	private function __remove_uid($uid = NULL) {
		if (is_null($uid)) {
			$this->uids = array();
		} else {
			$array = explode(',', $uid);
			foreach ($array as $c) {
				if (array_key_exists($c, $this->uids)) {
					unset($this->uids[$c]);
				}
			}
		}
	}
	/**
	 * Définition d'un élément de condition SQL pour les centres
	 *
	 * @param void
	 *
	 * @return string un élément de condition SQL
	 */
	public function centres_as_sql_cond() {
		if (count($this->__centres()) > 0) {
			if ($this->__centres() == 'all') {
				return '';
			}
			$condition = " AND (";
			foreach ($this->__centres() as $centre) {
				$condition .= sprintf("`centre` = '%s' OR ", $_SESSION['db']->db_real_escape_string($centre));
			}
			return $condition . "`centre` = 'all')";
		} else {
			return "";
		}
	}
	/**
	 * Les centres sous forme de liste séparée par des virgules
	 *
	 * @param void
	 *
	 * @return string liste des centres
	 */
	public function centres_as_csv() {
		if (count($this->__centres()) > 0) {
			$liste = '';
			foreach ($this->__centres() as $centre) {
				$liste .= $_SESSION['db']->db_real_escape_string($centre) . ",";
			}
			return substr($liste, 0, -1);
		} else {
			return "";
		}
	}
	/**
	 * Définition d'un élément de condition SQL pour les équipes
	 *
	 * @param void
	 *
	 * @return string un élément de condition SQL
	 */
	public function teams_as_sql_cond() {
		if (count($this->__teams()) > 0) {
			if ($this->__teams() == 'all') {
				return '';
			}
			$condition = " AND (";
			foreach ($this->__teams() as $team) {
				$condition .= sprintf("`team` = '%s' OR ", $_SESSION['db']->db_real_escape_string($team));
			}
			return $condition . "`team` = 'all')";
		} else {
			return "";
		}
	}
	/**
	 * Les équipes sous forme de liste séparée par des virgules
	 *
	 * @param void
	 *
	 * @return string liste des équipes
	 */
	public function teams_as_csv() {
		if (count($this->__teams()) > 0) {
			$liste = '';
			foreach ($this->__teams() as $team) {
				$liste .= $_SESSION['db']->db_real_escape_string($team) . ",";
			}
			return substr($liste, 0, -1);
		} else {
			return "";
		}
	}
	/**
	 * Définition d'un élément de condition SQL pour les uids
	 *
	 * @param void
	 *
	 * @return string un élément de condition SQL
	 */
	public function uids_as_sql_cond() {
		if (count($this->__uids()) > 0) {
			if ($this->__uids() == 'all') {
				return '';
			}
			$condition = " AND (";
			foreach ($this->__uids() as $uid) {
				$condition .= sprintf("`uid` = '%s' OR ", $_SESSION['db']->db_real_escape_string($uid));
			}
			return $condition . "`uid` = 'all')";
		} else {
			return "";
		}
	}
	/**
	 * Les uids sous forme de liste séparée par des virgules
	 *
	 * @param void
	 *
	 * @return string liste des équipes
	 */
	public function uids_as_csv() {
		if (count($this->__uids()) > 0) {
			$liste = '';
			foreach ($this->__uids() as $uid) {
				$liste .= $_SESSION['db']->db_real_escape_string($uid) . ",";
			}
			return substr($liste, 0, -1);
		} else {
			return "";
		}
	}
	/**
	 * Insertion ou mise à jour de l'événement dans la base de données.
	 *
	 * @param void
	 */
	private function __updateEvent() {
		// Mise à jour de la tables des évènements de calendrier
		$sql = sprintf("
			REPLACE  INTO `TBL_CAL_EVENTS`
			(`id`, `uid`, `dateD`, `dateF`, `description`, `type`, `centres`, `teams`, `uids`)
			VALUES
			(%d, %d, '%s', '%s', '%s', '%s', '%s', '%s', '%s')
			"
			, $this->__id()
			, $this->__uid()
			, $this->__dateD()->date()
			, $this->__dateF()->date()
			, $_SESSION['db']->db_real_escape_string($this->__description())
			, $this->__type()
			, $this->centres_as_csv()
			, $this->teams_as_csv()
			, $this->uids_as_csv()
		);
		$_SESSION['db']->db_interroge($sql);
		// Mise à jour de TBL_GRILLE
		$this->updateGrille();
	}
	/**
	 * Suppression de l'événement dans la base de données.
	 *
	 * @param void
	 */
	private function __deleteEvent() {
		$sql = sprintf("
			DELETE FROM `TBL_CAL_EVENTS`
			WHERE `id` = %d
			"
			, $this->__id()
		);
		$_SESSION['db']->db_interroge($sql);
		$sql = sprintf("
			UPDATE `TBL_GRILLE`
			SET `%s` = NULL
			WHERE `%s` = %d
			"
			, $this->__type()
			, $this->__type()
			, $this->__id()
		);
		$_SESSION['db']->db_interroge($sql);
	}
	/**
	 * Construction de l'objet à partir d'une entrée de la bdd
	 *
	 * @param int index de l'évènement dans la bdd
	 *
	 * @return TRUE on success
	 */
	private function __load_from_id($id) {
		if ($id != (int) $id) {
			return FALSE;
		}
		$sql = sprintf("
			SELECT * FROM `TBL_CAL_EVENTS`
			WHERE `id` = %d
			"
			, $id
		);
		$row = $_SESSION['db']->db_fetch_assoc($_SESSION['db']->db_interroge($sql));
		$this->__id($id);
		$this->__uid($row['uid']);
		$this->__dateD($row['dateD']);
		$this->__dateF($row['dateF']);
		$this->__description($row['description']);
		$this->__type($row['type']);
		$this->__centres($row['centres']);
		$this->__teams($row['teams']);
		$this->__uids($row['uids']);
		return TRUE;
	}
// Méthodes publiques
	/**
	 * Constructeurs
	 *
	 * @param mixed un tableau associatif contenant les données de l'évènement
	 *              ou un entier/chaîne contenant l'id de l'évènement
	 *              si seul l'id est passé, l'objet est chargé à partir de la bdd
	 *              sinon l'évènement est chargé ou créé
	 *
	 * @return TRUE on success
	 *         FALSE on error
	 */
	public function __construct($row = NULL) {
		if (is_null($row)) {
			return FALSE;
		}
		if (is_int($row) || ctype_digit($row)) {
			$this->__load_from_id($row);
		} else {
			if (!isset($row['centres'])) {
				$row['centres'] = 'all';
			}
			if (!isset($row['teams'])) {
				$row['teams'] = 'all';
			}
			if (!isset($row['uids'])) {
				$row['uids'] = 'all';
			}
			$this->__uid($_SESSION['utilisateur']->uid());
			$this->__dateD($row['dateD']);
			$this->__dateF($row['dateF']);
			$this->__description($row['description']);
			$this->__type($row['type']);
			$this->__centres($row['centres']);
			$this->__teams($row['teams']);
			$this->__uids($row['uids']);
			if (isset($row['id']) && (is_int($row['id']) || ctype_digit($row['id']))) {
				$this->__id($row['id']);
			}
			$this->__updateEvent();
		}
	}
	/**
	 * Affichage de
	 *
	 * @param
	 *
	 * @return string
	 */
	public function id() {
		return $this->id;
	}
	/**
	 * Affichage de
	 *
	 * @param
	 *
	 * @return string
	 */
	public function uid() {
		return $this->uid;
	}
	/**
	 * Affichage de
	 *
	 * @param
	 *
	 * @return string
	 */
	public function dateD() {
		return $this->dateD->formatDate();
	}
	/**
	 * Affichage de
	 *
	 * @param
	 *
	 * @return string
	 */
	public function dateF() {
		return $this->dateF->formatDate();
	}
	/**
	 * Affichage de
	 *
	 * @param
	 *
	 * @return string
	 */
	public function description() {
		return $this->description;
	}
	/**
	 * Affichage de
	 *
	 * @param
	 *
	 * @return string
	 */
	public function type() {
		return $this->type;
	}
	/**
	 * Affichage de
	 *
	 * @param
	 *
	 * @return string
	 */
	public function centres() {
		return $this->centres;
	}
	/**
	 * Affichage de
	 *
	 * @param
	 *
	 * @return string
	 */
	public function teams() {
		return $this->teams;
	}
	/**
	 * Affichage de
	 *
	 * @param
	 *
	 * @return string
	 */
	public function uids() {
		return $this->uids;
	}
	/**
	 * Ajout des infos dans TBL_GRILLE
	 *
	 * @param
	 *
	 * @return void
	 */
	public function updateGrille() {
		if ($this->__type() != 'msid') {
			// Suppression de l'ancien événement de TBL_GRILLE
			$sql = sprintf("
				UPDATE `TBL_GRILLE`
				SET `%s` = NULL
				WHERE `%s` = %d
				"
				, $this->__type()
				, $this->__type()
				, $this->__id()
			);
			$_SESSION['db']->db_interroge($sql);
			$sql = sprintf("
				UPDATE `TBL_GRILLE`
				SET `%s` = %d
				WHERE `date` BETWEEN '%s' AND '%s'
				%s
				"
				, $this->__type()
				, $this->__id()
				, $this->dateD->date()
				, $this->dateF->date()
				, $this->centres_as_sql_cond()
			);
			$_SESSION['db']->db_interroge($sql);
		}
	}
}
?>
