<?php
// class_elemmenu.inc.php

class elemMenu {
	const maxProf = 3; // Profondeur maximum de sous-menus (pour éviter une récursivité infinie)
	private $idx;
	private $titre;
	private $description;
	private $lien;
	private $sousmenu = NULL; // index du sous-menu éventuel
	private $submenu; // L'objet sous-menu éventuel
	private $creation;
	private $modification;
	private $allowed; // Chaîne contenant les groupes/utilisateurs autorisés à voir cet élément
	private $actif;
	private $position;
	private $profondeur = 1; // Profondeur actuelle du présent menu (pour éviter une récursivité infinie)
// Constructeur
	function __construct($param = NULL) { // $idx en param
		if (! is_null($param)) {
			$this->idx($param);
			if (is_null($this->db_setElem())) {
				$this->__destruct();
				return NULL;
			}
	       	}
	}
	function __destruct() {
		unset($this);
	}
// Accesseurs et attribution des valeurs
	public function idx($param = NULL) {
		if (! is_null($param)) { $this->idx = (int) $param; }
		return $this->idx;
	}
	public function titre($param = NULL) {
		if (! is_null($param)) { $this->titre = htmlspecialchars($param, ENT_NOQUOTES, $_SESSION['db']->encoding()); }
		return $this->titre;
	}
	public function description($param = NULL) {
		if (! is_null($param)) { $this->description = $param; }
		return $this->description;
	}
	public function lien($param = NULL) {
		if (! is_null($param)) { $this->lien = $param; }
			return $this->lien;
	}
	public function sousmenu($param = NULL) {
		if (! is_null($param)) {
			$this->sousmenu = (int) $param;
			$this->_build_sousmenu();
	       	}
		return $this->sousmenu;
	}
	private function _build_sousmenu() { // FIXME ATTENTION aux problèmes de récursivité dans les sous-menus...
		if (! is_object($this->submenu) && ! is_null($this->sousmenu) && $this->profondeur < self::maxProf) {
			$this->submenu = new menu($this->sousmenu);
			$this->submenu->profondeur($this->profondeur);
		}
	}
	public function submenu() {
		if (is_object($this->submenu)) {
			return $this->submenu;
		} else {
			return NULL;
		}
	}
	public function creation($param = NULL) {
		if (! is_null($param)) { $this->creation = new Date($param); }
			return $this->creation;
	}
	public function modification($param = NULL) {
		if (! is_null($param)) { $this->modification = new Date($param); }
			return $this->modification;
	}
	public function position($param = NULL) {
		if (! is_null($param)) { $this->position = $param; }
			return $this->position;
	}
	public function actif($param = NULL) {
		if (! is_null($param)) {
			if ($param) {
				$this->setActive();
			} else {
				$this->unsetActive();
			}
		}
		return $this->actif;
	}
	public function setActive() {
		$this->actif = true;
	}
	public function unsetActive() {
		$this->actif = false;
	}
	public function allowed($param = NULL) {
		if (! is_null($param)) { $this->allowed = $param; }
		return $this->allowed;
	}
	public function profondeur($param = NULL) {
		if (! is_null($param)) { $this->profondeur = $param; }
		return $this->profondeur;
	}
// Actions sur l'objet
	private function _setFromElemMenu($param) {
		if (!is_array($param)) return NULL;
		$this->titre($param['titre']);
		$this->description($param['description']);
		$this->lien($param['lien']);
		$this->sousmenu($param['sousmenu']);
		$this->creation($param['creation']);
		$this->modification($param['modification']);
		$this->allowed($param['allowed']);
		$this->actif($param['actif']);
		return true;
	}
// méthodes de bdd
	public function db_setElem() {
		$find_in_set = "";
		foreach (array_flip(array_flip(array_merge(array('all'), $_SESSION['utilisateur']->roles()))) as $set) {
			$find_in_set .= sprintf("FIND_IN_SET('%s', `allowed`) OR ", $_SESSION['db']->db_real_escape_string($set));
		}
		$requete = sprintf("
			SELECT * FROM `TBL_ELEMS_MENUS`
			WHERE `idx` = %d
			AND `actif` = TRUE
			AND (%s)"
			, $this->idx
			, substr($find_in_set, 0, -4)
		);
		if (!empty($TRACE) && true === $TRACE) {
			$_SESSION['db']->db_interroge(sprintf('CALL messageSystem("%s", "DEBUG", "db_setElem", NULL, NULL)'
				, $requete)
			);
		}
		$this->_setFromElemMenu($_SESSION['db']->db_fetch_assoc($_SESSION['db']->db_interroge($requete)));
	}
	private function _db_insertDB() {
		$requete = sprintf("
			INSERT INTO `TBL_ELEMS_MENUS`
			(`idx`, `titre`, `description`, `lien`, `sousmenu`, `creation`, `allowed`, `actif`)
			VALUES (NULL, '%s', '%s', '%s', %d, NOW(), '%s', %d)"
			, $this->titre
			, $this->description
			, $this->lien
			, $this->sousmenu
			, $this->allowed
			, $this->actif
		);
	}
	private function _db_updateDB() {
		$requete = sprintf("
			UPDATE `TBL_ELEMS_MENUS`
			SET `titre` = '%s'
			, `description` = '%s'
			, `lien` = '%s'
			, `sousmenu` = %d
			, `modification` = NOW()
			, `allowed` = '%s'
			, `actif` = %d
			WHERE `idx` = %d"
			, $this->titre
			, $this->description
			, $this->lien
			, $this->sousmenu
			, $this->allowed
			, $this->actif
			, $this->idx
		);
	}
// Display
	public function debug_display() {
		printf ("%s (%s) [%s] @ %s => %s<br />", $this->titre, $this->description, $this->idx, $this->position, $this->lien);
		if (!is_null($this->sousmenu)) {
			$this->submenu->debug_display();
		}
		print "_______________<br />";
	}
}
?>
