<?php
// class_adresse.inc.php
//
// Classe pour gérer les contacts des utilisateurs
//

/*
	TeamTime is a software to manage people working in team on a cyclic shift.
	Copyright (C) 2012 Manioul - webmaster@teamtime.me

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


class Adresse {
	private $adresseid;
	private $uid;
	private $adresse;
	private $cp;
	private $ville;
	private $table = 'TBL_ADRESSES'; // La table qui gère les affectations
// Consctucteur
	public function __construct($param = NULL) {
		if (is_null($param)) return true;
		if (is_int($param)) {
			$this->setFromDb($param);
		}
		if (is_array($param)) {
			$this->setFromRow($param);
		}
	}
	public function __destruct() {
		return true;
	}
// Accesseurs
	public function adresseid($string = NULL) {
		if (!is_null($string)) {
			$this->adresseid = (int) $string;
		}
		return $this->adresseid;
	}
	public function uid($string = NULL) {
		if (!is_null($string)) {
			$this->uid = (int) $string;
		}
		return $this->uid;
	}
	public function adresse($string = NULL) {
		if (!is_null($string)) {
			$this->adresse = $string;
		}
		return $this->adresse;
	}
	public function cp($string = NULL) {
		if (!is_null($string)) {
			$this->cp = $string;
		}
		return $this->cp;
	}
	public function ville($string = NULL) {
		if (!is_null($string)) {
			$this->ville = $string;
		}
		return $this->ville;
	}
	public function setFromRow($row) {
		foreach ($row as $key => $value) {
			if (method_exists($this, $key)) {
				$this->$key($value);
			} else {
				$this->key = $value;
			}
		}
	}
	/*
	 * Alias de getFromAdresseid
	 */
	public function setFromDb($param) {
		return $this->getFromAdresseid($param);
	}
	public function asArray() {
		return array(
			'adresseid'	=> $this->adresseid()
			,'uid'		=> $this->uid()
			,'adresse'	=> $this->adresse()
			,'cp'		=> $this->cp()
			,'ville'	=> $this->ville()
		);
	}
// Méthodes de la bdd
	public function getFromAdresseid($adresseid) {
		$sql = sprintf("SELECT *
			FROM `%s`
			WHERE `adresseid` = %d"
			, $this->table
			, $adresseid
		);
		$row = $_SESSION['db']->db_fetch_assoc($_SESSION['db']->db_interroge($sql));
		$this->adresseid($row['adresseid']);
		$this->uid($row['uid']);
		$this->adresse($row['adresse']);
		$this->cp($row['cp']);
		$this->ville($row['ville']);
		return true;
	}
	public function update() {
		return $_SESSION['db']->db_update($this->table, $this->asArray());
	}
	public function insert() {
		// Si un adresseid existe déjà, il s'agit d'une mise à jour et non d'une insertion
		if (!empty($this->adresseid)) {
			$this->update();
			return $this->adresseid;
		} else {
			firePhpLog($this, 'Insertion de adresse dans la base');
			$this->adresseid = NULL;
			return $this->adresseid($_SESSION['db']->db_insert($this->table, $this->asArray()));
		}
	}
	public function delete() {
		$sql = sprintf("
			DELETE FROM `%s`
			WHERE `adresseid` = %d"
			, $this->table
			, $this->adresseid()
		);
		$_SESSION['db']->db_interroge($sql);
		$this->__destruct();
		unset($this);
	}
}
?>
