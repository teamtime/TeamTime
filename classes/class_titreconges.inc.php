<?php
// class_titreCong.inc.php
//
// Classe permettant d'éditer les titres de congés des agents
//
/*
	TeamTime is a software to manage people working in team on a cyclic shift.
	Copyright (C) 2012 Manioul - webmaster@teamtime.me

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once ('tfpdf/tfpdf.php');
class FPDF extends tFPDF {
	protected $_tplIdx;
}
require_once ('FPDI/fpdi.php');


class TitreConges extends FPDI {
	// var $nom = iconv('UTF-8', 'ISO-8859-1', 'Forest Agnès'); // Si la police n'est pas utf8
	// $maxlength; Longueur maximale du motif de congé
	/**
	 * Numéro de ligne en train d'être édité
	 */
	private static $numeroLigne = 0;
	/**
	 * Nombre maximum de ligne par page
	 */
	private static $maxLignes = 13;
	/**
	 * Coordonnée X du nom de la personne en congé
	 */
	private $cNom = 8;
	/**
	 * Coordonnées X du type de congé
	 *
	 * Ceci correspond à la case qui recevra le nombre de congés
	 */
	private $cTypeCong = array(
		1	=> 60	// V
		, 2	=> 70	// F
		, 3	=> 78	// W
		, 8	=> 87	// Rcup
		, 30	=> 113	// VRO
		, 32	=> 96	// Vex
		//	=> 104	// Comp FMP
	);
	/**
	 * Coordonnée X des remarques éventuelles
	 */
	private $cRemarques = 177;
	/**
	 * Coordonnées XY du nom du rédacteur
	 */
	private $cRedacteur = array(30, 184);
	/**
	 * Coordonnées XY de l'équipe
	 */
	private $cTeam = array(25, 25);
	/**
	 * Coordonnées XY de la date de dépôt des congés
	 */
	private $cdateTitre = array(96, 191);
	/**
	 * Coordonnée X des dates de congés
	 */
	private $cDateCong = 129;
	/**
	 * Coordonnée Y des différentes lignes
	 */
	private $cLigne = array(
		56
		, 65
		, 75
		, 84
		, 94
		, 103
		, 112
		, 122
		, 131
		, 141
		, 150
		, 159
		, 169
	);
	/**
	 * Centre pour lequel on édite les congés
	 */
	private $centre;
	/**
	 * L'équipe pour laquelle on édite les congés
	 */
	private $team;
	/**
	 * La date d'édition du titre
	 */
	private $dateTitre;
	/**
	 * La date jusqu'à laquelle il faut éditer les congés
	 */
	private $dateUntil;
	/**
	 * Le nom du rédacteur du titres
	 */
	private $redacteur;
	/**
	 * Un objet Database
	 */
	private $db;
	/**
	 * Le nombre de congés édités
	 */
	private $quantity;

/*
 * Constructeur
 */
	/**
	 * Constructeur de l'objet TitreConges
	 *
	 * @param
	 *
	 * @return
	 */
	public function __construct($dateUntil = NULL, $dateTitre = NULL, $redacteur = NULL, $centre = NULL, $team = NULL, $db = NULL) {
		parent::__construct();
		$this->SetMargins(0, 0);
		$this->SetAutoPageBreak(false, 0);
		$this->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$this->SetFont('DejaVu', '', 8);

		if (!is_null($dateUntil)) {
			$this->dateUntil($dateUntil);
		} else {
			$this->dateUntil(date('Y-m-d'));
		}
		if (!is_null($dateTitre)) {
			$this->dateTitre($dateTitre);
		} else {
			$this->dateTitre(date('Y-m-d'));
		}
		if (!is_null($redacteur)) {
			$this->redacteur($redacteur);
		} else {
			$this->redacteur(ucfirst($_SESSION['utilisateur']->prenom()) . " " . ucwords($_SESSION['utilisateur']->nom()));
		}
		if (!is_null($centre)) {
			$this->centre($centre);
		} else {
			$this->centre($_SESSION['utilisateur']->centre());
		}
		if (!is_null($team)) {
			$this->team($team);
		} else {
			$this->team($_SESSION['utilisateur']->team());
		}
		if (!is_null($db)) {
			$this->db($db);
		} else {
			$this->db($_SESSION['db']);
		}
		// Édite les titres de congés
		$this->editTitreConges();
	}

/*
 * Accesseurs
 */
	/**
	 * Attribution et accès à centre
	 *
	 * @param $centre string centre concerné par le titre
	 *
	 * @return string centre concerné par le titre
	 */
	public function centre($centre = NULL) {
		if (! is_null($centre)) {
			$this->centre = $centre;
		}
		return $this->centre;
	}
	/**
	 * Attribution et accès à team
	 *
	 * @param $team string team concerné par le titre
	 *
	 * @return string team concerné par le titre
	 */
	public function team($team = NULL) {
		if (! is_null($team)) {
			$this->team = $team;
		}
		return $this->team;
	}
	/**
	 * Attribution et accès à dateTitre
	 *
	 * @param $dateTitre string dateTitre concerné par le titre
	 *
	 * @return string dateTitre concerné par le titre
	 */
	public function dateTitre($dateTitre = NULL) {
		if (! is_null($dateTitre)) {
			$this->dateTitre = new Date($dateTitre);
		}
		return $this->dateTitre;
	}
	/**
	 * Attribution et accès à dateUntil
	 *
	 * @param $dateUntil string dateUntil concerné par le titre
	 *
	 * @return string dateUntil concerné par le titre
	 */
	public function dateUntil($dateUntil = NULL) {
		if (! is_null($dateUntil)) {
			$this->dateUntil = $dateUntil;
		}
		return $this->dateUntil;
	}
	/**
	 * Attribution et accès à redacteur
	 *
	 * @param $redacteur string redacteur concerné par le titre
	 *
	 * @return string redacteur concerné par le titre
	 */
	public function redacteur($redacteur = NULL) {
		if (! is_null($redacteur)) {
			$this->redacteur = $redacteur;
		}
		return $this->redacteur;
	}
	/**
	 * Attribution et accès à db
	 *
	 * @param $db object Database
	 *
	 * @return object Database
	 */
	public function db($db = NULL) {
		if (! is_null($db)) {
			$this->db = $db;
		}
		return $this->db;
	}
	/**
	 * Accès au nombre de congés édités
	 *
	 * @param void
	 *
	 * @return int Le nombre de congés édités
	 */
	public function quantity() {
		return $this->quantity;
	}

/*
 * Début des méthodes
 */
	/**
	 * Création d'une nouvelle page de titre
	 */
	protected function initPage() {
		$this->setSourceFile('tfpdf/Conges2016.pdf');
		$tplId = $this->importPage(1);
		$size = $this->getTemplateSize($tplId);
		if ($size['w'] > $size['h']) {
			$this->AddPage('L', array($size['w'], $size['h']));
		} else {
			$this->AddPage('P', array($size['w'], $size['h']));
		}
		$this->useTemplate($tplId);
		$this->SetFont('DejaVu', '', 10);
		// Ajoute la date de l'édition des titres
		$this->SetXY($this->cdateTitre[0], $this->cdateTitre[1]);
		$this->Cell(0, 0, $this->dateTitre()->formatDate());
		// Ajoute le nom du rédacteur
		$this->SetXY($this->cRedacteur[0], $this->cRedacteur[1]);
		$this->Cell(0, 0, $this->redacteur());
		// Ajoute l'équipe du rédacteur
		$this->SetXY($this->cTeam[0], $this->cTeam[1]);
		$this->Cell(0, 0, $this->team() . ucfirst(substr($this->centre, -1)));
	}
	/**
	 * Ajoute une ligne à la feuille des congés
	 *
	 * Si il s'agit de la première page ou si le nombre de ligne maximal est atteint
	 * une nouvelle page est créée.
	 */
	protected function addLine($nom, $did, $nbCong, $dateCong, $team, $remarques = NULL) {
		// On ouvre une nouvelle page chaque fois que le nombre maximum de lignes est atteint
		if (TitreConges::$numeroLigne % TitreConges::$maxLignes == 0) {
			$this->initPage();
		}
		$this->SetFont('DejaVu', '', 8);

		// Position de la ligne à ajouter
		$Y = $this->cLigne[TitreConges::$numeroLigne % TitreConges::$maxLignes];

		// Passe le texte en rouge pour les annulations
		if ($remarques == 'ANNULATION') {
			$this->setTextColor(255, 0, 0);
		} else {
			$this->setTextColor(0, 0, 0);
		}
		// Ajoute le nom pour le congé
		$this->SetXY($this->cNom, $Y);
		$this->Cell(0, 0, $nom);

		// Ajoute le nbCong pour le congé
		$this->SetXY($this->cTypeCong[$did], $Y);
		$this->Cell(0, 0, $nbCong);

		// Ajoute le dateCong pour le congé
		$this->SetXY($this->cDateCong, $Y);
		$this->Cell(0, 0, $dateCong);

		// Ajoute le remarques pour le congé
		if (!is_null($remarques)) {
			$this->SetXY($this->cRemarques, $Y);
			$this->Cell(0, 0, $remarques);
		}

		// Incrémentation du nombre de ligne
		TitreConges::$numeroLigne++;
	}

	/**
	 * Édition complète des congés
	 *
	 * @param $date string La date jusqu'à laquelle on veut éditer les titres
	 *        $centre string Le centre concerné
	 *        $team string L'équipe concernée
	 *        $db object Un objet Database
	 *
	 * @return void NULL si aucun congé à poser
	 */
	public function editTitreConges() {
		$arr = array();
		$nbConges = 0;
		// Les propriétés de l'objet sont initialisées par défaut dans le constructeur
		//
		$date1 = new Date($this->dateUntil);
		// Les congés sont édités au moins jusque la date
		// la plus tardive des annulations de congés
		// (si il y a annulation de congés, c'est que les congés
		// ont au moins déjà été édités jusque là)
		// On recherche donc la date maximale des annulations
		$row = $_SESSION['db']->db_fetch_array($_SESSION['db']->db_interroge(sprintf("
			SELECT MAX(`date`)
			FROM `TBL_USERS` AS `u`
			INNER JOIN `TBL_VACANCES_A_ANNULER` AS `v`
			ON `u`.`uid` = `v`.`uid`
			AND `edited` IS FALSE
			INNER JOIN `TBL_AFFECTATION` AS `a`
			ON `a`.`uid` = `u`.`uid`
			AND `centre` = '%s'
			AND `team` = '%s'
			AND `date` BETWEEN `beginning` AND `end`
			AND `principale` IS FALSE"
			, $this->centre
			, $this->team)
		));
		if ($date1->compareDate($row[0] < 0)) {
			$this->dateUntil($row[0]);
			$date1 = new Date($row[0]);
		}
		$date2 = clone $date1;
		$date2->addJours(Cycle::getCycleLength($this->centre, $this->team) - 1); // FIXME génère une erreur 500 si $date2 est une date vide

		/*
		 *  Recherche les congés et les annulations qui doivent être déposés
		 */
		$sql = sprintf("
			SELECT DISTINCT `nom`,
			`prenom`,
			`did`,
			`date`,
			`title`,
			`u`.`uid`
			FROM `TBL_USERS` AS `u`
			INNER JOIN `TBL_L_SHIFT_DISPO` AS `l`
			ON `l`.`uid` = `u`.`uid`
			AND `date` < (SELECT `date`
				FROM `TBL_GRILLE` AS `g`
				WHERE `date` BETWEEN '%s' AND '%s'
				AND `cid` = (
					SELECT `cid`
					FROM `TBL_CYCLE`
					WHERE (`centre` = 'all' OR `centre` = '%s')
					AND (`team` = 'all' OR `team` = '%s')
					ORDER BY `rang` DESC
					LIMIT 1
					)
				AND `centre` = '%s'
				AND `team` = '%s'
				)
			INNER JOIN `TBL_AFFECTATION` AS `a`
			ON `a`.`uid` = `u`.`uid`
			AND `centre` = '%s'
			AND `team` = '%s'
			AND `date` BETWEEN `beginning` AND `end`
			AND `principale` IS FALSE
			INNER JOIN `TBL_VACANCES` AS `v`
			ON `l`.`sdid` = `v`.`sdid`
			AND `etat` = 0
			UNION
			SELECT DISTINCT `nom`,
			`prenom`,
			`did`,
			`date`,
			'ANNULATION',
			`u`.`uid`
			FROM `TBL_USERS` AS `u`
			INNER JOIN `TBL_VACANCES_A_ANNULER` AS `v`
			ON `u`.`uid` = `v`.`uid`
			AND `edited` IS FALSE
			INNER JOIN `TBL_AFFECTATION` AS `a`
			ON `a`.`uid` = `u`.`uid`
			AND `centre` = '%s'
			AND `team` = '%s'
			AND `date` BETWEEN `beginning` AND `end`
			AND `principale` IS FALSE
			ORDER BY `nom`
			, `did`
			, `date`
			, `title`
			", $date1->date()
			, $date2->date()
			, $this->centre
			, $this->team
			, $this->centre
			, $this->team
			, $this->centre
			, $this->team
			, $this->centre
			, $this->team
		);
		$result = $this->db->db_interroge($sql);
		while ($row = $this->db->db_fetch_array($result)) {
			$arr[$row['nom'].$row['prenom']][$row['date']] = array(
				'did'		=> $row['did']
				,'uid'		=> $row['uid']
				,'title'	=> $row['title'] // Le commentaire pour les congés exceptionnels par exemple
				,'nom'		=> strtoupper($row['nom']) . " " . ucwords($row['prenom'])
				,'traite'	=> 0 // État du congé (0 = non traité, 1 = déjà édité)
			);
		}
		mysqli_free_result($result);

		/*
		 * Il n'y a pas de congé à poser...
		 */
		if (sizeof($arr) == 0) {
			$this->quantity = 0;
			return NULL;
		} else {
			foreach (array_keys($arr) as $nom) {
				foreach(array_keys($arr[$nom]) as $date) {
					if ($arr[$nom][$date]['traite'] === 0) { // On ne traite que les congés qui ne l'ont pas encore été
						$nbConges++;
						$dateDebut = new Date($date);
						$nbCong = 0;
						// On doit vérifier si le jour travaillé suivant est un congé et de même type
						$prochainJt = new jourTravail($date, $affectation['centre'], $affectation['team']);

						do {
							$nbCong++;
							$dateFin = clone $prochainJt; // La date de fin de congé est le jour du congé si il n'y a qu'un seul jour de congé
							// S'agit-il d'une annulation ?
							if ($arr[$nom][$date]['title'] == 'ANNULATION') {
								// Le jour présentement traité passe à l'état 1 (filed)
								$this->db->db_interroge(sprintf("
								UPDATE `TBL_VACANCES_A_ANNULER`
								SET `edited` = 1
								WHERE `date` = '%s'
								AND `uid` = %d
								", $prochainJt->date()
								, $arr[$nom][$date]['uid']
							));
							$arr[$nom][$dateFin->date()]['traite'] = 1; // Ce congé est traîté
							$prochainJt = clone $prochainJt->nextWorkingDay();
							$sql = sprintf("
								SELECT `edited`
								, `did`
								FROM `TBL_VACANCES_A_ANNULER`
								WHERE `uid` = %d
								AND `date` = '%s'
								AND `edited` IS FALSE
								", $arr[$nom][$date]['uid']
								, $prochainJt->date());
							$this->db->db_interroge('
								CALL messageSystem("Annulation de congé.", "DEBUG", "conges.php", "annul conG", "uid:' . $arr[$nom][$date]['uid'] . ';date:' . $prochainJt->date() . ';")');
						} else {
							// Le jour présentement traité passe à l'état 1 (filed)
							// Ceci corrige un bug : précédemment, l'état des jours
							// où des congés étaient déposés passaient en totalité à 1
							// Or, il pouvait très bien y avoir dans cette période des
							// congés dont l'état était déjà à 2 (confirmed).
							$this->db->db_interroge(sprintf("
								UPDATE `TBL_VACANCES`
								SET `etat` = 1
								WHERE `sdid` = (SELECT `sdid`
								FROM `TBL_L_SHIFT_DISPO`
								WHERE `date` = '%s'
								AND `uid` = %d
								AND `pereq` IS FALSE
								ORDER BY `sdid` DESC
								LIMIT 1
								)
								", $prochainJt->date()
								, $arr[$nom][$date]['uid']
							));
							$arr[$nom][$dateFin->date()]['traite'] = 1; // Ce congé est traîté
							$prochainJt = clone $prochainJt->nextWorkingDay();
							$sql = sprintf("
								SELECT `etat`
								, `did`
								FROM `TBL_VACANCES` AS `v`
								, `TBL_L_SHIFT_DISPO` AS `l`
								WHERE `v`.`sdid` = `l`.`sdid`
								AND `uid` = %d
								AND `date` = '%s'
								AND `etat` = 0
								", $arr[$nom][$date]['uid']
								, $prochainJt->date());
						}
						$row = $this->db->db_fetch_array($this->db->db_interroge($sql));
						} while (!empty($row[1]) && $row[1] == $arr[$nom][$date]['did']);

						// Formattage des dates des congés :
						// si un ou deux jours, les dates des jours sont énumérées
						// sinon on affiche la date de début et la date de fin
						if ($nbCong == 1) {
							$dateCong = $dateDebut->formatDate();
						} elseif ($nbCong == 2) {
							$dateCong = $dateDebut->formatDate() . ', ' . $dateFin->formatDate();
						} elseif ($nbCong > 2) {
							$dateCong = $dateDebut->formatDate() . ' -> ' . $dateFin->formatDate();
						}

						if ($arr[$nom][$date]['did'] == 1) {
							$nbCong = (string) $nbCong / 6;
							$nbCong = preg_replace('/\./', ',', $nbCong);
							$dateFin->addJours(3); // Un congé demi-cycle comprend les trois jours de repos
						}
						$this->addLine(
							$arr[$nom][$date]['nom']
							, $arr[$nom][$date]['did']
							, $nbCong
							, $dateCong
							, $affectation['team'] . strtoupper(substr($affectation['centre'], -1))
							, $arr[$nom][$date]['title']
						);
					}
				}
			}
			$this->quantity = $nbConges;
			$this->editTitres();
		}
		return $this->quantity; // Retourne le nombre de jours de congés posés
	}

	/**
	 * Sauvegarde la feuille des congés et en envoie une copie au navigateur
	 */
	public function editTitres() {
		$titre = sprintf("%s/titresConges/%s_%s_%s_%s.pdf", INSTALL_DIR, $this->centre, $this->team, date('YmdHis'), md5($_SESSION['utilisateur']->login() . date('YmdHis')));
		$this->Output($titre, 'F');
		$this->Output('titres.pdf', 'D');
	}
}

?>
