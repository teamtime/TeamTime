<?php
// class_utilisateursdelagrille.inc.php
//
// étend la classe utilisateur aux utilisateurs de la grille
//

/*
	TeamTime is a software to manage people working in team on a cyclic shift.
	Copyright (C) 2012 Manioul - webmaster@teamtime.me

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class utilisateursDeLaGrille {
	private static $_instance = null;
	public static function getInstance() {
		if (is_null(self::$_instance)) {
			self::$_instance = new utilisateursDeLaGrille();
		}
		return self::$_instance;
	}
	private $users = array();

	public function __construct() {
	}
	/** Retourne une table d'utilisateurGrille en fonction de la requête sql passée en argument.
	 *
	 * @param string requête SQL
	 *
	 * @return array
	 */
	public function retourneUsers($sql) {
		$result = $_SESSION['db']->db_interroge($sql);
		while ($row = $_SESSION['db']->db_fetch_assoc($result)) {
			$this->users[$row['uid']] = new utilisateurGrille($row);
		}
		mysqli_free_result($result);
		return $this->users;
	}
	/**
	 * Efface la table des utilisateurGrille.
	 */
	public function flushUsers() {
		$this->users = array();
	}
	/**
	 * Retourne une table d'utilisateurGrille d'utilisateurs actifs pour une affectation précise.
	 */
	public function getActiveUsersFromTo($from = NULL, $to = NULL, $centre = NULL, $team = NULL) {
		return $this->getUsersFromTo($from, $to, $centre, $team, 1);
	}
	/**
	 * Retourne une table d'utilisateurGrille d'utilisateurs actifs pour une affectation principale.
	 */
	public function getActiveUsersFromToPrincipale($from = NULL, $to = NULL, $centre = NULL, $team = NULL) {
		return $this->getUsersFromTo($from, $to, $centre, $team, 1, true);
	}
	/**
	 * Retourne une table d'utilisateurGrille filtrée par plusieurs critères.
	 *
	 * La liste des utilisateurs peut être classée à l'aide de $_REQUEST['order']
	 * - nom classe les utilisateurs selon leur nom
	 * - aff classe les utilisateurs selon leur affectation puis par leur poids
	 * - affn classe les utilisateurs selon leur affectation puis par leur nom.
	 *
	 * $_REQUEST['inaff'] (quelque soit la valeur) permet de retrouver les utilisateurs sans affectation.
	 *
	 *
	 * @param $centre STRING permet de filtrer sur le centre d'affectation.
	 * @param $team STRING permet de filtrer sur l'équipe des utilisateurs.
	 * @param $active INT 0 permet d'afficher les utilisateurs non actifs,
	 * 		      1 uniquement les actifs
	 * 		      et tout le monde pour n'importe quelle autre valeur.
	 * 		      Le critère actif est valable uniquement pour les utilisateurs affectés.
	 * 		      Pour les utilisateurs non affectés, il faut obligatoirement ajouter le paramètre $_GET['inaff'].
	 * @param $principale BOOLEAN true ne sélectionne que les utilisateurs dont l'affectation est principale
	 *                            false sélectionne les utilisateurs dont l'affectation courante est ou n'est pas principale
	 *
	 *
	 * @return array tableau d'objets utilisateurGrille.
	 */
	public function getUsersFromTo($from = NULL, $to = NULL, $centre = NULL, $team = NULL, $active = 1, $principale = false) {
		if (is_null($from)) $from = date('Y-m-d');
		if (is_null($to)) $to = date('Y-m-d');
		$affectation = $_SESSION['utilisateur']->affectationOnDate($from);
		if (is_null($centre)) {
			if (array_key_exists('ADMIN', $_SESSION)) {
				$centre = 'all';
			} else {
				$centre = $affectation['centre'];
			}
		}
		if (is_null($team)) {
			if (array_key_exists('EDITEURS', $_SESSION)) {
				$team = 'all';
			} else {
				$team = $affectation['team'];
			}
		}
		// Recherche les non affectés
		if (array_key_exists('EDITEURS', $_SESSION) && array_key_exists('inaff', $_REQUEST)) {
			$sql = "SELECT DISTINCT `TU`.`uid`,
				`TU`.*,
				'vide' AS `centre`,
				'vide' AS `team`
				FROM `TBL_USERS` AS `TU`
				WHERE `TU`.`uid` NOT IN (SELECT `uid`
							FROM `TBL_AFFECTATION`
							WHERE`beginning` <= '$to'
							AND `end` >= '$from'
							AND `principale` IS FALSE
							)";
			if (1 == $active) $sql .= "
				AND `TU`.`actif` = 1 ";
			if (0 == $active) $sql .= "
				AND `TU`.`actif` = 0 ";
			if (array_key_exists('order', $_REQUEST)) {
				if ($_REQUEST['order'] == 'nom') {
					$sql .= "ORDER BY `TU`.`nom` ASC";
				} elseif ($_REQUEST['order'] == 'uid') {
					$sql .= "ORDER BY `TU`.`uid` ASC";
				}
			} else {
				$sql .= "ORDER BY `TU`.`poids` ASC";
			}
		} else {
			if ('all' == $centre && 'all' == $team) {
				$sql = "SELECT DISTINCT `TU`.`uid`,
					`TU`.*,
					`TA`.`centre`,
					`TA`.`team`,
					`TA`.`poids`
					FROM `TBL_USERS` AS `TU`
					, `TBL_AFFECTATION` AS `TA`
					WHERE `TU`.`uid` = `TA`.`uid`";
				if (-1 != $from && -1 != $to) $sql .= "
					AND `TA`.`beginning` <= \"$to\"
					AND `TA`.`end`  >= \"$from\"";
				if (1 == $active) $sql .= "
					AND `TU`.`actif` = 1 ";
				if (0 == $active) $sql .= "
					AND `TU`.`actif` = 0 ";
				if (array_key_exists('order', $_REQUEST)) {
					if ($_REQUEST['order'] == 'nom') {
						$sql .= "ORDER BY `TU`.`nom` ASC";
					} elseif ($_REQUEST['order'] == 'uid') {
						$sql .= "ORDER BY `TU`.`uid` ASC";
					} elseif ($_REQUEST['order'] == 'aff') {
						$sql .= "ORDER BY `TA`.`centre`, `TA`.`team`, `TA`.`poids` ASC";
					} elseif ($_REQUEST['order'] == 'affn') {
						$sql .= "ORDER BY `TA`.`centre`, `TA`.`team`, `TU`.`nom` ASC";
					}
				} else {
					$sql .= "ORDER BY `TA`.`poids` ASC";
				}
			} elseif ('all' == $centre) {
				$sql = "SELECT DISTINCT `TU`.`uid`,
					`TU`.*,
					`TA`.`centre`,
					`TA`.`team`,
					`TA`.`poids`
					FROM `TBL_USERS` AS `TU`
					, `TBL_AFFECTATION` AS `TA`
					WHERE `TU`.`uid` = `TA`.`uid`
					AND `TA`.`team`= \"$team\"";
				if (-1 != $from && -1 != $to) $sql .= "
					AND `TA`.`beginning` <= \"$to\"
					AND `TA`.`end`  >= \"$from\"";
				if (1 == $active) $sql .= "
					AND `TU`.`actif` = 1 ";
				if (0 == $active) $sql .= "
					AND `TU`.`actif` = 0 ";
				if (array_key_exists('order', $_REQUEST)) {
					if ($_REQUEST['order'] == 'nom') {
						$sql .= "ORDER BY `TU`.`nom` ASC";
					} elseif ($_REQUEST['order'] == 'uid') {
						$sql .= "ORDER BY `TU`.`uid` ASC";
					} elseif ($_REQUEST['order'] == 'aff') {
						$sql .= "ORDER BY `TA`.`centre`, `TA`.`team`, `TA`.`poids` ASC";
					} elseif ($_REQUEST['order'] == 'affn') {
						$sql .= "ORDER BY `TA`.`centre`, `TA`.`team`, `TU`.`nom` ASC";
					}
				} else {
					$sql .= "ORDER BY `TA`.`poids` ASC";
				}
			} elseif ('all' == $team) {
				$sql = "SELECT DISTINCT `TU`.`uid`,
					`TU`.*,
					`TA`.`centre`,
					`TA`.`team`,
					`TA`.`poids`
					FROM `TBL_USERS` AS `TU`
					, `TBL_AFFECTATION` AS `TA`
					WHERE `TU`.`uid` = `TA`.`uid`
					AND `TA`.`centre`= \"$centre\"";
				if (-1 != $from && -1 != $to) $sql .= "
					AND `TA`.`beginning` <= \"$to\"
					AND `TA`.`end`  >= \"$from\"";
				if (1 == $active) $sql .= "
					AND `TU`.`actif` = 1 ";
				if (0 == $active) $sql .= "
					AND `TU`.`actif` = 0 ";
				if (array_key_exists('order', $_REQUEST)) {
					if ($_REQUEST['order'] == 'nom') {
						$sql .= "ORDER BY `TU`.`nom` ASC";
					} elseif ($_REQUEST['order'] == 'uid') {
						$sql .= "ORDER BY `TU`.`uid` ASC";
					} elseif ($_REQUEST['order'] == 'aff') {
						$sql .= "ORDER BY `TA`.`centre`, `TA`.`team`, `TA`.`poids` ASC";
					} elseif ($_REQUEST['order'] == 'affn') {
						$sql .= "ORDER BY `TA`.`centre`, `TA`.`team`, `TU`.`nom` ASC";
					}
				} else {
					$sql .= "ORDER BY `TA`.`poids` ASC";
				}
			} else {
				$sql = "SELECT DISTINCT `TU`.`uid`,
					`TU`.*,
					`TA`.`centre`,
					`TA`.`team`,
					`TA`.`poids`
					FROM `TBL_USERS` AS `TU`
					, `TBL_AFFECTATION` AS `TA`
					WHERE `TU`.`uid` = `TA`.`uid`
					AND `TA`.`principale` IS " . ($principale ? 'TRUE' : 'FALSE') ."
					AND `TA`.`centre`= \"$centre\"
					AND `TA`.`team` = \"$team\"";
				if (-1 != $from && -1 != $to) $sql .= "
					AND `TA`.`beginning` <= \"$to\"
					AND `TA`.`end`  >= \"$from\"";
				if (1 == $active) $sql .= "
					AND `TU`.`actif` = 1 ";
				if (0 == $active) $sql .= "
					AND `TU`.`actif` = 0 ";
				if (array_key_exists('order', $_REQUEST)) {
					if ($_REQUEST['order'] == 'nom') {
						$sql .= "ORDER BY `TU`.`nom` ASC";
					} elseif ($_REQUEST['order'] == 'uid') {
						$sql .= "ORDER BY `TU`.`uid` ASC";
					} elseif ($_REQUEST['order'] == 'aff') {
						$sql .= "ORDER BY `TA`.`centre`, `TA`.`team`, `TA`.`poids` ASC";
					} elseif ($_REQUEST['order'] == 'affn') {
						$sql .= "ORDER BY `TA`.`centre`, `TA`.`team`, `TU`.`nom` ASC";
					}
				} else {
					$sql .= "ORDER BY `TA`.`poids` ASC";
				}
			}
		}
		return self::retourneUsers($sql);
	}
	// Méthodes utiles pour l'affichage
	public function usersCell($dateDebut) {
		$array = array();
		foreach ($this->users as $user) {
			$array[] = $user->userCell($dateDebut);
		}
		return $array;
	}
	public function getActiveUsersCell($from, $to, $centre = 'LFFFE', $team = '9', $principale = false) {
		if ($principale) {
			$sql = sprintf("
				SELECT DISTINCT `a`.`uid`
				, `nom`
				, `prenom`
				, `grade`
				, `a`.`poids`
				FROM `TBL_AFFECTATION` AS `a`
				, `TBL_USERS` AS `u`
				WHERE `a`.`uid` = `u`.`uid`
				AND `actif`  IS TRUE
				AND `a`.`centre` = '%s'
				AND `a`.`team` = '%s'
				AND (
				(`a`.`beginning` <= '%s'
				AND `a`.`end` >= '%s'
				AND `principale` IS TRUE)
				OR
				`a`.`end` >= NOW()
				)
				ORDER BY `a`.`poids` ASC
				, `nom` ASC"
				, $_SESSION['db']->db_real_escape_string($centre)
				, $_SESSION['db']->db_real_escape_string($team)
				, $_SESSION['db']->db_real_escape_string($to)
				, $_SESSION['db']->db_real_escape_string($from)
			);
		} else {
			$sql = sprintf("
				SELECT DISTINCT `a`.`uid`
				, `nom`
				, `prenom`
				, `grade`
				, `a`.`poids`
				FROM `TBL_AFFECTATION` AS `a`
				, `TBL_USERS` AS `u`
				WHERE `a`.`uid` = `u`.`uid`
				AND `actif`  IS TRUE
				AND `a`.`centre` = '%s'
				AND `a`.`team` = '%s'
				AND `a`.`beginning` <= '%s'
				AND `a`.`end` >= '%s'
				AND `principale` IS FALSE
				ORDER BY `a`.`poids` ASC
				, `nom` ASC"
				, $_SESSION['db']->db_real_escape_string($centre)
				, $_SESSION['db']->db_real_escape_string($team)
				, $_SESSION['db']->db_real_escape_string($to)
				, $_SESSION['db']->db_real_escape_string($from)
			);
		}
		$oldUid = -1; // Pour gérer des classes multiples
		$i = 0;
		$array = array();
		$result = $_SESSION['db']->db_interroge($sql);
		while($row = $_SESSION['db']->db_fetch_assoc($result)) {
			if ($row['uid'] == $oldUid) { // On ajoute une classe
				$array[$i]['classe'] .= " " . $row['classe'];
			} else {
				$oldUid = $row['uid'];
				$i++;
				$array[$i] = array(
					'nom'		=> htmlentities($row['nom'], ENT_NOQUOTES, 'utf-8')
					, 'prenom'	=> htmlentities($row['prenom'], ENT_NOQUOTES, 'utf-8')
					, 'classe'	=> 'nom ' . htmlentities($row['grade'], ENT_NOQUOTES, 'utf-8')
					, 'id'		=> 'u' . $row['uid']
					, 'uid'		=> $row['uid']
				);
			}
		}
		mysqli_free_result($result);
		return $array;
	}
	/**
	 * Recherche les utilisateurs d'une équipe
	 *
	 * @param $centre string centre recherché
	 *        $team string équipe recherchée
	 *
	 * @return
	 */
	public function getUsersPrincipale($centre, $team, $date = NULL) {
		if (is_string($date)) {
			$date = new Date($date);
		}
		if (is_null($date) || !is_a($date, 'Date')) {
			$date = new Date(date('Y-m-d'));
		}
		$sql = sprintf("SELECT DISTINCT `a`.`uid`
			FROM `TBL_AFFECTATION` AS `a`
			, `TBL_USERS` AS `u`
			WHERE `a`.`uid` = `u`.`uid`
			AND `a`.`principale` IS TRUE
			AND `a`.`centre` = '%s'
			AND `a`.`team` = '%s'
			AND '%s' BETWEEN `a`.`beginning` AND `a`.`end`
			AND `actif`  IS TRUE
			ORDER BY `a`.`poids` ASC
			, `nom` ASC"
			, $_SESSION['db']->db_real_escape_string($centre)
			, $_SESSION['db']->db_real_escape_string($team)
			, $date->date()
		);
		return self::retourneUsers($sql);
	}
	/**
	 * Recherche les utilisateurs d'une équipe en affectation principale
	 * plus les éventuels utilisateurs de passage dans l'équipe (renforts)
	 *
	 * @param
	 *
	 * @return array
	 */
	public function getTableauUsersPrincipaleAndOccasional($year, $centre, $team) {
		if ($year >= date('Y')) {
		$sql = sprintf("
			SELECT DISTINCT `a`.`uid`
			, `a`.`poids` AS `poids`
			FROM `TBL_USERS` AS `u`
			, `TBL_AFFECTATION` AS `a`
			WHERE `u`.`uid` = `a`.`uid`
			AND `a`.`principale` IS FALSE
			AND `a`.`centre`= \"%s\"
			AND `a`.`team` = \"%s\"
			AND `a`.`beginning` <= \"%s-12-31\"
			AND `a`.`end`  >= \"%s\"
			AND `u`.`actif` IS TRUE
			AND `a`.`poids` != 0
			UNION
			SELECT DISTINCT `a`.`uid`
			, `a`.`poids` AS `poids`
			FROM `TBL_AFFECTATION` AS `a`
			, `TBL_USERS` AS `u`
			WHERE `a`.`uid` = `u`.`uid`
			AND `a`.`principale` IS TRUE
			AND `a`.`centre` = '%s'
			AND `a`.`team` = '%s'
			AND `a`.`beginning` <= '%d-12-31'
			AND `a`.`end` >= '%d-01-01'
			AND `actif` IS TRUE
			AND `a`.`poids` != 0
			ORDER BY `poids`"
			, $_SESSION['db']->db_real_escape_string($centre)
			, $_SESSION['db']->db_real_escape_string($team)
			, $year
			, date('Y-m-d')
			, $_SESSION['db']->db_real_escape_string($centre)
			, $_SESSION['db']->db_real_escape_string($team)
			, $year
			, $year);
		} else {
		$sql = sprintf("
			SELECT DISTINCT `a`.`uid`
			, `a`.`poids` AS `poids`
			FROM `TBL_AFFECTATION` AS `a`
			, `TBL_USERS` AS `u`
			WHERE `a`.`uid` = `u`.`uid`
			AND `a`.`principale` IS TRUE
			AND `a`.`centre` = '%s'
			AND `a`.`team` = '%s'
			AND `a`.`beginning` <= '%d-12-31'
			AND `a`.`end` >= '%d-01-01'
			AND `actif` IS TRUE
			AND `a`.`poids` != 0
			ORDER BY `poids`"
			, $_SESSION['db']->db_real_escape_string($centre)
			, $_SESSION['db']->db_real_escape_string($team)
			, $year
			, $year
			, $_SESSION['db']->db_real_escape_string($centre)
			, $_SESSION['db']->db_real_escape_string($team)
			, $year
			, $year);
		}
		$result = $_SESSION['db']->db_interroge($sql);
		while ($row = $_SESSION['db']->db_fetch_assoc($result)) {
			$this->users[$row['uid']] = new utilisateurGrille((int) $row['uid']);
		}
		mysqli_free_result($result);
		return $this->users;
	}
	public function getGrilleActiveUsers($dateDebut, $nbCycle = 1) {
		global $conf;
		$dateIni = new Date($dateDebut);

		// Détermination du centre et de l'équipe de l'utilisateur
		$affectation = $_SESSION['utilisateur']->affectationOnDate($dateIni);
		if (false === $affectation) {
			$affectation = $_SESSION['utilisateur']->nextAffectation($dateIni);
			$dateIni = new Date($affectation['beginning']);
		}
		$centre = $affectation['centre'];
		$team = $affectation['team'];
		$beginningAffectation = $affectation['beginning'];
		$endAffectation = $affectation['end'];

		/*
		 * Recherche des dates de début et de fin
		 * selon la méthode retenue par la classe cycle
		 */
		$dateMin = clone $dateIni;
		$dateMin->subJours(Cycle::getCycleLength($affectation['centre'], $affectation['team'])-1);
		$dateMaxS = clone $dateIni;
		$dateMaxS->addJours(Cycle::getCycleLength($affectation['centre'], $affectation['team'])*$nbCycle-1);
		/* Cette création d'un objet Cycle permet de
		 * créer la grille dans la base de données si
		 * elle n'existe pas
		 */
		$c = new Cycle($dateMaxS);
		unset($c);
		$sql = sprintf("
			SELECT `date`
			FROM `TBL_GRILLE`
			WHERE `cid` =
				(SELECT `cid`
				FROM `TBL_CYCLE`
				WHERE `rang` = 1
				AND (`centre` = '%s' OR `centre` = 'all')
				AND (`team` = '%s' OR `team` = 'all')
			)
			AND `date` BETWEEN '%s' AND '%s'
			AND (`centre` = '%s' OR `centre` = 'all')
			AND (`team` = '%s' OR `team` = 'all')
			UNION
			SELECT MAX(`date`)
			FROM `TBL_GRILLE`
			WHERE `cid` =
				(SELECT MAX(`cid`)
					FROM `TBL_CYCLE`
					WHERE (`centre` = '%s' OR `centre` = 'all')
					AND (`team` = '%s' OR `team` = 'all')
				)
			AND `date` BETWEEN '%s' AND '%s'
			AND (`centre` = '%s' OR `centre` = 'all')
			AND (`team` = '%s' OR `team` = 'all')
			"
			, $centre
			, $team
			, $dateMin->date()
			, $dateIni->date()
			, $centre
			, $team
			, $centre
			, $team
			, $dateIni->date()
			, $dateMaxS->date()
			, $centre
			, $team
		);
		$result = $_SESSION['db']->db_interroge($sql);
		$date = array();
		while ($row = $_SESSION['db']->db_fetch_array($result)) {
			$date[] = $row[0];
		}
		$dateDebut = new Date($date[0]);
		$dateFin = new Date($date[1]);
		firePhpLog($dateDebut->date(), 'Date de début de cycle');
		firePhpLog($dateFin->date(), 'Date de fin du dernier cycle');
		
		// Recherche des infos de date pour créer un navigateur
		$nextCycle = clone($dateDebut);
		$previousCycle = clone($dateDebut);
		$nextCycle->addJours(Cycle::getCycleLength($centre, $team)*$nbCycle);
		$previousCycle->subJours(Cycle::getCycleLength($centre, $team)*$nbCycle);


		// Si l'utilisateur change d'affectation avant la date de fin,
		// on limite la date de fin à la date de changement d'affectation
		if ($dateFin->compareDate($endAffectation) > 0) {
			$dateFin = new Date($endAffectation);
		}

		// Chargement des propriétés des dispos
		$proprietesDispos = jourTravail::proprietesDispo(1, $centre, $team);

		// Jours de semaine au format court
		$jdsc = Date::$jourSemaineCourt;

		// Le tableau $users qui constituera la grille
		$users = array();

		// Les deux premières lignes du tableau sont dédiées au jourTravail (date, vacation...)
		$users[] = array('nom'		=> 'navigateur'
			,'classe'	=> 'dpt'
			,'id'		=> ''
			,'uid'		=> 'jourTravail'
		);
		$users[] = array('nom'		=> '<div class="boule"></div>'
			,'classe'	=> 'dpt'
			,'id'		=> ''
			,'uid'		=> 'jourTravail'
		);

		/*
		 * Recherche la liste des utilisateurs
		 */
		$users = array_merge($users, utilisateursDeLaGrille::getInstance()->getActiveUsersCell($dateDebut->date(), $dateFin->date(), $centre, $team));

		// Ajout d'une rangée pour le décompte des présences
		$users[] = array('nom'		=> 'décompte'
			,'class'	=> 'dpt'
			,'id'		=> 'dec'
			,'uid'		=> 'dcpt'
		);

		// Recherche des jours de travail
		//
		$cycle = array();
		if (isset($DEBUG) && true === $DEBUG) debug::getInstance()->startChrono('load_planning_duree_norepos'); // Début chrono
		for ($i=0; $i<$nbCycle; $i++) {
			$cycle[$i] = new Cycle($dateIni, $centre, $team);
			$dateIni->addJours(Cycle::getCycleLength($centre, $team));
			$cycle[$i]->cycleId($i);
		}
		if (isset($DEBUG) && true === $DEBUG) debug::getInstance()->stopChrono('load_planning_duree_norepos'); // Fin chrono

		// Lorsque l'on n'affiche qu'un cycle ou qu'on le souhaite, on ajoute des compteurs en fin de tableau
		$evenSpec = array();
		if ($nbCycle == 1 || (array_key_exists('cpt', $_COOKIE) && $_COOKIE['cpt'] == 1)) {
			// Récupération des compteurs
			if (isset($DEBUG) && true === $DEBUG) debug::getInstance()->startChrono('Relève compteur'); // Début chrono
			$sql = "
				SELECT DISTINCT `type decompte`, `did`
				FROM `TBL_DISPO`
				WHERE `actif` = TRUE
				AND `need_compteur` = TRUE
				AND `type decompte` != 'conges'
				AND (FIND_IN_SET('$centre', `centres`) OR `centres` = 'all')
				AND (`team` = 'all' OR `team` = '$team')
				ORDER BY `did` ASC
				";
			$results = $_SESSION['db']->db_interroge($sql);
			while ($res = $_SESSION['db']->db_fetch_array($results)) {
				$evenSpec[$res[0]] = array(
					'nomLong'	=> htmlspecialchars($res[0], ENT_COMPAT)
				);
			}
			mysqli_free_result($results);

			/*
			 * Recherche le décompte des évènements spéciaux
			 */
			$sql = sprintf("
				SELECT `uid`,
				`type decompte`,
				MOD(COUNT(`td`.`did`),90),
				COUNT(`td`.`did`),
				MAX(`date`)
				FROM `TBL_L_SHIFT_DISPO` AS `tl`,
				`TBL_DISPO` AS `td`
				WHERE `td`.`did` = `tl`.`did`
				AND `td`.`actif` = TRUE
				AND `date` <= '%s'
				AND `need_compteur` = TRUE
				AND `type decompte` != 'conges'
				AND `uid` IN (SELECT DISTINCT `uid`
						FROM `TBL_AFFECTATION`
						WHERE `centre` = '%s'
						AND `team` = '%s'
						AND '%s' BETWEEN `beginning` AND `end`
						AND `principale` IS FALSE
					)
				AND `td`.`did` IN (SELECT `did`
						FROM `TBL_DISPO`
						WHERE (FIND_IN_SET('%s', `centres`) OR `centres` = 'all')
						AND (`team` = 'all' OR `team` = '%s')
					)
				GROUP BY `td`.`type decompte`, `uid`"
				, $cycle[$nbCycle-1]->dateRef()->date()
				, $centre
				, $team
				, $cycle[$nbCycle-1]->dateRef()->date()
				, $centre
				, $team
				, $cycle[$nbCycle-1]->dateRef()->date()
			);
			firePhpLog($sql, 'Requête décompte évènements spéciaux');

			$results = $_SESSION['db']->db_interroge($sql);
			while ($res = $_SESSION['db']->db_fetch_array($results)) {
				$evenSpec[$res[1]]['uid'][$res[0]] = array(
					'nom'		=> $res[2]
					,'title'	=> $res[3] . " : " . $res[4]
					,'id'		=> "u" . $res[0] . "even" . $res[1]
					,'classe'	=> ""
				);
			}
			mysqli_free_result($results);
			if (isset($DEBUG) && true === $DEBUG) debug::getInstance()->stopChrono('Relève compteur'); // Fin chrono
		}

		$lastLine = count($users)-1;
		for ($i=0; $i<$nbCycle; $i++) {
			$compteurLigne = 0;
			foreach ($users as $user) {
				switch ($compteurLigne) {
					/*
					 * Première ligne contenant le navigateur, l'année et le nom du mois
					 */
				case 0:
					if ($i == 0) {
						$grille[$compteurLigne][] = array(
							'nom'		=> $cycle[$i]->dateRef()->annee()
							,'id'		=> 'navigateur'
							,'classe'	=> ''
							,'colspan'	=> 2
							,'navigateur'	=> 1 // Ceci permet à smarty de construire un navigateur entre les cycles
						);
					}
					$grille[$compteurLigne][] = array(
						'nom'		=> $cycle[$i]->dateRef()->moisAsHTML()
						,'id'		=> 'moisDuCycle' . $cycle[$i]->dateRef()->dateAsId()
						,'classe'	=> ''
						,'colspan'	=> ($i == $nbCycle-1 ? Cycle::getCycleLengthNoRepos($centre, $team)+1+count($evenspec) : Cycle::getCycleLengthNoRepos($centre, $team)+1)
					);
					break;
					/*
					 * Deuxième ligne contenant les dates, les vacations, charge et vacances scolaires
					 */
				case 1:
					// La deuxième ligne contient la description de la vacation (date...)
					if ($i == 0) {
						// Ajout d'une colonne pour le nom de l'utilisateur
						$grille[$compteurLigne][] = array(
							'classe'		=> "entete"
							,'id'			=> ""
							,'nom'			=> htmlentities("Nom", ENT_NOQUOTES, 'utf-8')
						);
						// Ajout d'une colonne pour les décomptes
						$grille[$compteurLigne][] = array(
							'classe'		=> "conf"
							,'id'			=> "conf" . $cycle[$i]->dateRef()->dateAsId()
							,'nom'			=> $cycle[$i]->conf()
						);
					}
					foreach ($cycle[$i]->dispos() as $dateVacation => $vacation) {
						// Préparation des informations de jours, date, jour du cycle (en-têtes de la grille)
						$grille[$compteurLigne][] = array(
							'jds'			=> $jdsc[$vacation['jourTravail']->jourDeLaSemaine()]
							,'jdm'			=> $vacation['jourTravail']->jour()
							,'classe'		=> $vacation['jourTravail']->ferie() ? 'ferie' : 'semaine'
							,'annee'		=> $vacation['jourTravail']->annee()
							,'mois'			=> $vacation['jourTravail']->moisAsHTML()
							,'vacation'		=> htmlentities($vacation['jourTravail']->vacation())
							,'vacances'		=> $vacation['jourTravail']->vsdesc()
							,'periodeCharge'	=> $vacation['jourTravail']->pcdesc()
							,'briefing'		=> $vacation['jourTravail']->bfdesc()
							,'id'			=> sprintf("%ss%s", $vacation['jourTravail']->dateAsId(), $vacation['jourTravail']->vacation())
							,'date'			=> $vacation['jourTravail']->date()
						);
					}
					// Ajout d'une colonne en fin de cycle
					// avec la configuration cds
					// ou une image pour la dernière colonne
					$dateCong = clone($cycle[$i]->dateRef());
					$dateCong->addJours(Cycle::getCycleLength()-1);
					if ($i < $nbCycle-1) {
						$grille[$compteurLigne][] = array(
							'confid'		=> "conf" . $cycle[$i+1]->dateRef()->dateAsId()
							,'dateCong'		=> $dateCong->date()
							,'classeconf'		=> 'conf'
							,'conges'		=> 1
							,'nom'			=> $cycle[$i+1]->conf()
							,'teamEdit'		=> $_SESSION['utilisateur']->hasRole('teamEdit')
						);
					} else {
						$grille[$compteurLigne][] = array(
							'classeconf'		=> "boule"
							,'dateCong'		=> $dateCong->date()
							,'id'			=> sprintf("sepA%sM%sJ%s", $vacation['jourTravail']->annee(), $vacation['jourTravail']->mois(), $vacation['jourTravail']->jour())
							,'date'			=> $vacation['jourTravail']->date()
							,'conges'		=> 1
							,'teamEdit'		=> $_SESSION['utilisateur']->hasRole('teamEdit')
						);
					}
					if (($nbCycle == 1 || array_key_exists('cpt', $_COOKIE)) && $i == $nbCycle - 1) {
						// Ajout d'une colonne pour les compteurs uniquement après la dernière grille
						foreach (array_keys($evenSpec) as $even) {
							$grille[$compteurLigne][] = array(
								'classe'		=> "semaine w15"
								,'id'			=> str_replace(" ", "", $evenSpec[$even]['nomLong']) // Certains noms longs comportent des espaces, ce qui n'est pas autorisé pour un id
								,'date'			=> ""
								,'nom'			=> "<div class='compteur-vertical'>" . htmlentities(ucfirst($even), ENT_NOQUOTES, 'utf-8') . "</div>"
								,'title'		=> ""
							);
						}
					}
					break;
					/*
					 * Dernière ligne contenant le nombre de présents
					 */
				case $lastLine:
					if ($i == 0) {
						$grille[$compteurLigne][] = array(
							'classe'		=> "decompte"
							,'id'			=> ""
							,'nom'			=> htmlentities("Présents", ENT_NOQUOTES, 'utf-8')
							,'colspan'	=> 2
						);
					}
					foreach ($cycle[$i]->dispos() as $dateVacation => $vacation) {
						$grille[$compteurLigne][] = array(
							'classe'		=> 'dcpt'
							,'id'			=> sprintf("deca%sm%sj%ss%sc%s", $vacation['jourTravail']->annee(), $vacation['jourTravail']->mois(), $vacation['jourTravail']->jour(), $vacation['jourTravail']->vacation(), $cycle[$i]->cycleId())
						);
					}
					// Ajout d'une colonne en fin de cycle qui permet le (dé)verrouillage du cycle
					$jtRef = $cycle[$i]->dispos($cycle[$i]->dateRef()->date());
					$lockClass = $jtRef['jourTravail']->readOnly() ? 'cadenasF' : 'cadenasO';
					$lockTitle = $jtRef['jourTravail']->readOnly() ? 'Déverrouiller le cycle' : 'Verrouiller le cycle';
					$un_lock = $jtRef['jourTravail']->readOnly() ? 'ouvre' : 'bloque';

					$grille[$compteurLigne][] = array(
						'classe'		=> "locker"
						,'id'			=> sprintf("locka%sm%sj%sc%s", $cycle[$i]->dateRef()->annee(), $cycle[$i]->dateRef()->mois(), $cycle[$i]->dateRef()->jour(), $cycle[$i]->cycleId())
						,'nom'			=> isset($_SESSION['TEAMEDIT']) ? sprintf("<div class=\"imgwrapper12\"><a href=\"lock.php?date=%s&amp;lock=%s&amp;noscript=1\"><img src=\"themes/%s/images/glue.png\" class=\"%s\" alt=\"#\" /></a></div>", $cycle[$i]->dateRef()->date(), $un_lock, $conf['theme']['current'], $lockClass) : sprintf("<div class=\"imgwrapper12\"><img src=\"themes/%s/images/glue.png\" class=\"%s\" alt=\"#\" /></div>", $conf['theme']['current'], $lockClass) // Les éditeurs ont le droit de (dé)verrouiller la grille
						,'title'	=> htmlentities($lockTitle, ENT_NOQUOTES, 'utf-8')
						,'colspan'	=> ($i == $nbCycle-1 ? 1+count($evenSpec) : 1)
					);
					break;
					/*
					 * Lignes utilisateurs
					 */
				default:
					if ($i == 0) {
						// La première colonne contient les infos sur l'utilisateur
						$grille[$compteurLigne][] = $user;
						// La deuxième colonne contient les décomptes horizontaux
						$grille[$compteurLigne][] = array(
							'nom'		=> 0+$cycle[$i]->compteTypeUser($user['uid'], 'dispo')
							,'id'		=> sprintf("decDispou%sc%s", $user['uid'], $cycle[$i]->cycleId())
							,'classe'	=> 'decompte'
						);
					}
					// On itère sur les vacations du cycle
					foreach ($cycle[$i]->dispos() as $dateVacation => $vacation) {
						$classe = "presence";
						if ($vacation['jourTravail']->readOnly()) $classe .= " protected";
						if (!empty($vacation[$user['uid']]['activite']) && !empty($proprietesDispos[$vacation[$user['uid']]['activite']]) && 1 == $proprietesDispos[$vacation[$user['uid']]['activite']]['absence']) {
							$classe .= " absent";
							// Ajout d'une classe particulière pour les congés validés
							if ('conges' == $proprietesDispos[$vacation[$user['uid']]['activite']]['type decompte']) {
								$result = $_SESSION['db']->db_interroge(sprintf("
									SELECT `etat`
									FROM `TBL_VACANCES`
									WHERE `sdid` = (SELECT `sdid`
								       			FROM `TBL_L_SHIFT_DISPO`
											WHERE `date` = '%s'
											AND `pereq` IS FALSE
											AND `uid` = %d
											AND `did` IN (SELECT `did`
													FROM `TBL_DISPO`
													WHERE `type decompte` = 'conges'
													AND (FIND_IN_SET('%s', `centres`) OR `centres` = 'all')
													AND (`team` = 'all' OR `team` = '%s')
												)
											)
									", $dateVacation
									, $user['uid']
									, $centre
									, $team
								));
								if (mysqli_num_rows($result) < 1) {
									$classe .= " erreur";
								} else {
									$row = $_SESSION['db']->db_fetch_row($result);
									if (1 == $row[0]) $classe .= " filed";
									if (2 == $row[0]) $classe .= " valide";
								}
								mysqli_free_result($result);
							}
						} else {
							// Cas des affectations en cours
							$sql = sprintf("
								SELECT `centre`, `team`, `beginning`, `end`
								FROM `TBL_AFFECTATION`
								WHERE `uid` = %d
								AND '%s' BETWEEN `beginning` AND `end`
								AND `principale` IS FALSE
								", $user['uid']
								, $dateVacation
							);
							$row = $_SESSION['db']->db_fetch_assoc($_SESSION['db']->db_interroge($sql));
							if ($row['centre'] == $centre && $row['team'] == $team) {
								$classe .= " present";
							} else {
								$classe .= " absent";
							}
						}
						$title = "";
						/*
						 * Affichage remplacements
						 */
						if (!empty($vacation[$user['uid']]['activite']) && "Rempla" == $vacation[$user['uid']]['activite']) {
							$title = "Mon remplaçant";
							$sql = sprintf("
								SELECT *
								FROM `TBL_REMPLA`
								WHERE `uid` = %d
								AND `date` = '%s'"
								, $user['uid']
								, $vacation['jourTravail']->date());
							$row = $_SESSION['db']->db_fetch_assoc($_SESSION['db']->db_interroge($sql));
							$title = $row['nom'] . " | " . $row['phone'];
						} //
						/*
						 * L'activité possède-t-elle un title à afficher ?
						 */
						if (!empty($vacation[$user['uid']]['activite']) && $title === "") {
							if (isset($vacation[$user['uid']]['title'])) {
								$title = $vacation[$user['uid']]['title'];
							} elseif (isset($proprietesDispos[$vacation[$user['uid']]['activite']]['nom_long'])) {
								$title = $proprietesDispos[$vacation[$user['uid']]['activite']]['nom_long'];
							}
						}
						// Un tableau pour les title et css
						$extra_array = array();
						if ($title !== '') {
							$extra_array = array(
								'title'		=> $title
							);
						}
						if (array_key_exists($user['uid'], $vacation)
							&& array_key_exists($vacation[$user['uid']]['activite'], $proprietesDispos)
							&& !is_null($proprietesDispos[$vacation[$user['uid']]['activite']]['css'])
						) {
							$extra_array = array_merge(
								$extra_array,
								array(
								'style'		=> $proprietesDispos[$vacation[$user['uid']]['activite']]['css']
								)
							);
						}
						$grille[$compteurLigne][] = array_merge(array(
							'nom'		=> isset($vacation[$user['uid']]['activite']) ? htmlentities($vacation[$user['uid']]['activite'], ENT_NOQUOTES, 'utf-8') : " "
							, 'id'		=> sprintf("u%s%ss%sc%s", $user['uid'], $vacation['jourTravail']->dateAsId(), $vacation['jourTravail']->vacation(), $cycle[$i]->cycleId())
							, 'classe'	=> $classe
							), $extra_array
						);
					}
					// La dernière colonne contient les décomptes horizontaux calculés
					// La date est celle de dateRef + durée du cycle
			/*$dateSuivante = clone $cycle[$i]->dateRef();
			$dateSuivante->addJours(Cycle::getCycleLength());*/
					$grille[$compteurLigne][] = array(
						'nom'		=> 0+$cycle[$i]->compteTypeUserFin($user['uid'], 'dispo')
						,'id'		=> sprintf("decDispou%sc%s", $user['uid'], $cycle[$i]->cycleId()+1)
						,'classe'	=> 'decompte'
					);
					if (($nbCycle == 1 || $_SESSION['utilisateur']->getPref('cpt')) && $i == $nbCycle - 1) {
						foreach (array_keys($evenSpec) as $even) {
							$grille[$compteurLigne][] = array(
								'nom'		=> empty($evenSpec[$even]['uid'][$user['uid']]['nom']) ? 0 : $evenSpec[$even]['uid'][$user['uid']]['nom']
								,'id'		=> empty($evenSpec[$even]['uid'][$user['uid']]['id']) ? "" : $evenSpec[$even]['uid'][$user['uid']]['id']
								,'title'	=> empty($evenSpec[$even]['uid'][$user['uid']]['title']) ? "" : $evenSpec[$even]['uid'][$user['uid']]['title']
								,'classe'	=> "decompte" . (empty($evenSpec[$even]['uid'][$user['uid']]['classe']) ? "" : $evenSpec[$even]['uid'][$user['uid']]['classe'])
							);
						}
					}
				}
				$compteurLigne++;
			}
		}

		/*
		 * Préparation des valeurs de retour
		 */
		$return = array();
		$return['nextCycle'] = $nextCycle->date();
		$return['previousCycle'] = $previousCycle->date();
		$return['presentCycle'] = date("Y-m-d");
		$return['dureeCycle'] = Cycle::getCycleLengthNoRepos($centre, $team);
		$return['anneeCycle'] = $cycle[0]->dateRef()->annee();
		$return['moisCycle'] = $cycle[0]->dateRef()->mois();
		$return['grille'] = $grille;
		$return['nbCycle'] = $nbCycle;
		/*
		 * Fin des assignations des valeurs de retour
		 */
		return $return;
	}
}
?>
