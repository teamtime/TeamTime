<?php
// smarty_ajax.inc.php

// Initialise le moteur smarty et crée un objet smarty
//
// N'ajoute aucun en-tête afin que le moteur smarty puisse être utilisé via ajax

/*
	TeamTime is a software to manage people working in team on a cyclic shift.
	Copyright (C) 2012 Manioul - webmaster@teamtime.me

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once 'smarty3/Smarty.class.php';

$smarty = new Smarty();

$smarty->template_dir = INSTALL_DIR . '/themes/' . $conf['theme']['current'] . '/templates';
$smarty->compile_dir = INSTALL_DIR . '/templates_c';
$smarty->config_dir = INSTALL_DIR . '/configs';
$smarty->cache_dir = INSTALL_DIR . '/cache';

?>
