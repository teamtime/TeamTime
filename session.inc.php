<?php
// session.inc.php
// Gère les sessions utilisateurs

/*
	TeamTime is a software to manage people working in team on a cyclic shift.
	Copyright (C) 2012 Manioul - webmaster@teamtime.me

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function session_begin($session_var, $back) {
	session_start();
	if (!isset($_SESSION[$session_var])) {
		$back = basename($back);
		header('Location:index.php' . !empty($back) ? "?back=$back" : '');
	}
}

// Insère les constantes de TBL_CONSTANTS dans une variable de session
function sql_globals_constants() {
	$sql = 'SELECT * FROM `TBL_CONSTANTS`';
	$result = $_SESSION['db']->db_interroge($sql);
	while ($row = $_SESSION['db']->db_fetch_assoc($result)) {
		if (isset($GLOBALS[$row['nom']])) {
			if (isset($DEBUG) && true === $DEBUG) debug::getInstance()->triggerError("\$GLOBALS['".$row['nom']."'] est déjà défini");
		} else {
			switch ($row['type']) {
				case 'int':
					$GLOBALS[$row['nom']] = (int) $row['valeur'];
					break;
				case 'bool':
					if ('false' == $row['valeur'] || 'FALSE' == $row['valeur']) {
						$GLOBALS[$row['nom']] = FALSE;
					} else {
						$GLOBALS[$row['nom']] = (bool) $row['valeur'];
					}
					break;
				case 'float':
					$GLOBALS[$row['nom']] = 0 + $row['valeur'];
					break;
				default:
					$GLOBALS[$row['nom']] = $row['valeur'];
			}
		}
	}
	mysqli_free_result($result);
}

function get_sql_globals_constant($constant_name) {
	if (isset($GLOBALS[$constant_name])) return $GLOBALS[$constant_name];
	$sql = "SELECT * FROM `TBL_CONSTANTS` WHERE `nom` = '$constant_name'";
	$result = $_SESSION['db']->db_interroge($sql);
	if (mysqli_num_rows($result) == 1) {
		$row = $_SESSION['db']->db_fetch_assoc($result);
		switch ($row['type']) {
			case 'int':
				$GLOBALS[$row['nom']] = (int) $row['valeur'];
				break;
			case 'bool':
				if ('false' == $row['valeur'] || 'FALSE' == $row['valeur']) {
					$GLOBALS[$row['nom']] = FALSE;
				} else {
					$GLOBALS[$row['nom']] = (bool) $row['valeur'];
				}
				break;
			case 'float':
				$GLOBALS[$row['nom']] = 0 + $row['valeur'];
				break;
			default:
				$GLOBALS[$row['nom']] = $row['valeur'];
		}
		return $GLOBALS[$row['nom']];
	} else {
		if (isset($DEBUG) && true === $DEBUG) debug::getInstance()->triggerError("Plusieurs ou aucun résultat à la requête sql '$sql'");
		return FALSE;
	}
	mysqli_free_result($result);
}

/**
 * Génération d'un token enregistré dans une variable session
 *
 * @param $length int nombre d'octets du token
 *
 * @param $session boolean positionné si le token doit être enregistré dans une variable de session ($_SESSION['token'])
 *
 * @return $token string le token produit
 */
function gen_token($length = 32, $session = TRUE) {
	if ($length < 16) {
		$length = 16;
	}
	if (version_compare(PHP_VERSION, '7.0.0') >= 0) {
		$token = bin2hex(random_bytes($length)); // PHP > 7.0
	} else {
		$token = bin2hex(openssl_random_pseudo_bytes($length));
	}
	if ($session) {
		$_SESSION['token'] = $token;
	}
	return $token;
}

/**
 * Vérifie que le token est valide
 *
 * @param $token string le token à vérifier
 *
 * @return boolean positionné si le token est valide
 */
function check_token($token) {
	return TRUE;
	if ($token == $_SESSION['token']) {
		return TRUE;
	} else {
		return FALSE;
	}
}

// Si la page nécessite que l'utilisateur soit logué, et qu'il ne l'est pas, on redirige vers la page de login
if (!empty($requireAuthenticatedUser) && !array_key_exists('AUTHENTICATED', $_SESSION)) {
	header('Location:index.php?norights=1&back=' . htmlspecialchars($_SERVER['REQUEST_URI']));
	exit;
}
if (!empty($requireEditeur) && !array_key_exists('EDITEURS', $_SESSION)) {
	header('Location:index.php?norights=1&back=' . htmlspecialchars($_SERVER['REQUEST_URI']));
	exit;
}
if (!empty($requireTeamEdit) && !array_key_exists('TEAMEDIT', $_SESSION)) {
	header('Location:index.php?norights=1&back=' . htmlspecialchars($_SERVER['REQUEST_URI']));
	exit;
}
if (!empty($requireMyEdit) && !array_key_exists('MY_EDIT', $_SESSION)) {
	header('Location:index.php?norights=1&back=' . htmlspecialchars($_SERVER['REQUEST_URI']));
	exit;
}
if (!empty($requireHeures) && !array_key_exists('HEURES', $_SESSION)) {
	header('Location:index.php?norights=1&back=' . htmlspecialchars($_SERVER['REQUEST_URI']));
	exit;
}
if (!empty($requireVirtualAdmin) && !array_key_exists('iAmVirtual', $_SESSION) && !array_key_exists('ADMIN', $_SESSION)) {
	header('Location:index.php?norights=1&back=' . htmlspecialchars($_SERVER['REQUEST_URI']));
	exit;
}
if (!empty($requireAdmin) && !array_key_exists('ADMIN', $_SESSION)) {
	header('Location:index.php?norights=1&back=' . htmlspecialchars($_SERVER['REQUEST_URI']));
	exit;
}


# S'il n'y a pas d'objet base de données défini dans la session, on en définit un
if (!array_key_exists('db', $_SESSION) || !is_a($_SESSION['db'], 'database')) {
	if (array_key_exists('utilisateur', $_SESSION) && is_a($_SESSION['utilisateur'], 'utilisateurGrille')) {
		$DSN = $GLOBALS['DSN']['user'];
		$DSN['username'] = 'ttm.'.$_SESSION['utilisateur']->uid();
		$_SESSION['db'] = new database($DSN);
	} elseif ($_SERVER['SCRIPT_NAME'] == '/createAccount.php') {
		$_SESSION['db'] = new database($GLOBALS['DSN']['createAccount']);
	} elseif ($_SERVER['SCRIPT_NAME'] == '/install.php') {
		$_SESSION['db'] = new database($GLOBALS['DSN']['admin']);
	} else {
		$_SESSION['db'] = new database($GLOBALS['DSN']['notlogged']);
	}
}

// Vérifie si le site est en maintenance
if (FALSE === get_sql_globals_constant('online') && !array_key_exists('ADMIN', $_SESSION)) header('Location:offline.html');

if (array_key_exists('utilisateur', $_SESSION)) {
	$conf['theme']['current'] = $_SESSION['utilisateur']->getPref('theme');
} else {
	$conf['theme']['current'] = $conf['theme']['default'];
}

?>
