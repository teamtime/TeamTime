<?php
/*
 * gestionUtilisateurs.js.php
 */

// Require authenticated user
// L'utilisateur doit être logué/admin pour accéder à cette page
// $requireAuthenticatedUser = true;
$requireEditeur = true;

header('Content-Type: application/javascript');
//ob_start();
$conf['page']['elements']['firePHP'] = true;
$conf['page']['include']['constantes'] = 1;
$conf['page']['include']['globalConfig'] = 1;
$conf['page']['include']['globals_db'] = 1;
$conf['page']['include']['init'] = 1;
$conf['page']['include']['session'] = 1;
require_once('../required_files.inc.php');

if (array_key_exists('ADMIN', $_SESSION)) {
	$affectations = array();
?>
affectations = new Array();
<?php
	$result = $_SESSION['db']->db_interroge("
		SELECT `centre`,
		`team`
		FROM `TBL_AFFECTATION`
		GROUP BY `centre`, `team`");
	while ($row = $_SESSION['db']->db_fetch_row($result)) {
		$affectations[$row[0]][] = $row[1];
	}
	mysqli_free_result($result);
	foreach ($affectations as $centre => $teams) {
		$index = 0;
?>
affectations["<?=$centre?>"] = new Array();
<?php
		foreach ($teams as $team) {
?>
affectations["<?=$centre?>"][<?=$index++?>] = "<?=$team?>";
<?php
		}
	}
} else {
}
