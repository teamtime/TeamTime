<?php
/*
 * online.js.php
 */

// Require authenticated user
// L'utilisateur doit être logué/admin pour accéder à cette page
// $requireAuthenticatedUser = true;
$requireAdmin = true;

header('Content-Type: application/javascript');
//ob_start();
$conf['page']['elements']['firePHP'] = true;
$conf['page']['include']['constantes'] = 1;
$conf['page']['include']['globalConfig'] = 1;
$conf['page']['include']['globals_db'] = 1;
$conf['page']['include']['init'] = 1;
$conf['page']['include']['session'] = 1;
require_once('../required_files.inc.php');

?>
$(function() {
	$("#ionoff").click(function() {
		if ($(this).hasClass('offline')) {
			$(this).removeClass('offline');
			$(this).addClass('online');
			$.post('ajax.php', {q:'LK',tk:$('#token').prop('title'),r:'on'})
			.done(function(data) {
				var oJSON = jQuery.parseJSON(data);
				traiteJSON(oJSON);
			});
		} else if ($(this).hasClass('online')) {
			$(this).removeClass('online');
			$(this).addClass('offline');
			$.post('ajax.php', {q:'LK',tk:$('#token').prop('title'),r:'off'})
			.done(function(data) {
				var oJSON = jQuery.parseJSON(data);
				traiteJSON(oJSON);
			});
		}
	});
	$("#cau").click(function() {
		$.post('ajax.php', {q:'RU',tk:$('#token').prop('title')})
		.done(function(data) {
			var oJSON = jQuery.parseJSON(data);
			traiteJSON(oJSON);
		});
	});
	$("#stm").click(function() {
		$.post('ajax.php', {q:'SM',tk:$('#token').prop('title')})
		.done(function(data) {
			var oJSON = jQuery.parseJSON(data);
			traiteJSON(oJSON);
		});
	});
});
<?php
//ob_end_flush();
?>
