<?php
/*
 *
 *  grille2.js.php
 *
*/

// Require authenticated user
// L'utilisateur doit être logué pour accéder à cette page
$requireAuthenticatedUser = true;

ob_start();

header('Content-Type: application/javascript');

$conf['page']['elements']['firePHP'] = true;
$conf['page']['include']['constantes'] = 1;
$conf['page']['include']['globalConfig'] = 1;
$conf['page']['include']['globals_db'] = 1;
$conf['page']['include']['init'] = 1;
$conf['page']['include']['session'] = 1;
require_once('../required_files.inc.php');

firePHPLog($_SESSION, 'SESSION');
// tableau des dispos
$sql = sprintf("
	SELECT *
	FROM `TBL_DISPO`
	WHERE `actif` IS TRUE
	AND (FIND_IN_SET('%s', `centres`) OR `centres` = 'all')
	AND (`team` = '%s' OR `team` = 'all')"
	, $_SESSION['utilisateur']->centre()
	, $_SESSION['utilisateur']->team()
	);
$dispos = "";
// L'ensemble des activités
$activites = array();
// Les activités autorisant une info supplémentaire (un title)
$sActTitle = "";
$result = $_SESSION['db']->db_interroge($sql);
while ($row = $_SESSION['db']->db_fetch_assoc($result)) {
	$activites[] = $row;
	if ($row['type decompte'] == 'dispo') {
		$dispos .= sprintf("'%s',", $row['dispo']);
	}
	if ($row['title']) {
		$sActTitle .= sprintf("case '%s':\n", $row['dispo']);
	}
}
mysqli_free_result($result);
$sDispos = substr($dispos, 0, -1);

$affectation = $_SESSION['utilisateur']->affectationOnDate(date('Y-m-d'));

//***********************
// Fonctions principales
//***********************
// Recherche les dispo pour une journée oThis
?>
function getAvailableOccupations(oThis) {
	var aArray = infosFromId(oThis.id);
	if (!aArray) {
	       	alert("Impossible d'obtenir les infos");
		return false;
       	}
<?php
	$find_in_set = "";
	if (!array_key_exists('ADMIN', $_SESSION)) { // Les non admins ont des restrictions sur les dispo qu'ils peuvent poser
		foreach (array_flip(array_flip(array_merge(array('all', $_SESSION['utilisateur']->login(), $affectation['grade']), $_SESSION['utilisateur']->roles()))) as $set) {
			$find_in_set .= sprintf("FIND_IN_SET('%s', `peut poser`) OR ", $_SESSION['db']->db_real_escape_string($set));
		}
		$find_in_set = " AND (" . substr($find_in_set, 0, -4) . ")";
	}
	$sqlDispo = sprintf("
		SELECT `dispo`
		, `jours possibles`
		FROM `TBL_DISPO`
		WHERE `actif` IS TRUE
		AND (FIND_IN_SET('%s', `centres`) OR `centres` = 'all')
		AND (`team` = '%s' OR `team` = 'all')
		%s
		ORDER BY `poids`"
		, $_SESSION['utilisateur']->centre()
		, $_SESSION['utilisateur']->team()
		, $find_in_set
	);
	$resDispo = $_SESSION['db']->db_interroge($sqlDispo);
	$sqlCycle = sprintf("SELECT `vacation` FROM `TBL_CYCLE` WHERE `vacation` != '%s' AND `centre` = '%s' AND (`team` = 'all' OR `team` = '%s')", REPOS, $affectation['centre'], $affectation['team']);
	$result = $_SESSION['db']->db_interroge($sqlCycle);

	while ($x = $_SESSION['db']->db_fetch_row($result)) {
		$vacations[] = $x[0];
	}
	mysqli_free_result($result);
	$aDispo = "";
	$dispo = array();
	foreach($vacations as $vacation) { // On initialise le tableau des dispos possibles
		$dispo[$vacation] = '';
	}
	while ($row = $_SESSION['db']->db_fetch_row($resDispo)) {
		foreach ($vacations as $vacation) {
			if ($row[1] == 'all' || preg_match("/$vacation/", $row[1]) > 0) { // On ajoute les dispo valides par vacation
				$dispo[$vacation] .= sprintf("'%s',", $row[0]);
			}
		}
	}
	mysqli_free_result($resDispo);
	?>
	switch (aArray["Vacation"]) {<?php
	foreach($vacations as $vacation) {?>
		case "<?=$vacation?>":
			var aDispo = new Array(<?=substr($dispo[$vacation], 0, -1)?>);
			break;
	<?php
	}?>
			default:
				var aDispoExt = new Array();
			break;
	}
	// Suppression des jours week end où il ne devrait pas y en avoir
	if (!aArray["isFerie"]) {
		var x = aDispo.indexOf("W");
		var aSecondSlice = aDispo.splice(x+1,aDispo.length);
		aDispo = aDispo.splice(0,x).concat(aSecondSlice);
	}
	var sStartString = '<div id="sandbox"><ul>';
	var sMidString = "<li>&nbsp;</li>";
	for (sDispo in aDispo) {
		sMidString += "<li>"+aDispo[sDispo]+"</li>";
	}
	var sEndString = '</ul></div>';
	var sString = sStartString + sMidString + sEndString;
	return sString;
}
// Ajoute une dispo sDispo à l'objet oThis
function addDispo(oThis, sDispo)
{
	// Si sDispo est vide
	if (escape(sDispo) == "%A0")
	{
		sDispo = "";
	}
	// Màj de la page
	var sOldDispo = $(oThis).text();
	changeValue(oThis, sDispo);

	var aArray = infosFromId(oThis.id);

	// Si l'ancienne ou la nouvelle valeur nécessite un recalcul des dispos
	var aDispos = new Array(<?=$sDispos?>);
	for (var iInner in aDispos)
	{
		if (aDispos[iInner] == sDispo || aDispos[iInner] == sOldDispo)
		{
			decompteDispo(aArray["uid"], aArray["cycleId"]);
		}
	}

	// Si l'ancienne valeur était un 'V', on doit propager
	switch (sOldDispo) {
	       case "V":
		var aNewArray = new Array();
		// pour les V la valeur est répétée iX fois à gauche
		var iX = 2;
		var oTempThis = oThis;
		for (var i=1; i<=iX; i++) {
			var oSibling = $(oTempThis).prev();
			aNewArray = infosFromId(oSibling[0].id);
			if (!aNewArray) {
				break;
			}
			// Si on est sur le jour d'avant
			if ((parseInt(aArray["Day"]) - aNewArray["Day"]) <= iX && (parseInt(aArray["Day"]) - aNewArray["Day"]) > 0) {
				changeValue(oSibling[0], "");
			}
			oTempThis = oSibling[0];
		}
		// et à droite
		oTempThis = oThis;
		for (var i=1; i<=iX; i++) {
			var oSibling = $(oTempThis).next();
			aNewArray = infosFromId(oSibling[0].id);
			if (!aNewArray) {
				break;
			}
			//alert(aArray["Day"]+" - "+aNewArray["Day"]);
			// Si on est sur le jour d'avant
			if ((aNewArray["Day"] - parseInt(aArray["Day"])) <= iX && (aNewArray["Day"] - parseInt(aArray["Day"])) > 0) {
				changeValue(oSibling[0], "");
			}
			oTempThis = oSibling[0];
		}
		break;
		case "Rempla":
			$('#'+oThis.id).attr('title', '');
		break;
	}
	// Cas nécessitant des actions supplémentaires
	switch (sDispo) {
		case "V":
			var aNewArray = new Array();
			// pour les V la valeur est répétée iX fois à gauche
			var iX = 2;
			var oTempThis = oThis;
			for (var i=1; i<=iX; i++) {
				var oSibling = $(oTempThis).prev();
				aNewArray = infosFromId(oSibling[0].id);
				if (!aNewArray) {
					break;
				}
				// Si on est sur le jour d'avant
				//alert(aArray["Day"]+" - "+aNewArray["Day"]);
				if ((parseInt(aArray["Day"]) - aNewArray["Day"]) <= iX && (parseInt(aArray["Day"]) - aNewArray["Day"]) > 0) {
					changeValue(oSibling[0], sDispo);
				}
				oTempThis = oSibling[0];
			}
			// et à droite
			oTempThis = oThis;
			for (var i=1; i<=iX; i++) {
				var oSibling = $(oTempThis).next();
				aNewArray = infosFromId(oSibling[0].id);
				if (!aNewArray) {
					break;
				}
				//alert(aArray["Day"]+" - "+aNewArray["Day"]);
				// Si on est sur le jour d'avant
				if ((aNewArray["Day"] - parseInt(aArray["Day"])) <= iX && (aNewArray["Day"] - parseInt(aArray["Day"])) > 0) {
					changeValue(oSibling[0], sDispo);
				}
				oTempThis = oSibling[0];
			}
			break;
		case "Rempla":
			// Placement de la boîte de choix
			var p = $('#'+oThis.id).position();
			$("#dFormRemplacement").css({"left" : p.left + 10 , "top" : p.top + 20});
			$("#dFormRemplacement").show('slow');
			$("#remplaUid").val(aArray['uid']);
			$("#remplaYear").val(aArray['Year']);
			$("#remplaMonth").val(aArray['Month']);
			$("#remplaDay").val(aArray['Day']);
			break;
<?php
	if ($sActTitle != "") {
	print ($sActTitle);
?>
			var p = $('#'+oThis.id).position();
			$("#dFormInfoSup").css({"left" : p.left + 10 , "top" : p.top + 20, "position": "absolute"});
			$("#fFormInfoSup").show('slow');
			$("#infoSupUid").val(aArray['uid']);
			$("#infoSupYear").val(aArray['Year']);
			$("#infoSupMonth").val(aArray['Month']);
			$("#infoSupDay").val(aArray['Day']);
			break;
<?php
	}
?>
		default:
		break;
	}
	return true;
}
// Calcul une semaine d'heures
$(function() {
	$.post('ajax.php', {
		'q':'CH'
	})
	.done(function(data) {
		var oJSON = jQuery.parseJSON(data);
		traiteJSON(oJSON);
	});
});
// Modifie la valeur d'une case et effectue les actions s'y rapportant (recalcul notamment)
function changeValue(oThis, sDispo)
{
	var aArray = infosFromId(oThis.id);
	$.post('ajax.php', {
		'q':'CV',
		'oldDispo':escape($(oThis).text()),
		'dispo':escape(sDispo),
		'uid':escape(aArray['uid']),
		'Year':escape(aArray['Year']),
		'Month':escape(aArray['Month']),
		'Day':escape(aArray['Day']),
		'tk':$('#token').attr('title')
		})
		.done(function (data) {
			var oJSON = jQuery.parseJSON(data);
			traiteJSON(oJSON);
			$(oThis).text(sDispo); // FIXME si la requête n'est pas traitée, l'affichage de la case est quand même modifié
			$(oThis).addClass('emphasize');
			setTimeout(comptePresents(oThis.id), 300*oJSON.msg.length);
		});
}
<?php
if (array_key_exists('TEAMEDIT', $_SESSION)) {
	// Ces fonctions ne doivent pas être accessible à tous
	// Seuls les éditeurs peuvent (dé)protéger la grille
	// Effectue les actions nécessaires à la (dé)protection de la grille
	// Envoi d'une requête ajax
	// mise à jour des données de la grille (affichage...)
?>
// Effectue les actions pour la protection d'une case
function attribProtect(oThis)
{
	$('#'+oThis.id).unbind();
	$('#'+oThis.id).addClass('protected');
}
// Effectue les actions pour la déprotection d'une case
function attribUnprotect(oThis)
{
	$('#'+oThis.id).removeClass('protected');
	$('#'+oThis.id).one("click", dropDownAct(this));
}
<?php
}
?>

// Compte le nombre de présent pour la colonne à laquelle sId appartient
function comptePresents(sId)
{
	if (sId == undefined) { return false; }
	var aArray =sId.match(/.*(a\d+m\d+j\d+s\w+)/);
	if (aArray instanceof Array)
	{
		var sDate = aArray[1];
		var aAffDate = sId.match(/.*a(\d+)m(\d+)j(\d+)s(\w+)c(\d+)/);
		var sAffDate = aAffDate[3]+"/"+aAffDate[2]+"/"+aAffDate[1]+" en "+aAffDate[4];
	}
	else
	{
		return false;
	}
	<?php
	// Recherche les dispo qui correspondent à des absences pour les enlever du décompte des présents
	$sqlDispo = sprintf("SELECT `dispo` FROM `TBL_DISPO` WHERE `actif` = '1' AND `absence` = '1' ORDER BY `poids`");
	$sqlDispo = sprintf("
		SELECT `dispo`
		, 1 - `absence`
		, `type decompte`
		FROM `TBL_DISPO`
		WHERE `actif` IS TRUE
		AND (FIND_IN_SET('%s', `centres`) OR `centres` = 'all')
		AND (`team` = '%s' OR `team` = 'all')
		ORDER BY `poids`"
		, $_SESSION['utilisateur']->centre()
		, $_SESSION['utilisateur']->team()
	);
	$resDispo = $_SESSION['db']->db_interroge($sqlDispo);
	$absences = "";
	$dispo = "";
	while ($abs = $_SESSION['db']->db_fetch_row($resDispo)) {
		$absences .= sprintf("'%s':%u, ", $abs[0], $abs[1] + .5);
		if ($abs[2] == "dispo" && $abs[1] != .5) {
			$dispo .= sprintf("'%s':0, ", $abs[0]);
		} else {
			$dispo .= sprintf("'%s':%0.1f, ", $abs[0], $abs[1]);
		}
	}
	mysqli_free_result($resDispo);
	?>
	var aAbsent = {<?=substr($absences, 0, -2)?>};
	var aD = {<?=substr($dispo, 0, -2)?>};
	var aPresents = {'cds': 0, 'ce': 0, 'fmp' : 0, 'dtch': 0, 'pc': 0, 'inst': 0, 'theo': 0, 'c': 0, 'aD':0};
	var aListeUid = listeUid();
	for (var iUid in aListeUid)
	{
		var sTemp = '#u'+iUid+sDate;
		if (!$(sTemp).hasClass('absent'))
		{
			if ($(sTemp).text() == " " || $(sTemp).text() == "") {
				aPresents[aListeUid[iUid]]++;
				if (aListeUid[iUid] != 'c' && aListeUid[iUid] != 'theo') {
					aPresents['aD']++;
				}
			} else {
				aPresents[aListeUid[iUid]] += aAbsent[$(sTemp).text()] || 0;
				if (aListeUid[iUid] != 'c' && aListeUid[iUid] != 'theo') {
					aPresents['aD'] += aD[$(sTemp).text()] || 0;
				}
			}
		}
	}
	aPresents['pc'] += aPresents['ce'] + aPresents['cds'] + aPresents['inst'] + aPresents['dtch'] + aPresents['fmp'];
	aPresents['c'] += aPresents['theo'];
	$('#dec'+sDate).text(aPresents['aD']+"/"+aPresents['pc']+"/"+aPresents['c']);
	if (aPresents['pc'] < <?=get_sql_globals_constant('effectif_mini')?> && !$('#dec'+sDate).hasClass('protected'))
	{
		$('#dec'+sDate).addClass('emphasize');
		attention("Trop de personnels absents: "+sAffDate+".");
	}
	else
	{
		$('#dec'+sDate).removeClass('emphasize');
	}
}
<?php
// Décompte l'ensemble des présents de la grille
?>
function decomptePresents()
{
	var aDecompte = $('.dcpt');
	for (var iCpt=0; iCpt<aDecompte.length; iCpt++)
	{
		comptePresents(aDecompte[iCpt].id);
	}
}
<?php
	/*
	* Les paramètres sont numériques :
	* Id utilisateur et Id du cycle
 	*/
?>
function decompteDispo(iUid, iCycleId)
{
	if ((iUid == undefined) || (iCycleId == undefined))
	{
		return false;
	}
	var aDispos = new Array(<?=$sDispos?>);
	var aPresence=$('.presence[id^="u'+iUid+'a"]');
	var aConcerned=$('.presence[id^="u'+iUid+'a"][id$="c'+iCycleId+'"]');
	var sIniDecompte=$('#decDispou'+iUid+'c'+iCycleId).text();
	for (var iIndex=0; iIndex<aConcerned.length; iIndex++)
	{
		for (var iInner in aDispos)
		{
			if (aDispos[iInner] == $(aConcerned[iIndex]).text())
			{
				sIniDecompte++;
			}
		}
	}
	var iNextCycleId=parseInt(iCycleId)+1;
	$('*[id="decDispou'+iUid+'c'+iNextCycleId+'"]').text(sIniDecompte);
}
// Gestion des remplacements
$(function() {
	$('#dFormRemplacement').hide();
	$('#fFormRemplacement').submit(function(event) {
		event.preventDefault();
		$('#dFormRemplacement').hide('slow');
		$.post('ajax.php', {
			q:'RE',
			tk:$('#token').prop('title'),
			uid:$('#remplaUid').val(),
			Year:$('#remplaYear').val(),
			Month:$('#remplaMonth').val(),
			Day:$('#remplaDay').val(),
			nom:$('#remplaNom').val(),
			phone:$('#remplaPhone').val(),
			email:$('#remplaEmail').val()
		}).done(function(data) {
			var oJSON = jQuery.parseJSON(data);
			traiteJSON(oJSON);
		});
		event.preventDefault();
	});
});
<?php
// permet de retrouver les infos relatives à l'élément dont l'id est passé en paramètre
// retourne une Array avec tous ces éléments
?>
function infosFromId(sId) {
	var aId = sId.match(/u(\d+)a(\d+)m(\d+)j(\d+)s(\w+)c(\d+)/);
	if (aId instanceof Array) {
		var aArray = new Array();
		aArray["uid"] = aId[1];
		aArray["Year"] = aId[2];
		aArray["Month"] = aId[3];
		aArray["Day"] = aId[4];
		aArray["Vacation"] = aId[5];
		aArray["cycleId"] = aId[6];
		aArray["isFerie"] = $("#a"+aArray["Year"]+"m"+aArray["Month"]+"j"+aArray["Day"]+"s"+aArray["Vacation"]).hasClass('ferie');
		return aArray;
	}
	else
	{
		aId = sId.match(/decsepA(\d+)M(\d+)J(\d+)/);
		if (aId instanceof Array) {
			var aArray = new Array();
			aArray["Year"] = aId[1];
			aArray["Month"] = aId[2];
			aArray["Day"] = aId[3];
			return aArray;
		}
		else
		{
			aId = sId.match(/locka(\d+)m(\d+)j(\d+)c(\d+)/);
			if (aId instanceof Array) {
				var aArray = new Array();
				aArray["Year"] = aId[1];
				aArray["Month"] = aId[2];
				aArray["Day"] = aId[3];
				aArray["cycleId"] = aId[4];
				return aArray;
			}
			else
			{
				aId = sId.match(/deca(\d+)m(\d+)j(\d+)s(.+)c(\d+)/);
				if (aId instanceof Array) {
					var aArray = new Array();
					aArray["Year"] = aId[1];
					aArray["Month"] = aId[2];
					aArray["Day"] = aId[3];
					aArray["Vacation"] = aId[4];
					aArray["cycleId"] = aId[5];
					return aArray;
				}
				else
				{
					aId = sId.match(/a(\d+)m(\d+)j(\d+)s(.+)/);
					if (aId instanceof Array) {
						var aArray = new Array();
						aArray["Year"] = aId[1];
						aArray["Month"] = aId[2];
						aArray["Day"] = aId[3];
						aArray["Vacation"] = aId[4];
						return aArray;
					}
					else
					{
						aId = sId.match(/confa(\d+)m(\d+)j(\d+)/);
						if (aId instanceof Array) {
							var aArray = new Array();
							aArray["Year"] = aId[1];
							aArray["Month"] = aId[2];
							aArray["Day"] = aId[3];
							return aArray;
						}
						else
						{
							return false;
						}
					}
				}
			}
		}
	}
}
<?php
// Retourne un tableau avec les uid présents sur le tableau
?>
function listeUid() {
	var aClasses = new Array('cds', 'ce', 'pc', 'inst', 'theo', 'c', 'dtch', 'fmp');
	var aEffectif = new Array();
	var aClass = new Array();
	for (var iClass in aClasses)
	{
		aClass = $('.'+aClasses[iClass]);
		for (var iCpt=0; iCpt<aClass.length; iCpt++)
		{
			var sId = aClass[iCpt].id;
			var aId = sId.match(/u(\d+)$/);
			if (aId instanceof Array)
			{
				aEffectif[aId[1]] = aClasses[iClass];
			}
			else
			{
			}
		}
	}
	return aEffectif;
}
<?php
//****************
// Initialisation
//****************
?>
$(function() {
<?php
	// teamEdit peut modifier toutes les lignes
	if ($_SESSION['utilisateur']->hasRole('teamEdit')) { ?>
	$('.presence').one('click', function() { dropDownAct(this); });
<?php	// Les utilisateurs standards ne peuvent modifier que leur ligne
	} else {
		printf("\$('.presence[id*=u%sa]').one('click', function() { dropDownAct(this); });", $_SESSION['utilisateur']->uid());
	}
		/* Supprime le menu pour modifier les dispo sur les cases protected */
?>		$('.protected').unbind();
		decomptePresents();
		$('.nom').one('click', function() { collapseRow(this); });
<?php
// Masque le texte des cases dtch
if ($_SESSION['utilisateur']->getPref('dtch') === 1) {
	?>
	$("td:contains('dtch')").text('');
<?php
}
?>
});
<?php
	// Gestion de la configuration des cycles (E/W)
	// TODO interdire l'édition lorsque la grille n'est pas éditable.
	if (!empty($_SESSION['TEAMEDIT'])) {
?>
$(function() {
	$('.conf').click(function() {
		var a = {'W':'E','E':'W'};
		var id = this.id;
		var text = $(this).text();
		$.post('ajax.php', {q:'CF',tk:$('#token').prop('title'),id:id,conf:a[text]})
		.done(function(data) {
			var oJSON = jQuery.parseJSON(data);
			if (oJSON.ok) $('#'+id).contents().replaceWith(a[text]);
			traiteJSON(oJSON);
		});
	});
});
<?php
	}

//*******************
// Fonctions annexes
//*******************
// Masque les rangs nom sauf l'id sId
?>
function collapseRow(sId) {
	var oParent = $(sId).parent();
	var sRow = "#"+oParent[0].id;
	var aSiblings = $(sRow).siblings();
	var sTest = "";
	for (var i=0; i<aSiblings.length - 1; i++) {
		var sTempId = "#" + aSiblings[i].id;
		if (sId != sTempId) {
			$(sTempId).hide();
		}
	}
	$(sId).one('click', function() { uncollapseRow(this); });
}
<?php
// Montre le rang qui contient l'id sId
?>
function uncollapseRow(sId) {
	var oParent = $(sId).parent();
	var sRow = oParent[0];
	var aSiblings = $(sRow).siblings();
	for (var i=0; i<aSiblings.length; i++) {
		var sTempId = "#" + aSiblings[i].id;
		if (sId != sTempId) {
			$(sTempId).show();
		}
	}
	$(sId).one('click', function() { collapseRow(this); });
}
// Les actions à effectuer lorsqu'une case est cliquée
function dropDownAct(oThis)
{
	// l'élément cliqué est mis en valeur
	$(oThis).addClass("emphasize");
	// La première case (contenant le nom) est mise en évidence
	var oParent = $(oThis).parent();
	var oChildren = $(oParent[0]).children();
	$(oChildren[0]).addClass("emphasize");
	// Recherche des informations de l'élément afin de déterminer la tête de colonne correspondante
	var aArray = infosFromId(oThis.id);
	var sVacationId = "#a"+aArray["Year"]+"m"+aArray["Month"]+"j"+aArray["Day"]+"s"+aArray["Vacation"];
	// la tête de colonne est mise en évidence
	$(sVacationId).addClass("emphasize");
	// Recherche des possibles occupations pour remplir la boite de choix
	var sString = getAvailableOccupations(oThis);
	$("#sandbox").replaceWith(sString);
	// Placement de la boite de choix
	var p = $(oThis).position();
	$("#sandbox").css({"left" : p.left + 10 , "top" : p.top + 20});
	$("#sandbox").show('slow');
	// Action du clic dans la boite de choix
	$("#sandbox li").click(function() {
		$("#sandbox").hide('slow');
		addDispo(oThis, $(this).text());
		$("td.nom").removeClass("emphasize");
		$(sVacationId).removeClass("emphasize");
		$(oThis).one('click', function() { dropDownAct(this); });
	});
	$(oThis).one('click', function() { liftUpAct(this); });
}
// actions à effectuer lorsqu'une case est à nouveau cliquée
function liftUpAct(oThis)
{
	$("td.nom").removeClass("emphasize");
	// Recherche des informations de l'élément afin de déterminer la tête de colonne correspondante
	var aArray = infosFromId(oThis.id);
	var sVacationId = "#a"+aArray["Year"]+"m"+aArray["Month"]+"j"+aArray["Day"]+"s"+aArray["Vacation"];
	$(sVacationId).removeClass("emphasize");
	$('#sandbox').hide('slow');
	$(oThis).one('click', function() { dropDownAct(this); });
}

<?php
ob_end_flush();
?>
