// Affiche un message à l'écran
function attention(sMessage)
{
	$('#info').text(sMessage)
		.fadeIn('slow')
		.fadeOut(sMessage.length*100);
}
/*
 * Applique les actions communes à toutes les réponses ajax
 * rafraichit le token et affiche un message si la réponse ajax contient un message
 */
function traiteJSON(oJSON)
{
	if (oJSON) {
		// Mise à jour du token
		$('#token').prop('title', oJSON.tk);
		$('.embedToken').prop('value', oJSON.tk);
		if (oJSON.msg.length > 0) {
			attention(oJSON.msg);
		}
	}
}
function newPhone()
{
	var objFieldset = document.createElement('fieldset');
	var objLegend = document.createElement('legend');
	var objTextLegend = document.createTextNode('Nouveau téléphone');
	objLegend.appendChild(objTextLegend);
	objFieldset.appendChild(objLegend);

	var objUl = document.createElement('ul');
	var objLiNb = document.createElement('li');
	var objLiDesc = document.createElement('li');
	var objLiPal = document.createElement('li');

	var objLabelNb = document.createElement('label');
	var objTextLabelNb  = document.createTextNode('Numéro : ');
	objLabelNb.setAttribute('for', 'newnb');
	objLabelNb.appendChild(objTextLabelNb);

	var objInputNb = document.createElement('input');
	objInputNb.setAttribute('name', 'newnb');
	objInputNb.setAttribute('type', 'tel');
	objInputNb.setAttribute('pattern', '\\d\\d\\d\\d\\d\\d\\d\\d\\d\\d');
	objInputNb.setAttribute('placeholder', '0011223344');
	
	objLiNb.appendChild(objLabelNb);
	objLiNb.appendChild(objInputNb);


	var objLabelDesc = document.createElement('label');
	var objTextLabelDesc  = document.createTextNode('Description : ');
	objLabelDesc.setAttribute('for', 'newdesc');
	objLabelDesc.appendChild(objTextLabelDesc);
	
	var objInputDesc = document.createElement('input');
	objInputDesc.setAttribute('name', 'newdesc');
	objInputDesc.setAttribute('type', 'text');
	objInputDesc.setAttribute('placeholder', 'maison');
	
	objLiDesc.appendChild(objLabelDesc);
	objLiDesc.appendChild(objInputDesc);
	

	var objLabelPal = document.createElement('label');
	var objTextLabelPal  = document.createTextNode('Principal : ');
	objLabelPal.setAttribute('for', 'newpal');
	objLabelPal.appendChild(objTextLabelPal);
	
	var objInputPal = document.createElement('input');
	objInputPal.setAttribute('name', 'newpal');
	objInputPal.setAttribute('type', 'checkbox');
	
	objLiPal.appendChild(objLabelPal);
	objLiPal.appendChild(objInputPal);


	objUl.appendChild(objLiNb);
	objUl.appendChild(objLiDesc);
	objUl.appendChild(objLiPal);

	objFieldset.appendChild(objUl);

	$('#iAddPhone').parent().before(objFieldset);
	$('#iAddPhone').parent().hide();
}
function newAddress()
{
	var objFieldset = document.createElement('fieldset');
	var objLegend = document.createElement('legend');
	var objTextLegend = document.createTextNode('Nouvelle adresse');
	objLegend.appendChild(objTextLegend);
	objFieldset.appendChild(objLegend);

	var objUl = document.createElement('ul');
	var objLiNb = document.createElement('li');
	var objLiDesc = document.createElement('li');
	var objLiPal = document.createElement('li');

	var objLabelNb = document.createElement('label');
	var objTextLabelNb  = document.createTextNode('Adresse : ');
	objLabelNb.setAttribute('for', 'newadresse');
	objLabelNb.appendChild(objTextLabelNb);

	var objInputNb = document.createElement('textarea');
	objInputNb.setAttribute('name', 'newadresse');
	
	objLiNb.appendChild(objLabelNb);
	objLiNb.appendChild(objInputNb);
	

	var objLabelDesc = document.createElement('label');
	var objTextLabelDesc  = document.createTextNode('Code postal : ');
	objLabelDesc.setAttribute('for', 'newcp');
	objLabelDesc.appendChild(objTextLabelDesc);
	
	var objInputDesc = document.createElement('input');
	objInputDesc.setAttribute('name', 'newcp');
	objInputDesc.setAttribute('type', 'text');
	
	objLiDesc.appendChild(objLabelDesc);
	objLiDesc.appendChild(objInputDesc);
	

	var objLabelPal = document.createElement('label');
	var objTextLabelPal  = document.createTextNode('Ville : ');
	objLabelPal.setAttribute('for', 'newville');
	objLabelPal.appendChild(objTextLabelPal);
	
	var objInputPal = document.createElement('input');
	objInputPal.setAttribute('name', 'newville');
	objInputPal.setAttribute('type', 'text');
	
	objLiPal.appendChild(objLabelPal);
	objLiPal.appendChild(objInputPal);

	
	objLiPal.appendChild(objLabelPal);
	objLiPal.appendChild(objInputPal);


	objUl.appendChild(objLiNb);
	objUl.appendChild(objLiDesc);
	objUl.appendChild(objLiPal);

	objFieldset.appendChild(objUl);

	$('#iAddAddress').parent().before(objFieldset);
	$('#iAddAddress').parent().hide();
}
function supprInfo(q, id, uid)
{
	$.post('ajax.php', {q:'SU',r:q,id:id,uid:uid,tk:$('#token').prop('title')})
		.done(function(data) {
		$('#'+q+id).hide('slow');
		var oJSON = jQuery.parseJSON(data);
		traiteJSON(oJSON);
	});
}
/*
 * Returns true if the content of both fields'content
 * whose ids are given are equal
 */
function checkSameContent(sId1, sId2)
{
	return $('#'+sId1).value() == $('#'+sId2).value();
}
function checkPwdComplexity(sPwd)
{
}
function checkLoginExists(sLogin)
{
}
function validateCrtAcct() {
	if (!checkSameContent('pwd', 'pwdchk')) {
		alert("Les mots de passe ne correspondent pas...")
		return false;
	}
	return true;
}
/*
 * Ajoute de l'info à des champs dont l'id peut être recréé à partir de la date et l'uid
 * par exemple dans des cases de la grille
 */
function addTextToGrille(aArray,info)
{
	var sId = 'u'+aArray["uid"]+'a'+aArray["Year"]+'m'+aArray["Month"]+'j'+aArray["Day"];
	$("<p>"+info+"</p>").appendTo($("*[id*="+sId+"]"));
}
/*
 * Affiche les heures attribuées aux utilisateurs sur chaque jour de travail
 */
function addHeures()
{
	$(".presence").css('width', '5em');
	$("th[id^='a']").each(
			function()
			{
				var aArray = infosFromId($(this).attr("id"));
				if (aArray instanceof Array) {
					$.post('ajax.php', {q:"GH",d:aArray['Day'],m:aArray['Month'],y:aArray['Year'],tk:$('#token').prop('title')}).done(function(data) {
						var oJSON = jQuery.parseJSON(data);
						traiteJSON(oJSON);
						var aArr = oJSON.heures;
						var theDate = new Date(aArr["date"]);
						var month = theDate.getMonth() + 1;
						var sIdH = 'deca'+theDate.getFullYear()+'m'+month+'j'+theDate.getDate()+"s";
						$("<ul class='heures' id='hto"+aArr["date"]+"'></ul>").appendTo($("td[id^="+sIdH+"]"));
						for (var i in aArr)
						{
							for (var s in aArr[i])
							{
								if (aArr[i][s]["uid"] == 0) {
									$("<li title='heures à partager "+i+"' class='hp'>"+i+" : "+aArr[i][s]['normales']+"</li>").prependTo($("ul[id=hto"+aArr["date"]+"]"));
								} else if (aArr[i][s]["uid"] == -1) {
									$("<li title='total des heures partagées "+i+"' class='thp'>"+i+" : "+aArr[i][s]['normales']+"</li>").appendTo($("ul[id=hto"+aArr["date"]+"]"));
								} else {
									var sId = 'u'+aArr[i][s]["uid"]+'a'+theDate.getFullYear()+'m'+month+'j'+theDate.getDate()+"s";
									$("<ul class='heures'><li>"+i+"</li><li title='heures normales "+i+"' class='hn'>"+aArr[i][s]['normales']+"</li><li title='heures instruction "+i+"' class='hi'>"+aArr[i][s]['instruction']+"</li></ul>").appendTo($("td[id^="+sId+"]"));
								}
							}
						}
					});
				}
			}
	);
	$('#bHeures').detach();
}
/*
 * Gère les boutons pour fermer l'élément parent
 */
function buttonClose() {
	$('.button-close').click(function() { $(this).parent().remove(); });
}
/*
 * crée un formulaire de saisie d'évènement calendrier
 */
function addFormCalEvent(id) {
	$('#formCalEvent').remove();
	$('*[id^=button-dBriefing]').show();
	$('div[id^=dBriefing]').hide();
	$('#dBriefing'+id).show();
	var objForm = document.createElement('form');
	objForm.setAttribute('name', 'formCalEvent');
	objForm.setAttribute('id', 'formCalEvent');
	objForm.setAttribute('class', 'ng fGestionCalendrier');
	objForm.setAttribute('method', 'post');
	objForm.setAttribute('action', '#');

	var objClose = document.createElement('button');
	objClose.setAttribute('class', 'button-close');
	objClose.setAttribute('id', 'bcForm');

	var objUlGlob = document.createElement('ul');
	var objLi1 = document.createElement('li');
	var objLi2 = document.createElement('li');
	var objLi3 = document.createElement('li');

	var objTextarea = document.createElement('textarea');
	objTextarea.setAttribute('name', 'description');
	objTextarea.setAttribute('id', 'description');
	objTextarea.setAttribute('placeholder', "Description de l'évènement");

	var objFieldset = document.createElement('fieldset');

	var objUlSub = document.createElement('ul');
	var objLiS1 = document.createElement('li');
	var objLiS2 = document.createElement('li');

	var objDateD = document.createElement('input');
	objDateD.setAttribute('type', 'date');
	objDateD.setAttribute('name', 'dateD');
	objDateD.setAttribute('id', 'dateD');
	objDateD.setAttribute('placeholder', "Début");

	var objDateF = document.createElement('input');
	objDateF.setAttribute('type', 'date');
	objDateF.setAttribute('name', 'dateF');
	objDateF.setAttribute('id', 'dateF');
	objDateF.setAttribute('placeholder', "Fin");

	var objDiv = document.createElement('div');
	var objUlAffect = document.createElement('ul');
	objUlAffect.setAttribute('id', 'affect');

	var objSubmit = document.createElement('input');
	objSubmit.setAttribute('type', 'submit');
	objSubmit.setAttribute('value', 'Envoyer');

	var objHidden = document.createElement('input');
	objHidden.setAttribute('type', 'hidden');
	objHidden.setAttribute('name', 'type');
	objHidden.setAttribute('value', id);

	objLi1.appendChild(objTextarea);
	objLiS1.appendChild(objDateD);
	objLiS2.appendChild(objDateF);
	objUlSub.appendChild(objLiS1);
	objUlSub.appendChild(objLiS2);
	objFieldset.appendChild(objUlSub);
	objLi2.appendChild(objFieldset);
	objLi3.appendChild(objSubmit);
	objUlGlob.appendChild(objLi1);
	objUlGlob.appendChild(objLi2);
	objUlGlob.appendChild(objLi3);
	objForm.appendChild(objClose);
	objForm.appendChild(objUlGlob);
	objForm.appendChild(objHidden);
	var objDiv = document.getElementById('dBriefing' + id);
	objDiv.appendChild(objForm);
	$('#formCalEvent').hide();
	$('#button-dBriefing'+id+'-addForm').hide(500);
	$('#formCalEvent').show(200);
	intervalDate();
	$('#bcForm').click(function() {
		$(this).parent().remove();
		$('#button-dBriefing'+id+'-addForm').show(500);
	});
	// Recherche des affectations existantes
	$.post('ajax.php', {q:"LA",t:"centre"}).done(function(x) {
		var aArr = JSON.parse(x);
		for (type in aArr) {
			var objUlListe = document.createElement('ul');
			objUlListe.setAttribute('id', type+'-liste');
			for (aff in aArr[type]) {
				var aArray = aArr[type][aff];
				var objLi = document.createElement('li');
				objLi.setAttribute('class', "type-"+type);
				objLi.setAttribute('title', aArray['description']);
				var objLabel = document.createElement('label');
				objLabel.setAttribute('for', aArray['nom']);
				objLabel.appendChild(document.createTextNode(aArray['nom']));
				var objInput = document.createElement('input');
				objInput.setAttribute('type', 'checkbox');
				objInput.setAttribute('name', type+'["'+aArray['nom']+'"]');
				objLi.appendChild(objLabel);
				objLi.appendChild(objInput);
				objUlListe.appendChild(objLi);
			}
			var objLiType = document.createElement('li');
			var objH3 = document.createElement('h3');
			objH3.appendChild(document.createTextNode('Choisissez les '+type+'s concernés'));
			objLiType.appendChild(objH3);
			objLiType.appendChild(objUlListe);
			$('<li><label for="all">All</label><input type="checkbox" name="centre[\"all\"]" /></li>').prependTo(objUlListe);
			$('#formCalEvent > ul').append(objLiType);
		}
	});
}
function intervalDate() {
	$('#dateD').datepicker({
		defaultDate:0,
		changeMonth:true,
		onSelect:function(selectedDate){
		$("#dateF").datepicker("option","minDate",selectedDate);}
		});
	$('#dateF').datepicker({
		defaultDate:12,
		changeMonth:true,
		onSelect:function(selectedDate){
			$("#dateD").datepicker("option","maxDate",selectedDate);
			}
		});
	$('#dateM').datepicker({
		defaultDate:12,
		changeMonth:true,
		});
	$.datepicker.setDefaults($.datepicker.regional['fr']);
}
$(function() {
	buttonClose();
	intervalDate();
	$('#alerte').click(function() { $(this).remove(); });
	/*
	 * Modifie les formulaires qui seront traités en ajax
	 * et cache le formulaire après soumission si un champ
	 * du formulaire s'appellant cachemoi existe.
	 *
	 * La destination du formulaire est utilisée comme destination de la requête ajax
	 */
	$("form").has("input[name='ajax'][value='true']")
		.prepend('<input type="hidden" name="tk" class="embedToken" value="'+$('#token').prop('title')+'" />')
		.submit(function(event) {
			event.preventDefault();
			var form = $(this);
			url = $(this).attr('action');
			if ($(form).find("input[name='cachemoi']")) {
				$(form).hide('slow');
			}
			$("#container").addClass('waiting');
			$.post(url, form.serialize())
				.done(function (data) {
					var oJSON = jQuery.parseJSON(data);
					traiteJSON(oJSON);
					$("#container").removeClass('waiting');
				});
		});
	// Formulaire de création de compte TeamTime ou récupération de mot de passe fcrtAcct
	$("#fcrtAcct").attr('onsubmit', 'return validateCrtAcct()');
	// Crée un champ hidden nommé w et de valeur le nom du formulaire FIXME deprecated?
	//$("form").prepend("<input type='hidden' name='w' value='" + this.name + "' />");
	$("td[id^='affectation']").addClass('clic');
	$("td[id^='affectation']").one('click', function(){
		//$(this).children().remove();
		$(this).load("ajax.php", {q:'FT',t:'centre',uid:$(this).attr('id').substr(11),tk:$('#token').prop('title')});
		$(this).append($(this).attr('id').substr(11));
		intervalDate();
	});
});
