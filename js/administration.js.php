<?php
/*
 * administration.js.php
 */

// Require authenticated user
// L'utilisateur doit être logué/admin pour accéder à cette page
$requireAuthenticatedUser = true;

header('Content-Type: application/javascript');
//ob_start();
$conf['page']['elements']['firePHP'] = true;
$conf['page']['include']['constantes'] = 1;
$conf['page']['include']['globalConfig'] = 1;
$conf['page']['include']['globals_db'] = 1;
$conf['page']['include']['init'] = 1;
$conf['page']['include']['session'] = 1;
require_once('../required_files.inc.php');

firePHPLog($_SESSION, 'SESSION');
if ($_SESSION['utilisateur']->hasRole('admin')) {
	// Création des champs pour l'affectation
?>
$(function() {
	var objLabelCentre = document.createElement('label');
	objLabelCentre.setAttribute('for', 'Centre');
	objLabelCentre.appendChild(document.createTextNode('Centre'));
	var objTdLabelCentre = document.createElement('td');
	objTdLabelCentre.appendChild(objLabelCentre);

	var objInputCentre = document.createElement('input');
	objInputCentre.setAttribute('type', 'text');
	objInputCentre.setAttribute('name', 'centre');
	objInputCentre.setAttribute('id', 'iCccentre');
	var objTdInputCentre = document.createElement('td');
	objTdInputCentre.appendChild(objInputCentre);
	var objFirstRow = document.createElement('tr');
	objFirstRow.appendChild(objTdLabelCentre);
	objFirstRow.appendChild(objTdInputCentre);

	var objLabelTeam = document.createElement('label');
	objLabelTeam.setAttribute('for', 'Team');
	objLabelTeam.appendChild(document.createTextNode('Team'));
	var objTdLabelTeam = document.createElement('td');
	objTdLabelTeam.appendChild(objLabelTeam);

	var objInputTeam = document.createElement('input');
	objInputTeam.setAttribute('type', 'text');
	objInputTeam.setAttribute('name', 'team');
	objInputTeam.setAttribute('id', 'iCcteam');
	var objTdInputTeam = document.createElement('td');
	objTdInputTeam.appendChild(objInputTeam);
	var objSecondRow = document.createElement('tr');
	objSecondRow.appendChild(objTdLabelTeam);
	objSecondRow.appendChild(objTdInputTeam);

	$('#iCcemail').parent().parent().after(objSecondRow);
	$('#iCcemail').parent().parent().after(objFirstRow);
});
<?php
}
?>
function subCc()
{
	$.post('ajax.php', serialize(document.forms.fCc))
		.done(function(data) {
			var oJSON = jQuery.parseJSON(data);
			traiteJSON(oJSON);
	});
}

//
// Recherche les utilisateurs dont le nom commence par les lettres passées (au moins deux lettres)
//
function showUsers(field)
{
	$('#affectation').hide();
	$('#affectpale').hide();
	$('#lCredentials').hide();
	$('#submitContact1').hide();
	$('#administration').hide();
	$('#prenom').val("");
	$('#email').val("");
	$('input[name="uid"]').val("");
	$('#login').val("");
<?php
	if ($_SESSION['utilisateur']->hasRole('admin')) {
		?>
	$('#submitContact2').hide();
<?php
	}
?>
	if (field.value.length >= 2)
	{
		$.post('ajax.php', { q:"LU",nom:field.value,tk:$('#token').prop('title') })
			.done(function(data) {
				var oJSON = jQuery.parseJSON(data);
				traiteJSON(oJSON);
				$('#iANU').remove();
				$('#affectation').hide();
				$('tr[id^="affectation"]').remove();
				$('#dropdownbox').after('<button name="iANU" id="iANU" class="bouton" onclick="return ANU();">Ajouter un nouvel utilisateur</button>');
				$('#lUid').hide();
				$('#lPrenom').hide();
				$('#lEmail').hide();
				$('#lAffectation').hide();
				$('#lCredentials').hide();
				$('#dropdownbox').hide();
				if (data.length > 0) {
					$('#dropdownbox > ul').remove();
					$('#dropdownbox').append('<ul style="width:190px;max-height:200px;overflow-y:auto;margin-left:120px;padding:3px;background-color:rgba(255,255,255,.8);z-index:800;position:absolute;"></ul>');
					$.each(oJSON.users, function(i, user) {
						$('#dropdownbox > ul').append('<li onclick="fillUser('+user.uid+')" style="border:none;margin:0;padding:0;cursor:pointer;">'+user.nom+' '+user.prenom+'</li>');
					});
					$('#dropdownbox').show('slow');
				}
				else
				{
					$('#dropdownbox').hide('slow');
				}
			});
	} else {
		$('#dropdownbox').hide();
	}
}
//
// Remplit les champs avec les infos de l'utilisateur
//
function fillUser(uid)
{
	$.post('ajax.php', {q: "FU",uid: uid,tk:$('#token').prop('title')})
		.done(function(data) {
			var oJSON = jQuery.parseJSON(data);
			traiteJSON(oJSON);
			$('#iANU').remove();
			$('#dropdownbox').hide('slow');
			$('li').show('slow');
			$('#affectation').show('slow');
			$('#nom').val(oJSON.user.nom);
			$('#prenom').val(oJSON.user.prenom);
			$('#email').val(oJSON.user.email);
			$('input[name="uid"]').val(oJSON.user.uid);
			$('#login').val(oJSON.user.login);
			$('#centre').val(oJSON.user.centre);
			$('#team').val(oJSON.user.team);
			$('#grade').val(oJSON.user.grade);
			// Affiche les affectations de l'utilisateur
			for (var i=0;i < oJSON.user.affectations.length; i++)
			{
				if (oJSON.user.affectations[i].principale) {
					$('#captabpal tbody').append("<tr id='affectation"+oJSON.user.affectations[i].aid+"' class='pale' title='Poids : "+oJSON.user.affectations[i].poids+"'><td>"+$('option[value="'+oJSON.user.affectations[i].centre+'"]').text()+"</td><td>"+$('option[value="'+oJSON.user.affectations[i].team+'"]').text()+"</td><td>"+oJSON.user.affectations[i].beginning+"</td><td>"+oJSON.user.affectations[i].end+"</td><td><div class='imgwrapper12' title="+'"Supprimer l'+"'"+'entrée" onclick="supprInfo('+"'affectation', "+oJSON.user.affectations[i].aid+", "+oJSON.user.uid+')" style="left:5px;cursor:pointer;"><img class="cnl" src="themes/<?=$conf['theme']['current']?>/images/glue.png" alt="supprimer" /></div></td></tr>');
				} else {
					$('#catab tbody').append("<tr id='affectation"+oJSON.user.affectations[i].aid+"'><td>"+$('option[value="'+oJSON.user.affectations[i].centre+'"]').text()+"</td><td>"+$('option[value="'+oJSON.user.affectations[i].team+'"]').text()+"</td><td>"+$('option[value="'+oJSON.user.affectations[i].grade+'"]').text()+"</td><td>"+oJSON.user.affectations[i].beginning+"</td><td>"+oJSON.user.affectations[i].end+"</td><td>"+oJSON.user.affectations[i].poids+"</td><td><div class='imgwrapper12' title="+'"Supprimer l'+"'"+'entrée" onclick="supprInfo('+"'affectation', "+oJSON.user.affectations[i].aid+", "+oJSON.user.uid+')" style="left:5px;cursor:pointer;"><img class="cnl" src="themes/<?=$conf['theme']['current']?>/images/glue.png" alt="supprimer" /></div></td></tr>');
}
			}
			$('#affectation').show();
			$('#affectpale').show();
		});
}
//
// Crée les champs pour la création d'un nouvel utilisateur
//
function ANU()
{
	$('#dropdownbox').hide('slow');
	$('#lPrenom').show();
	$('#lEmail').show();
	$('button').remove();
	$('#lCredentials').show();
	$('#submitContact1').show();
	$('#administration').show();
	$('#affectation').show();
<?php
	if ($_SESSION['utilisateur']->hasRole('admin')) {
		?>
	$('#submitContact2').show();
<?php
	}
?>
	return false;
}
function increment(id, pas)
{
document.getElementById(id).value = parseFloat(document.getElementById(id).value) + parseFloat(pas);
}
// Ajouter une valeur d'un champ à un autre champ
function displayVals(v)
{
	var mul = $('#'+v).val() || [];
	$('#'+v+'p').val(mul.join(","));
}
// Édition d'une activité
function edAct(id)
{
	$('#act'+id).children().css('background-color', 'red');
}
// Formulaire d'ajout d'activité
// masquage et affichage des champs pour les compteurs
$(function() {
	$('#namCpt').hide();
	$('#lPond').hide();
	$('#needCpt').on("change", (function() {
		if (this.checked == false) {
			elem = document.getElementById('dp');
			elem.value = "";
			$('#namCpt').hide();
		} else {
			$('#namCpt').show();
		}
	}));
	$('#isd').on("change", (function() {
		if (this.checked == true) {
			elem = document.getElementById('needCpt');
			elem.checked = false;
			elem = document.getElementById('dp');
			elem.value = "";
			$('#namCpt').hide();
			$('#neeCpt').hide();
			$('#lPond').show();
		} else {
			$('#neeCpt').show();
			$('#lPond').hide();
		}
	}));
	$('#compteur').on("change", (function() {
		$('#dp').val(this.value);
	}));
	$('#dp').on('focus', (function() {
		this.value = "";
	}));
	$(".supDid").on('click', function() {
		$(this).prepend('<input type="hidden" name="tk" class="embedToken" value="'+$('#token').prop('title')+'" />')
		$.post('ajax.php', $(this).serialize())
			.done(function (data) {
				var oJSON = jQuery.parseJSON(data);
				traiteJSON(oJSON);
				$('#'+oJSON.id).detach();
		});
	});
});
// Year Toggle
$(function() {
	$("td[id^='yt_']").click(function() {
		var aId = this.id.match(/yt_u(\d+)d(\d+-\d+-\d+)/);
		$.post('ajax.php', {q:"YT",d:aId[2],uid:aId[1]}).done(function(data) {
			var oJSON = jQuery.parseJSON(data);
			traiteJSON(oJSON);
		});
	});
});
// Suppression des péréq
$(function() {
	$("td[id^='sp_']").click(function() {
		var aId = this.id.match(/sp_(\d+)/);
		$.post('ajax.php', {q:"SP",sdid:aId[1]}).done(function(data) {
			var oJSON = jQuery.parseJSON(data);
			traiteJSON(oJSON);
			$('#sdid'+aId[1]).detach();
		});
	});
});

function hideTableaux()
{
	$('.tabCong').hide();
}
function showTableau(sId)
{
	$('#'+sId).show('slow');
}
function desemphasize()
{
	$('li[id^="chcong"]').removeClass('selected');
}
$(function() {
		hideTableaux();
		// On retire le lien du menu pour le gérer avec jquery
		$('li[id^="chcong"]').children('a').replaceWith(function() { return $(this).contents(); });
		$('li[id^="chcong"]').click(function() {
			var aId = this.id.match(/chcong(\d+)/);
			hideTableaux();
			desemphasize();
			$(this).addClass('selected');
			$('#d_'+aId[1]).show();
			});
		// On retire les liens sur les dates de congés dans les tableaux pour les gérer avec jquery
		$('td.date').children('a').replaceWith(function() { return $(this).contents(); });
		<?php
		if (!empty($_SESSION['utilisateur']->hasRole('teamEdit'))) { ?>
		$('#datePicker').datepicker($.datepicker.regional['fr']);
		$('td.filed').addClass('pointer');
		$('td.filed').click(function() {
			$(this).addClass('valide');
			$(this).removeClass('pointer');
			$(this).removeClass('filed');
			$.post('ajax.php', {q:'CG',tk:$('#token').prop('title'),f:2,id:this.id})
				.done(function(data) {
				var oJSON = jQuery.parseJSON(data);
				traiteJSON(oJSON);
			});
		});
		<?php
		}
		?>
});
<?php
// ob_end_flush();
?>
