<?php
// install.php
//
// Script d'installation de TeamTime
// Concrètement, il crée les utilisateurs de la base de données
// nécessaires au bon fonctionnement de TeamTime

/*
	TeamTime is a software to manage people working in team on a cyclic shift.
	Copyright (C) 2012 Manioul - webmaster@teamtime.me

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Require authenticated user
// L'utilisateur doit être logué pour accéder à cette page
// $requireAuthenticatedUser = true;

ob_start(); // Obligatoire pour firePHP

/*
 * Configuration de la page
 * Définition des include nécessaires
 */
	$conf['page']['include']['constantes'] = 1; // Ce script nécessite la définition des constantes
	$conf['page']['include']['errors'] = 1; // le script gère les erreurs avec errors.inc.php
	$conf['page']['include']['class_debug'] = 1; // La classe debug est nécessaire à ce script
	$conf['page']['include']['globalConfig'] = 1; // Ce script nécessite config.inc.php
	$conf['page']['include']['init'] = 1; // la session est initialisée par init.inc.php
	$conf['page']['include']['globals_db'] = 1; // Le DSN de la connexion bdd est stockée dans globals_db.inc.php
	$conf['page']['include']['class_db'] = 1; // Le script utilise class_db.inc.php
	$conf['page']['include']['session'] = 1; // Le script utilise les sessions par session.inc
	$conf['page']['include']['classUtilisateur'] = NULL; // Le sript utilise uniquement la classe utilisateur (auquel cas, le fichier class_utilisateur.inc.php
	$conf['page']['include']['class_utilisateurGrille'] = NULL; // Le sript utilise la classe utilisateurGrille
	$conf['page']['include']['class_cycle'] = NULL; // La classe cycle est nécessaire à ce script (remplace grille.inc.php
	$conf['page']['include']['class_menu'] = NULL; // La classe menu est nécessaire à ce script
	$conf['page']['include']['smarty'] = NULL; // Smarty sera utilisé sur cette page
	$conf['page']['compact'] = false; // Compactage des scripts javascript et css
	$conf['page']['include']['bibliothequeMaintenance'] = false; // La bibliothèque des fonctions de maintenance est nécessaire
/*
 * Fin de la définition des include
 */


/*
 * Configuration de la page
 */
        $conf['page']['titre'] = sprintf("Installation des utilisateurs de la base de données"); // Le titre de la page
// Définit la valeur de $DEBUG pour le script
// on peut activer le debug sur des parties de script et/ou sur certains scripts :
// $DEBUG peut être activer dans certains scripts de required et désactivé dans d'autres
	$DEBUG = false;

	/*
	 * Choix des éléments à afficher
	 */
	
	// Affichage du menu horizontal
	$conf['page']['elements']['menuHorizontal'] = false;
	// Affichage messages
	$conf['page']['elements']['messages'] = false;
	// Affichage du choix du thème
	$conf['page']['elements']['choixTheme'] = false;
	// Affichage du menu d'administration
	$conf['page']['elements']['menuAdmin'] = false;
	
	// éléments de debug
	
	// FirePHP
	$conf['page']['elements']['firePHP'] = true;
	// Affichage des timeInfos
	$conf['page']['elements']['timeInfo'] = $DEBUG;
	// Affichage de l'utilisation mémoire
	$conf['page']['elements']['memUsage'] = $DEBUG;
	// Affichage des WherewereU
	$conf['page']['elements']['whereWereU'] = $DEBUG;
	// Affichage du lastError
	$conf['page']['elements']['lastError'] = $DEBUG;
	// Affichage du lastErrorMessage
	$conf['page']['elements']['lastErrorMessage'] = $DEBUG;
	// Affichage des messages de debug
	$conf['page']['elements']['debugMessages'] = $DEBUG;



	// Utilisation de jquery
	$conf['page']['javascript']['jquery'] = false;
	// Utilisation de grille2.js.php
	$conf['page']['javascript']['grille2'] = false;
	// Utilisation de utilisateur.js
	$conf['page']['javascript']['utilisateur'] = false;

	// Feuilles de styles
	// Utilisation de la feuille de style general.css
	$conf['page']['stylesheet']['general'] = true;
	$conf['page']['stylesheet']['grille'] = false;
	$conf['page']['stylesheet']['grilleUnique'] = false;
	$conf['page']['stylesheet']['utilisateur'] = false;

	// Compactage des pages
	$conf['page']['compact'] = false;
	
/*
 * Fin de la configuration de la page
 */

require 'required_files.inc.php';

if (array_key_exists('user', $_POST) && array_key_exists('pass', $_POST)) {
	$dsn = $GLOBALS['DSN']['admin'];
	$dsn['username'] = $_POST['user'];
	$dsn['password'] = $_POST['pass'];
	$dsn['dbname'] = NULL; // On doit se connecter sans base de données choisie car la base de données n'existe pas encore...
	$db = new database($dsn);

?><p>Voici les commandes SQL à effectuer pour l'installation de TeamTime :<br>
Pensez à <span style="color:red;">supprimer le fichier install.php</span> lorsque les commandes ont été exécutées.<br><br>
Si vous utilisez mysql &lt; 5.7.6, Supprimez IF NOT EXISTS des commandes CREATE USER comme cette option n'est pas supporté pour les versions antérieures.<br><br>
Il est sage de commencer par supprimer tous les utilisateurs de la base de données en rapport avec TeamTime afin que les mots de passe soient corrects.
</p><br><br>
	<pre><?php
// Création de la base de données
$sql = sprintf("
	DROP DATABASE IF EXISTS %s"
	, $GLOBALS['DSN']['admin']['dbname']
);
$db->db_interroge($sql);
print($sql);
?>;<br><?php
// Création de la base de données
$sql = sprintf("
	CREATE DATABASE %s CHARACTER SET = '%s' COLLATE = 'utf8_general_ci'"
	, $GLOBALS['DSN']['admin']['dbname']
	, $GLOBALS['DSN']['admin']['NAMES']
);
$db->db_interroge($sql);
print($sql);
?>;<br><?php
// Création du compte notlogged
$sql = sprintf("
	DROP USER IF EXISTS '%s'@'%s'"
	, $GLOBALS['DSN']['notlogged']['username']
	, ($GLOBALS['DSN']['notlogged']['hostname'] == 'localhost' ? 'localhost' : '%')
);
$db->db_interroge($sql);
print($sql);
?>;<br><?php
$sql = sprintf("
	CREATE USER '%s'@'%s' IDENTIFIED BY '%s'"
	, $GLOBALS['DSN']['notlogged']['username']
	, ($GLOBALS['DSN']['notlogged']['hostname'] == 'localhost' ? 'localhost' : '%')
	, $GLOBALS['DSN']['notlogged']['password']
);
$db->db_interroge($sql);
print($sql);
?>;<br><?php
$sql = sprintf("
	GRANT SELECT, EXECUTE
	ON %s.* TO '%s'@'%s'"
	, $GLOBALS['DSN']['notlogged']['dbname']
	, $GLOBALS['DSN']['notlogged']['username']
	, ($GLOBALS['DSN']['notlogged']['hostname'] == 'localhost' ? 'localhost' : '%')
);
$db->db_interroge($sql);
print($sql);
?>;<br><?php
// Création du compte createAccount
$sql = sprintf("
	DROP USER IF EXISTS '%s'@'%s'"
	, $GLOBALS['DSN']['createAccount']['username']
	, ($GLOBALS['DSN']['createAccount']['hostname'] == 'localhost' ? 'localhost' : '%')
);
$db->db_interroge($sql);
print($sql);
?>;<br><?php
$sql = sprintf("
	CREATE USER '%s'@'%s' IDENTIFIED BY '%s'"
	, $GLOBALS['DSN']['createAccount']['username']
	, ($GLOBALS['DSN']['createAccount']['hostname'] == 'localhost' ? 'localhost' : '%')
	, $GLOBALS['DSN']['createAccount']['password']
);
$db->db_interroge($sql);
print($sql);
?>;<br><?php
$sql = sprintf("
	GRANT USAGE, CREATE USER
	ON *.* TO '%s'@'%s' WITH GRANT OPTION"
	, $GLOBALS['DSN']['createAccount']['username']
	, ($GLOBALS['DSN']['createAccount']['hostname'] == 'localhost' ? 'localhost' : '%')
);
$db->db_interroge($sql);
print($sql);
?>;<br><?php
$sql = sprintf("
	GRANT SELECT, INSERT, UPDATE, DELETE, CREATE TEMPORARY TABLES, EXECUTE
	ON %s.* TO '%s'@'%s'"
	, $GLOBALS['DSN']['createAccount']['dbname']
	, $GLOBALS['DSN']['createAccount']['username']
	, ($GLOBALS['DSN']['createAccount']['hostname'] == 'localhost' ? 'localhost' : '%')
);
$db->db_interroge($sql);
print($sql);
?>;<br><?php
// Création de l'utilisateur admin
$sql = sprintf("
	DROP USER IF EXISTS '%s'@'%s'"
	, $GLOBALS['DSN']['admin']['username']
	, ($GLOBALS['DSN']['admin']['hostname'] == 'localhost' ? 'localhost' : '%')
);
$db->db_interroge($sql);
print($sql);
?>;<br><?php
$sql = sprintf("
	CREATE USER '%s'@'%s' IDENTIFIED BY '%s'"
	, $GLOBALS['DSN']['admin']['username']
	, ($GLOBALS['DSN']['admin']['hostname'] == 'localhost' ? 'localhost' : '%')
	, $GLOBALS['DSN']['admin']['password']
);
$db->db_interroge($sql);
print($sql);
?>;<br><?php
$sql = sprintf("
	GRANT USAGE
	ON *.* TO '%s'@'%s' WITH GRANT OPTION"
	, $GLOBALS['DSN']['admin']['username']
	, ($GLOBALS['DSN']['admin']['hostname'] == 'localhost' ? 'localhost' : '%')
);
$db->db_interroge($sql);
print($sql);
?>;<br><?php
$sql = sprintf("
	GRANT ALL PRIVILEGES
	ON %s.* TO '%s'@'%s' WITH GRANT OPTION"
	, $GLOBALS['DSN']['admin']['dbname']
	, $GLOBALS['DSN']['admin']['username']
	, ($GLOBALS['DSN']['admin']['hostname'] == 'localhost' ? 'localhost' : '%')
);
$db->db_interroge($sql);
print($sql);
?>;
</pre><br><br>
<p>Vous devez ensuite sourcer les fichiers sql du répertoire sql de TeamTime avant d'exécuter la commande suivante :</p><br>
<pre>
<?php
$sql = sprintf("
	CALL __recreateAllUsersDb('%s', '%s')"
	, $GLOBALS['DSN']['user']['password']
	, ($GLOBALS['DSN']['admin']['hostname'] == 'localhost' ? 'localhost' : '%')
);
print($sql);
?>;</pre><?php

} else {
?>
<p>Identifiants pour la base de données :
<form name="idbd" method="POST">
<input type="text" name="user" placeholder="Nom de l'utilisateur" />
<input type="password" name="pass" placeholder="Mot de passe" />
<input type="submit" />
</form>
<?php

}

/*
 * Informations de debug
 */
include 'debug.inc.php';
firePhpLog($conf, '$conf');
firePhpLog(debug::getInstance()->format(), 'format debug messages');
firePhpLog($javascript, '$javascript');
firePhpLog($stylesheet, '$stylesheet');

// Affichage du bas de page
$smarty->display('footer.tpl');

ob_end_flush(); // Obligatoire pour firePHP

?>
